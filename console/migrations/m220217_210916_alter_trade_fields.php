<?php

use yii\db\Migration;

/**
 * Class m220217_210916_alter_trade_fields
 */
class m220217_210916_alter_trade_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_trade}}', 'tp', $this->text()->null()->defaultValue(null)->after('setup_abbr'));
        $this->addColumn('{{%user_trade}}', 'short_amount', $this->decimal(10, 4)->null()->defaultValue(null)->after('setup_abbr'));
        $this->addColumn('{{%user_trade}}', 'long_amount', $this->decimal(10, 4)->null()->defaultValue(null)->after('setup_abbr'));
        $this->addColumn('{{%user_trade}}', 'loss', $this->tinyInteger()->null()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%user_trade}}', 'tp');
       $this->dropColumn('{{%user_trade}}', 'short_amount');
       $this->dropColumn('{{%user_trade}}', 'long_amount');
       $this->dropColumn('{{%user_trade}}', 'loss');
    }
}
