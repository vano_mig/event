<?php

use common\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%statistics}}`.
 */
class m211214_212723_create_statistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%statistics}}', [
            'id' => $this->bigPrimaryKey(),
            'user_id' =>$this->integer()->null()->defaultValue(null),
            'amount'=>$this->decimal(10.2)->null()->defaultValue(null),
            'market'=>$this->string()->null()->defaultValue(null),
            'file' =>$this->string()->null()->defaultValue(null),
            'comment'=>$this->text()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null)
        ]);

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);

        $userCreate = $auth->createPermission('statistic.create');
        $userCreate->description = 'Create statistic record';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('statistic.update');
        $userUpdate->description = 'Update statistic data';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('statistic.delete');
        $userDelete->description = 'Delete statistic data';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('statistic.view');
        $userView->description = 'View statistic data';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('statistic.listview');
        $userListview->description = 'View statistic list';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%statistics}}');

        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('statistic.listview');
        $view = $auth->getPermission('statistic.view');
        $create = $auth->getPermission('statistic.create');
        $update = $auth->getPermission('statistic.update');
        $delete = $auth->getPermission('statistic.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
    }
}
