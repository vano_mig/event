<?php

use backend\models\classes\UserModel;
use common\models\User;
use yii\db\Migration;

/**
 * Class m211210_093717_add_translation_permission
 */
class m211210_093717_add_translation_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);

        //Создаем права доступа для ролей

        //доступ к пользователям
        $userCreate = $auth->createPermission('translation.create');
        $userCreate->description = 'Create translation';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('translation.update');
        $userUpdate->description = 'Update translation';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('translation.delete');
        $userDelete->description = 'Delete translation';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('translation.view');
        $userView->description = 'View translation data';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('translation.listview');
        $userListview->description = 'View translation list';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole(User::ROLE_ADMIN);
        $list = $auth->getPermission('translation.listview');
        $view = $auth->getPermission('translation.view');
        $create = $auth->getPermission('translation.create');
        $update = $auth->getPermission('translation.update');
        $delete = $auth->getPermission('translation.delete');
        $auth->removeChild($admin, $list);
        $auth->removeChild($admin, $view);
        $auth->removeChild($admin, $create);
        $auth->removeChild($admin, $update);
        $auth->removeChild($admin, $delete);
        $auth->remove($list);
        $auth->remove($view);
        $auth->remove($create);
        $auth->remove($update);
        $auth->remove($delete);
    }
}
