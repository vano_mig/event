<?php

use yii\db\Migration;

/**
 * Class m220115_190827_add_field_to_user_table
 */
class m220115_190827_add_field_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'coach_email', $this->string()->null()->defaultValue(null)->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'coach_email');
    }
}
