<?php

use yii\db\Migration;

/**
 * Class m220214_075137_add_logged_column_to_user
 */
class m220214_075137_add_logged_column_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'logged', $this->dateTime()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'logged');
    }
}
