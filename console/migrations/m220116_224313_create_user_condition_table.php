<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_condition}}`.
 */
class m220116_224313_create_user_condition_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_condition}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'rituale'=>$this->text()->null()->defaultValue(null),
            'prepare'=>$this->text()->null()->defaultValue(null),
            'documentation'=>$this->text()->null()->defaultValue(null),
            'setup_one_data'=>$this->text()->null()->defaultValue(null),
            'setup_one_manage'=>$this->text()->null()->defaultValue(null),
            'setup_one_criterium'=>$this->text()->null()->defaultValue(null),
            'setup_one_criterium_name'=>$this->text()->null()->defaultValue(null),
            'setup_one_criterium_abbr'=>$this->string(3)->null()->defaultValue(null),
            'setup_two_data'=>$this->text()->null()->defaultValue(null),
            'setup_two_manage'=>$this->text()->null()->defaultValue(null),
            'setup_two_criterium'=>$this->text()->null()->defaultValue(null),
            'setup_two_criterium_name'=>$this->text()->null()->defaultValue(null),
            'setup_two_criterium_abbr'=>$this->string(3)->null()->defaultValue(null),
            'setup_three_data'=>$this->text()->null()->defaultValue(null),
            'setup_three_manage'=>$this->text()->null()->defaultValue(null),
            'setup_three_criterium'=>$this->text()->null()->defaultValue(null),
            'setup_three_criterium_name'=>$this->text()->null()->defaultValue(null),
            'setup_three_criterium_abbr'=>$this->string(3)->null()->defaultValue(null),
            'markets'=>$this->text()->null()->defaultValue(null),
            'time_work'=>$this->text()->null()->defaultValue(null),
            'time_pause'=>$this->text()->null()->defaultValue(null),
            'max_trade'=>$this->integer()->null()->defaultValue(null),
            'max_decrease_trade'=>$this->integer()->null()->defaultValue(null),
            'max_decrease_point_trade'=>$this->integer()->null()->defaultValue(null),
            'limit_desc'=>$this->text()->null()->defaultValue(null),
            'limit_to_do'=>$this->text()->null()->defaultValue(null),
            'limit_daily'=>$this->decimal(10,2)->null()->defaultValue(null),
            'limit_daily_step'=>$this->integer()->null()->defaultValue(null),
            'limit_daily_max'=>$this->integer()->null()->defaultValue(null),
            'limit_weekly'=>$this->decimal(10,2)->null()->defaultValue(null),
            'dayly_amount'=>$this->decimal(10,2)->null()->defaultValue(null),
            'photo_1' =>$this->string(255)->null()->defaultValue(null),
            'photo_2' =>$this->string(255)->null()->defaultValue(null),
            'photo_3' =>$this->string(255)->null()->defaultValue(null),
            'photo_4' =>$this->string(255)->null()->defaultValue(null),
            'tasks'=>$this->text()->null()->defaultValue(null),
            'emotions'=>$this->text()->null()->defaultValue(null),
            'contract_ort'=>$this->string()->null()->defaultValue(null),
            'contract_date'=>$this->string()->null()->defaultValue(null),
            'contract_signature'=>$this->string()->null()->defaultValue(null),
            'step_status'=>$this->tinyInteger()->null()->defaultValue(0),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_condition}}');
    }
}
