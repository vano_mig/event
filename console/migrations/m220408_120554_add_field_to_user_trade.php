<?php

use yii\db\Migration;

/**
 * Class m220408_120554_add_field_to_user_trade
 */
class m220408_120554_add_field_to_user_trade extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_trade}}', 'total', $this->decimal(10,2)->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%user_trade}}', 'total');
    }
}
