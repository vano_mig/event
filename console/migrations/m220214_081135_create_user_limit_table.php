<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_limit}}`.
 */
class m220214_081135_create_user_limit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_limit}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'amount'=>$this->decimal(10,2)->null()->defaultValue(null),
            'amount_weekly'=>$this->decimal(10,2)->null()->defaultValue(null),
            'limit'=>$this->decimal(10,2)->null()->defaultValue(null),
            'limit_amount'=>$this->decimal(10,2)->null()->defaultValue(null),
//            'block'=>$this->decimal(10,2)->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_limit}}');
    }
}
