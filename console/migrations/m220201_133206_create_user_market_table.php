<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_market}}`.
 */
class m220201_133206_create_user_market_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_market}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->null()->defaultValue(null),
            'name'=>$this->string()->null()->defaultValue(null),
            'short_name'=>$this->string()->null()->defaultValue(null),
            'status'=>$this->integer()->null()->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_market}}');
    }
}
