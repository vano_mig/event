<?php

use backend\models\classes\UserModel;
use common\models\User;
use yii\db\Migration;

/**
 * Class m211204_165543_create
 */
class m211204_165543_create extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $publisher = $auth->createRole(User::ROLE_USER);
        $publisher->description = 'user';
        $auth->add($publisher);

        $manager = $auth->createRole(User::ROLE_MANAGER);
        $manager->description = 'manager';
        $auth->add($manager);

        $auth = Yii::$app->authManager;
        $admin = $auth->createRole(User::ROLE_ADMIN);
        $admin->description = 'administrator';
        $auth->add($admin);

        //Создаем права доступа для ролей

        //доступ к пользователям
        $userCreate = $auth->createPermission('user.create');
        $userCreate->description = 'Create user';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('user.update');
        $userUpdate->description = 'Update user';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('user.delete');
        $userDelete->description = 'Delete user';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('user.view');
        $userView->description = 'View user data';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('user.listview');
        $userListview->description = 'View user list';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);

        //доступ к языкам
        $langCreate = $auth->createPermission('language.create');
        $langCreate->description = 'Create language';
        $auth->add($langCreate);
        $auth->addChild($admin, $langCreate);

        $langUpdate = $auth->createPermission('language.update');
        $langUpdate->description = 'Update language';
        $auth->add($langUpdate);
        $auth->addChild($admin, $langUpdate);

        $langDelete = $auth->createPermission('language.delete');
        $langDelete->description = 'Delete language';
        $auth->add($langDelete);
        $auth->addChild($admin, $langDelete);

        $langView = $auth->createPermission('language.view');
        $langView->description = 'View language data';
        $auth->add($langView);
        $auth->addChild($admin, $langView);

        $langListview = $auth->createPermission('language.listview');
        $langListview->description = 'View language list';
        $auth->add($langListview);
        $auth->addChild($admin, $langListview);

        //доступ к странам
        $countryCreate = $auth->createPermission('country.create');
        $countryCreate->description = 'Create countyr';
        $auth->add($countryCreate);
        $auth->addChild($admin, $countryCreate);

        $countryUpdate = $auth->createPermission('country.update');
        $countryUpdate->description = 'Update language';
        $auth->add($countryUpdate);
        $auth->addChild($admin, $countryUpdate);

        $countryDelete = $auth->createPermission('country.delete');
        $countryDelete->description = 'Delete country';
        $auth->add($countryDelete);
        $auth->addChild($admin, $countryDelete);

        $countryView = $auth->createPermission('country.view');
        $countryView->description = 'View country data';
        $auth->add($countryView);
        $auth->addChild($admin, $countryView);

        $countryListview = $auth->createPermission('country.listview');
        $countryListview->description = 'View country list';
        $auth->add($countryListview);
        $auth->addChild($admin, $countryListview);

        //доступ к пользователям
        $userCreate = $auth->createPermission('permission.create');
        $userCreate->description = 'Create role';
        $auth->add($userCreate);
        $auth->addChild($admin, $userCreate);

        $userUpdate = $auth->createPermission('permission.update');
        $userUpdate->description = 'Update role';
        $auth->add($userUpdate);
        $auth->addChild($admin, $userUpdate);

        $userDelete = $auth->createPermission('permission.delete');
        $userDelete->description = 'Delete role';
        $auth->add($userDelete);
        $auth->addChild($admin, $userDelete);

        $userView = $auth->createPermission('permission.view');
        $userView->description = 'View role';
        $auth->add($userView);
        $auth->addChild($admin, $userView);

        $userListview = $auth->createPermission('permission.listview');
        $userListview->description = 'View role list';
        $auth->add($userListview);
        $auth->addChild($admin, $userListview);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Yii::$app->authManager->removeAll();
    }
}
