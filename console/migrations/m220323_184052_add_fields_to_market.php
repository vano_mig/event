<?php

use yii\db\Migration;

/**
 * Class m220323_184052_add_fields_to_market
 */
class m220323_184052_add_fields_to_market extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_market}}', 'tickgrobe', $this->decimal(10, 2)->null()->defaultValue(null)->after('short_name'));
        $this->addColumn('{{%user_market}}', 'tickwert', $this->decimal(10, 2)->null()->defaultValue(null)->after('tickgrobe'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%user_market}}', 'tickgrobe');
       $this->dropColumn('{{%user_market}}', 'tickwert');
    }
}
