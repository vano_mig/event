<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_trade}}`.
 */
class m220202_092219_create_user_trade_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_trade}}', [
            'id' => $this->bigPrimaryKey(),
            'trade_number' =>$this->string(50)->null()->unique()->defaultValue(null),
            'user_id' =>$this->integer()->null()->defaultValue(null),
            'coach_email' =>$this->string(170)->null()->defaultValue(null),
            'ticker_name' => $this->string(170)->null()->defaultValue(null),
            'ticker_short_name' => $this->string(20)->null()->defaultValue(null),
            'lot'=>$this->integer()->null()->defaultValue(null),
            'ticks_sl'=>$this->integer()->null()->defaultValue(null),
            'setup'=>$this->string(10)->null()->defaultValue(null),
            'setup_abbr'=>$this->string(3)->null()->defaultValue(null),
            'emojy'=>$this->text()->null()->defaultValue(null),
            'files' =>$this->text()->null()->defaultValue(null),
            'start_trade'=>$this->integer()->null()->defaultValue(null),
            'finish_trade'=>$this->integer()->null()->defaultValue(null),
            'status'=>$this->integer()->null()->defaultValue(null),
            'created_at'=>$this->dateTime()->null()->defaultValue(null)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_trade}}');
    }
}
