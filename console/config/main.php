<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                    'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
                '*' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                    'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=event.db;dbname=event',
            'username' => 'root',
            'password' => 'cnfhjcnf2007',
            'charset' => 'utf8mb4',
        ],
    ],
    'params' => $params,
];