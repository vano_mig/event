<?php
/**
 * Telegram log target for Yii 2
 *
 * @see       https://github.com/sergeymakinen/yii2-telegram-log
 * @copyright Copyright (c) 2017 Sergey Makinen (https://makinen.ru)
 * @license   https://github.com/sergeymakinen/yii2-telegram-log/blob/master/LICENSE MIT License
 */

namespace backend\components;

use backend\models\User;
use sergeymakinen\yii\logmessage\Message;
use sergeymakinen\yii\telegramlog\Target;

class TargetHelper extends Target
{
    /**
     * Returns an array with the default substitutions.
     * @return array default substitutions.
     */
    protected function defaultSubstitutions()
    {
        return [
            'levelAndRequest' => [
                'title' => null,
                'short' => false,
                'wrapAsCode' => false,
                'value' => function (Message $message) {
                    if (isset($this->levelEmojis[$message->message[1]])) {
                        $value = $this->levelEmojis[$message->message[1]] . ' ';
                    } else {
                        $value = '*' . ucfirst($message->getLevel()) . '* @ ';
                    }
                    if ($message->getIsConsoleRequest()) {
                        $value .= '`' . $message->getCommandLine() . '`';
                    } else {
                        $value .= '[' . $message->getUrl() . '](' . $message->getUrl() . ')';
                    }
                    return $value;
                },
            ],
            'category' => [
                'emojiTitle' => '📖',
                'short' => true,
                'wrapAsCode' => false,
                'value' => function (Message $message) {
                    return '`' . $message->getCategory() . '`';
                },
            ],
            'user' => [
                'emojiTitle' => '🙂',
                'short' => true,
                'wrapAsCode' => false,
                'value' => function (Message $message) {
                    $value = [];
                    $ip = $message->getUserIp();
                    if ((string) $ip !== '') {
                        $value[] = $ip;
                    }
                    $id = $message->getUserId();
                    $user = null;
                    if ($id) {
                        $user = User::findOne($id);
                    }
                    if ((string) $id !== '') {
                        $val = "ID: `{$id}`";
                        if ($user) {
                            $val .= ": " . $user->email;
                        }
                        $value[] = $val;
                    }
                    return implode(str_repeat(' ', 4), $value);
                },
            ],
            'stackTrace' => [
                'title' => 'Stack Trace',
                'short' => false,
                'wrapAsCode' => true,
                'value' => function (Message $message) {
                    return $message->getStackTrace();
                },
            ],
            'text' => [
                'title' => null,
                'short' => false,
                'wrapAsCode' => true,
                'value' => function (Message $message) {
                    $text = $message->getText();

                    if (($pos = mb_strpos($text, 'Stack trace:')) !== false) {
                        $text = mb_substr($text, 0, $pos);
                    }

                    return $text;
                },
            ],
        ];
    }
}
