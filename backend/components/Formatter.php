<?php

namespace backend\components;

use yii\i18n\Formatter as Form;

class Formatter extends Form
{
    public function load($formatClass)
    {
        return new $formatClass;
    }
}