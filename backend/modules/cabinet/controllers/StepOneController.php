<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\classes\UserConditionModel;
use Yii;
use yii\helpers\Url;

/**
 * StepOne controller for the `cabinet` module
 */
class StepOneController extends BackendController
{

    public function actionIndex()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/trading-plain')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one', ['user'=>$user]);
    }

    public function actionTradingPlain()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/time')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('tradingplan', ['user'=>$user]);
    }

    public function actionTime()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/trading-plain')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/trading-job')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_time', ['user'=>$user]);
    }

    public function actionTradingJob()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/time')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/routine')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_job', ['user'=>$user]);
    }

    public function actionRoutine()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/trading-job')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rituale')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_routine', ['user'=>$user]);
    }

    public function actionRituale()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $active = true;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        if(empty($conditionModel->rituale)) {
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/routine')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/prepare'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_rituale', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveRituale()
    {
        $post = Yii::$app->request->post('rituale', '');

        if(empty($post))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->rituale = $post;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionPrepare()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $active = true;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        if(empty($conditionModel->prepare)) {
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rituale')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/documentation'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_prepare', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSavePrepare()
    {
        $post = Yii::$app->request->post('prepare', '');

        if(empty($post))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->prepare = $post;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionDocumentation()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/prepare')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/documentation-next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_documentation', ['user'=>$user]);
    }

    public function actionDocumentationNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;
        if(empty($conditionModel->documentation)) {
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/documentation')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/environtment'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_documentation_next', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveDocumentation()
    {
        $post = Yii::$app->request->post('documentation', '');

        if(empty($post))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);


        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->documentation = $post;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionEnvirontment()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/documentation-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/queue')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_env', ['user'=>$user]);
    }

    public function actionQueue()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/environtment')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/ablenkung')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_queue', ['user'=>$user]);
    }

    public function actionAblenkung() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/queue')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/ablenkung-next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_ablenkung', ['user'=>$user]);
    }

    public function actionAblenkungNext() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/ablenkung')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_ablenkung_next', ['user'=>$user]);
    }

    public function actionSetup() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/ablenkung-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-one')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_setup', ['user'=>$user]);
    }

    public function actionSetupOne() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->setupOneData) || empty($conditionModel->setupOneManage)) {
            $active = false;
        }
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-one-next'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_setup_one', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveSetupOne()
    {
        $resultData = [];
        $resultManage = [];

        $postData = Yii::$app->request->post('setup_data', '');
        $postManage = Yii::$app->request->post('setup_manage', '');


        if(empty($postData) || empty($postManage))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->setupOneData = $postData;
        $condition->setupOneManage = $postManage;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionSetupOneNext() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->setupOneCriterium ) || empty($conditionModel->setupOneCriteriumName) || !$conditionModel->setupOneCriteriumAbbr) {
            $active = false;
        }
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-one')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-two'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_setup_one_next', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveSetupOneNext()
    {
        $postCriterium = Yii::$app->request->post('setup_criterium', '');
        $postName = Yii::$app->request->post('setup_criterium_name', '');
        $postAbbr = Yii::$app->request->post('setup_criterium_abbr', '');

        if(empty($postCriterium) || empty($postName) || $postAbbr == '')
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);


        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->setupOneCriterium = $postCriterium;
        $condition->setupOneCriteriumName = $postName;
        $condition->setupOneCriteriumAbbr = $postAbbr;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionSetupTwo() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->setupTwoData) || empty($conditionModel->setupTwoManage)) {
            $active = false;
        }
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-one-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-two-next'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_setup_two', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveSetupTwo()
    {
        $postData = Yii::$app->request->post('setup_data', '');
        $postManage = Yii::$app->request->post('setup_manage', '');

        if(empty($postData) || empty($postManage))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->setupTwoData = $postData;
        $condition->setupTwoManage = $postManage;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionSetupTwoNext() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->setupTwoCriterium ) || empty($conditionModel->setupTwoCriteriumName) || !$conditionModel->setupTwoCriteriumAbbr) {
            $active = false;
        }
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-two')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-three'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_setup_two_next', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveSetupTwoNext()
    {
        $postCriterium = Yii::$app->request->post('setup_criterium', '');
        $postName = Yii::$app->request->post('setup_criterium_name', '');
        $postAbbr = Yii::$app->request->post('setup_criterium_abbr', '');

        if(empty($postCriterium) || empty($postName) || $postAbbr == '')
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->setupTwoCriterium =$postCriterium;
        $condition->setupTwoCriteriumName = $postName;
        $condition->setupTwoCriteriumAbbr = $postAbbr;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionSetupThree() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->setupThreeData) || empty($conditionModel->setupThreeManage)) {
            $active = false;
        }
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-two-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-three-next'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_setup_three', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveSetupThree()
    {
        $postData = Yii::$app->request->post('setup_data', '');
        $postManage = Yii::$app->request->post('setup_manage', '');

        if(empty($postData) || empty($postManage))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->setupThreeData = $postData;
        $condition->setupThreeManage = $postManage;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionSetupThreeNext() {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->setupThreeCriterium ) || empty($conditionModel->setupThreeCriteriumName) || !$conditionModel->setupThreeCriteriumAbbr) {
            $active = false;
        }
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-three')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/structure'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_setup_three_next', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveSetupThreeNext()
    {
        $postCriterium = Yii::$app->request->post('setup_criterium', '');
        $postName = Yii::$app->request->post('setup_criterium_name', '');
        $postAbbr = Yii::$app->request->post('setup_criterium_abbr', '');

        if(empty($postCriterium) || empty($postName) || $postAbbr == '')
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->setupThreeCriterium = $postCriterium;
        $condition->setupThreeCriteriumName = $postName;
        $condition->setupThreeCriteriumAbbr = $postAbbr;
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionStructure()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/setup-three-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/markets')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_one_structure', ['user'=>$user]);
    }

    public function actionMarkets()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->markets )) {
            $active = false;
        }
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/structure')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/market-stream'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('markets', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveMarkets()
    {
        $postMarkets = Yii::$app->request->post('setup_markets', '');

        if(empty($postMarkets))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->userId = Yii::$app->user->id;
        $condition->markets = trim($postMarkets);
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionMarketStream()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/markets')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/work-time')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('markets_stream', ['user'=>$user]);
    }

    public function actionWorkTime()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/market-stream')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/work-time-next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('work_time', ['user'=>$user]);
    }

    public function actionWorkTimeNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/work-time')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/work-time-pause')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('work_time_continue', ['user'=>$user]);
    }

    public function actionWorkTimePause()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;
        if(!empty($conditionModel->timeWork)) {
            $conditionModel->timeWork = json_decode($conditionModel->timeWork, true);
        } else {
            $conditionModel->timeWork = [
                Yii::t('cabinet', 'mon') => '',
                Yii::t('cabinet', 'tue') => '',
                Yii::t('cabinet', 'wed') => '',
                Yii::t('cabinet', 'thu') => '',
                Yii::t('cabinet', 'fri') => '',
            ];
            $active = false;
        }

        if(!empty($conditionModel->timePause)) {
            $conditionModel->timePause = json_decode($conditionModel->timePause, true);
        } else {
            $conditionModel->timePause = [
                Yii::t('cabinet', 'mon') => '',
                Yii::t('cabinet', 'tue') => '',
                Yii::t('cabinet', 'wed') => '',
                Yii::t('cabinet', 'thu') => '',
                Yii::t('cabinet', 'fri') => '',
            ];
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/work-time-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rules'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('work_time_form', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveWorkTimePause()
    {
        $postWork = Yii::$app->request->post('work_time', []);
        $postPause = Yii::$app->request->post('work_pause', []);

        if(empty($postWork) || empty($postPause))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);


        $validateDay = false;
        foreach ($postWork as $data) {
            if(!$data)
                continue;
            else
                $validateDay = true;
        }

        $validatePause = false;
        foreach ($postPause as $data) {
            if(!$data)
                continue;
            else
                $validatePause = true;
        }

        if($validateDay == false || $validatePause == false)
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->timeWork = json_encode($postWork);
        $condition->timePause = json_encode($postPause);
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionRules()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/work-time-pause')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rules-next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('rules', ['user'=>$user]);
    }

    public function actionRulesNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rules')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/news-business')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('rules_next', ['user'=>$user]);
    }

    public function actionNewsBusiness()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rules-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-trade')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('news_business', ['user'=>$user]);
    }

    public function actionLimitTrade()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;
        if(empty($conditionModel->maxTrade) || empty($conditionModel->maxDecreaseTrade)) {
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/news-business')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-trade-next'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('limit_trade', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveLimitTrade()
    {
        $limitTrade = (int) Yii::$app->request->post('max_trade', 0);
        $limitDecreaseTrade =(int) Yii::$app->request->post('max_decrease_trade', 0);

        if(!$limitTrade || !$limitDecreaseTrade)
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->maxTrade = number_format($limitTrade, 2, '.', '');
        $condition->maxDecreaseTrade = number_format($limitDecreaseTrade, 2, '.', '');
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionLimitTradeNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;
        if(empty($conditionModel->maxDecreasePointTrade)) {
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-trade')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-trade-finish'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('limit_trade_next', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveLimitTradeNext()
    {
        $maxDecreasePointTrade = (int) Yii::$app->request->post('max_decrease_point_trade', 0);

        if(!$maxDecreasePointTrade)
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->maxDecreasePointTrade = number_format($maxDecreasePointTrade, 2, '.', '');
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionLimitTradeFinish()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->limitDesc ) || empty($conditionModel->limitToDo )) {
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-trade-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-lost'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('limit_trade_finish', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveLimitTradeFinish()
    {
        $postDesc = Yii::$app->request->post('limit_desc', '');
        $postToDo = Yii::$app->request->post('limit_to_do', '');

        if(empty($postDesc) || empty($postToDo))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->limitDesc = trim($postDesc);
        $condition->limitToDo = trim($postToDo);
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionLimitLost()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-trade-finish')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-lost-next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('limit_lost', ['user'=>$user]);
    }

    public function actionLimitLostNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->limitDaily ) || empty($conditionModel->limitDailyMax ) || empty($conditionModel->limitDailyStep )) {
            if(!$conditionModel->limitDailyMax)
                $conditionModel->limitDailyMax = 3;

            if(!$conditionModel->limitDailyStep)
                $conditionModel->limitDailyStep = 3;
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-lost')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-lost-finish'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('limit_lost_next', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveLimitLostNext()
    {
        $postDaily = Yii::$app->request->post('limit_daily', 0);
        $postDailyStep = Yii::$app->request->post('limit_daily_step', 0);
        $postDailyMax = Yii::$app->request->post('max_limit_one', 0);

        if(empty($postDaily) || empty($postDailyStep) || empty($postDailyMax))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        if((float)$postDaily <= 0 || (int)$postDailyStep <=0 || (int)$postDailyMax <=0)
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->limitDaily = number_format($postDaily,2 ,'.', '');
        $condition->limitDailyStep = number_format($postDailyStep,2 ,'.', '');
        $condition->limitDailyMax = number_format($postDailyMax,2 ,'.', '');
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionLimitLostFinish()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        $active = true;

        if(empty($conditionModel->limitWeekly )) {
            $active = false;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-lost-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/risk'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('limit_lost_finish', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveLimitLostFinish()
    {
        $postWeekly = Yii::$app->request->post('limit_weekly', 0);

        if(empty($postWeekly))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        if((float)$postWeekly <= 0)
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->limitWeekly = number_format($postWeekly, 2, '.', '');
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionRisk()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/limit-lost-finish')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/example')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('risk', ['user'=>$user]);
    }

    public function actionExample()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/risk')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rentable')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('example', ['user'=>$user]);
    }

    public function actionRentable()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/example')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rentable-manage')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('rentable', ['user'=>$user]);
    }

    public function actionRentableManage()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rentable')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/tasks')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('rentable_manage', ['user'=>$user]);
    }

    public function actionTasks()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/rentable-manage')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/tasks-next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('tasks', ['user'=>$user]);
    }

    public function actionTasksNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $active = false;
        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        if($condition->photo_1) {
            $condition->photo_1 = basename($condition->photo_1);
            $active = true;
        }
        if($condition->photo_2) {
            $condition->photo_2 = basename($condition->photo_2);
            $active = true;
        }
        if($condition->photo_3) {
            $condition->photo_3 = basename($condition->photo_3);
            $active = true;
        }
        if($condition->photo_4) {
            $condition->photo_4 = basename($condition->photo_4);
            $active = true;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/tasks')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/tasks-steps'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('tasks_next', ['user'=>$user, 'condition'=>$condition]);
    }

    public function actionLoadPhoto()
    {
//        echo "<pre>";print_r($_FILES);die;
        $id = $_POST['id'];
        if ($id) {
            if (!empty($_FILES) && array_key_exists('file', $_FILES)) {
                $ext = pathinfo($_FILES['file']['name']);
                $file = '/images/user_files/'.time().'.'.$ext['extension'];
                $path = Yii::getAlias('@backend').'/web'.$file;
//                if (file_put_contents($path, $_FILES['file']['tmp_name'])) {
                if (move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    $attribute = 'photo_'.$id;
                    $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
                    $condition->$attribute = $file;
                    if(UserConditionModel::saveModel($condition)) {
                        return $this->asJson(['status'=>'success']);
                    }
                    return $this->asJson(['status' => 'success']);
                }
            }
        }
        return $this->asJson(['status' => 'error']);
    }

    public function actionTasksSteps()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/tasks-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/tasks-steps-next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('tasks_steps', ['user'=>$user]);
    }

    public function actionTasksStepsNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/tasks-steps')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/step-by-step')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('tasks_steps_next', ['user'=>$user]);
    }

    public function actionStepByStep()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/tasks-steps-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/step-by-step-next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('steps_by_step', ['user'=>$user]);
    }

    public function actionStepByStepNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/step-by-step')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/step-by-step-form')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('steps_by_step_next', ['user'=>$user]);
    }

    public function actionStepByStepForm()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $active = false;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);

        if(!empty($conditionModel->tasks))
            $active = true;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/step-by-step-next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/step-by-step-dayly'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_form', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveStepByStepForm()
    {
        $postTasks = Yii::$app->request->post('tasks', '');

        if(empty($postTasks))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->tasks = trim($postTasks);
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionStepByStepDayly()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $active = false;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        if($conditionModel->daylyAmount) {
            $active = true;
        }

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/step-by-step-form')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/discipline'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_form_dayly', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveStepByStepDayly()
    {
        $postTasks = (float)Yii::$app->request->post('dayly_amount', 0);
        $postTasks = number_format($postTasks, 2, '.', '');
        if((float)$postTasks <= 0)
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->daylyAmount = number_format($postTasks, 2, '.', '');
        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);
    }

    public function actionDiscipline()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/step-by-step-dayly')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/contract')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('discipline', ['user'=>$user]);
    }

    public function actionContract()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $active = true;
        $conditionModel = UserConditionModel::getConditionByUserId($user->id);
        if(!$conditionModel->contractOrt) {
            $active = false;
        }
        if(!$conditionModel->contractDate) {
            $active = false;
        }
        if(!$conditionModel->contractSignature) {
            $active = false;
        } else {
            $conditionModel->contractSignature = basename($conditionModel->contractSignature);
        }
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Two'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Tradingplan'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Tradingplan'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/discipline')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-one/congrats'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('contract', ['user'=>$user, 'condition'=>$conditionModel]);
    }

    public function actionSaveContract()
    {
        $ort = $_POST['ort'];
        $date = $_POST['date'];

        if(!$ort || !$date)
            return $this->asJson(['status' => 'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
            if (!empty($_FILES) && array_key_exists('file', $_FILES)) {
                $ext = pathinfo($_FILES['file']['name']);
                $file = '/images/user_files/'.time().'.'.$ext['extension'];
                $path = Yii::getAlias('@backend').'/web'.$file;
//                if (file_put_contents($path, $_FILES['file']['tmp_name'])) {
                if (move_uploaded_file($_FILES['file']['tmp_name'], $path)) {
                    $condition->contractOrt = $ort;
                    $condition->contractDate = $date;
                    $condition->contractSignature = $file;
                    if(UserConditionModel::saveModel($condition, true)) {
                        return $this->asJson(['status'=>'success']);
                    }
                    return $this->asJson(['status' => 'success']);
                }
            } elseif($condition->contractSignature) {
                $condition->contractOrt = $ort;
                $condition->contractDate = $date;
                if(UserConditionModel::saveModel($condition, true)) {
                    return $this->asJson(['status'=>'success']);
                }
            }

        return $this->asJson(['status' => 'error', 'message'=>"Please upload signature"]);
    }

    public function actionCongrats()
    {
        $this->layout = 'congrats';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'buttons'=> [
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('congrats', ['user'=>$user]);
    }
}
