<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\classes\CountryModel;
use backend\models\Language;
use backend\modules\cabinet\models\LanguageModel;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class LanguageController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Language models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->accessRules('language.listview');
        $models = LanguageModel::searchLanguageList();

        return $this->render('index', [
            'dataProvider' => $models,
        ]);
    }

    /**
     * Finds the Language model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LanguageModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LanguageModel::getLanguageById($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }

    /**
     * Creates a new Language model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        $this->accessRules('language.create');
        $model = new LanguageModel();
        if ($model->load(Yii::$app->request->post())) {
            $country = CountryModel::getCountryByName($model->name);
            $model->iso_code = $country->iso_code;
            if ($model->validate() && LanguageModel::saveLanguage($model)) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute('index'));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Language model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->accessRules('language.update');
        $model = $this->findModel($id);
        $main = $model->system;
        if ($model->load(Yii::$app->request->post())) {
            $country = CountryModel::getCountryByName($model->name);
            $model->iso_code = $country->iso_code;

            if ($model->validate() && LanguageModel::saveLanguage($model)) {
                if ($model->system != $main) {
                    LanguageModel::checkMainLang($model->main, $model->id);
                }
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute('index'));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Makes language active
     * @param $id integer
     * @return mixed
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionMain($id)
    {
        $this->accessRules('language.update');
        $model = $this->findModel($id);

        if ($model->system == Language::STATUS_ACTIVE) {
            $model->system = Language::STATUS_INACTIVE;
        } else {
            $model->system = Language::STATUS_ACTIVE;
            $model->status = Language::STATUS_ACTIVE;
        }

        LanguageModel::checkMainLang($model->system, $model->id);
        if (LanguageModel::saveLanguage($model)) {
            return $this->redirect(Url::toRoute('index'));
        }
    }

    /**
     * Deletes an existing Language model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->accessRules('language.delete');
        LanguageModel::deleteLanguage($id);
        Yii::$app->session->setFlash('warning', Yii::t('admin', 'record deleted'));

        return $this->redirect(Url::toRoute('index'));
    }
}