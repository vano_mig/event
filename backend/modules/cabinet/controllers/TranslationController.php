<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\Message;
use backend\models\SourceMessage;
use backend\modules\cabinet\models\LanguageModel;
use backend\modules\cabinet\models\MessageModel;
use backend\modules\cabinet\models\SourceModel;
use backend\models\classes\SourceMessageSearch;
use Yii;
use yii\base\Model;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class TranslationController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SourceMessage models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->accessRules('translation.listview');
        $searchModel = new SourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SourceMessage model.
     *
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
//        echo "<pre>";print_r(MessageModel::getMessages($id));die;
        $this->accessRules('translation.view');
        $langs = LanguageModel::getActiveCodeLanguages();
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'messages' => MessageModel::getMessages($model->id),
            'langs' => $langs
        ]);
    }

    /**
     * Create view.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        $this->accessRules('translation.create');
        $model = new SourceModel();
        $modelMessage = new MessageModel();
        $lang = LanguageModel::getActiveCodeLanguages();

        return $this->render('create', [
            'model' => $model,
            'modelMessage' => $modelMessage,
            'lang' => $lang,
        ]);
    }

    /**
     * Update view.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $this->accessRules('translation.update');
        $model = $this->findModel($id);
        $lang = LanguageModel::getActiveCodeLanguages();
        return $this->render('update', [
            'model' => $model,
            'messages' => MessageModel::getMessages($model->id),
            'lang' => $lang,
        ]);
    }

    /**
     * Updates logic.
     *
     * @return yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateTranslation()
    {
        if (Yii::$app->request->post('save')) {
            $post = Yii::$app->request->post();
            $model = SourceMessage::findOne($post['SourceModel']['id']);
            $modelMessage = MessageModel::getMessages($model->id);
            if (Model::loadMultiple($modelMessage, Yii::$app->request->post()) && Model::validateMultiple($modelMessage)) {
                    foreach ($modelMessage as $setting) {
                        $setting->save(false);
                    }
                    Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                    return $this->redirect(Url::toRoute(['view', 'id' => $post['SourceModel']['id']]));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
            }
            return $this->redirect(Url::toRoute(['update', 'id' => $post['SourceModel']['id']]));
        }
    }

    /**
     * Create logic.
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreateTranslation()
    {
        if (Yii::$app->request->post('save')) {
            $post = Yii::$app->request->post();
            $model = new SourceMessage();
            $model->category = $post['SourceModel']['category'];
            $model->message = $post['SourceModel']['message'];
            if ($model->validate()) {
                $model->save();
                foreach ($post['MessageModel'] as $item) {
                    $modelMessage = new Message();
                    $modelMessage->id = $model->id;
                    $modelMessage->language = $item['language'];
                    $modelMessage->translation = $item['translation'];
                    if ($modelMessage->validate()) {
                        $modelMessage->save();
                    } else {
                        $modelMessage::deleteAll(['id'=> $model->id]);
                        $model->delete();
                        Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
                        return $this->redirect(Url::toRoute(['create']));
                    }
                }
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute(['view', 'id' => $model->id]));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
            }
        }
        return $this->redirect(Url::toRoute(['create']));
    }

    /**
     * Deletes translation.
     *
     * @param $id
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->accessRules('translation.delete');
        SourceMessage::deleteAll(['id'=>$id]);
        Message::deleteAll(['id' => $id]);
        Yii::$app->session->setFlash('success', Yii::t('admin', 'record deleted'));
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SourceModel::getSourceMessage($id))) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}