<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\modules\cabinet\models\UserModel;
use Yii;
use yii\helpers\Url;

/**
 * Zero controller for the `cabinet` module
 */
class ZeroController extends BackendController
{

    public function actionProfile()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'zero',
            'body'=>'cabinet-body',
            'system_text'=> Yii::t('cabinet', 'Step Zero'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Erstelle deinen Account'),
                'second'=>Yii::t('cabinet', 'Bearbeite deinen Account'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
                'right'=>['visible'=>false, 'link'=>Url::toRoute('/cabinet/dashboard')],
            ],
        ];
        $this->view->params['step'] = $header;

        if(Yii::$app->request->post()) {
            $userModel = new UserModel();
            $username = Yii::$app->request->post('username', '');
            $password = Yii::$app->request->post('password', '');
            $email = Yii::$app->request->post('email', '');
            $coachEmail = Yii::$app->request->post('coach_email', '');
            $userModel->username = $username;
            $userModel->email = $email;
            $userModel->coach_email = $coachEmail;
            $userModel->id = $user->id;
            $userModel->role = $user->role;
            $userModel->status = $user->status;
            $userModel->password_hash = $password ? $password : $user->password_hash;
            if($userModel->validate() && $userModel::saveUser($userModel)) {
                return $this->redirect('/cabinet/dashboard');
            } else {
                $user->username = $username;
                $user->email = $email;
                $user->coach_email = $coachEmail;
            }

        }

        return $this->render('step_zero', ['user'=>$user]);
    }
}
