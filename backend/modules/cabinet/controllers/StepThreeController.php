<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\classes\UserConditionModel;
use backend\models\classes\UserMarketModel;
use backend\models\classes\UserTradeModel;
use backend\models\UserCondition;
use backend\models\UserTrade;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * StepThree controller for the `cabinet` module
 */
class StepThreeController extends BackendController
{

    public function actionIndex()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'three',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Four'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-three/next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_three', ['user'=>$user]);
    }

    public function actionNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'three',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Four'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-three')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-three/do-today')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('next', ['user'=>$user]);
    }

    public function actionDoToday()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'three',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Four'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-three/next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-three/emojy')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('do_today', ['user'=>$user]);
    }


    public function actionEmojy()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $emotions = UserConditionModel::getEmotions();
        $active = true;
        $condition = UserConditionModel::getConditionByUserId($user->id);

        if(empty($condition->emotions)) {
            $condition->emotions = [];
        } else {
            $condition->emotions = json_decode($condition->emotions);
        }

        if(!empty($condition->emotions))
            $active = true;

        $header = [
            'class'=>'three',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Four'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-three/do-today')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-three/congrats'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('emojy', ['user'=>$user, 'emotions'=>$emotions, 'condition'=>$condition]);
    }

    public function actionSaveEmojy()
    {
        $emojy = Yii::$app->request->post('emojy', []);
        if(empty($emojy))
            return $this->asJson(['status' => 'error', 'message'=>'Please fill the fields']);

        $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
        $condition->emotions = json_encode($emojy);

        if(UserConditionModel::saveModel($condition)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status' => 'error', 'message'=>"Please fill the fields"]);
    }

    public function actionCongrats()
    {
        $this->layout = 'congrats';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'three',
            'body'=>'cabinet-body-one',
            'buttons'=> [
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard#research')],
            ],
        ];
        $this->view->params['step'] = $header;
        UserCondition::updateAll(['step_status'=>4], ['user_id'=>$user->id]);

        return $this->render('congrats', ['user'=>$user]);
    }

    public function actionStartTrade()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $active = true;
        $trade = new UserTradeModel();
        $tradeList = UserTradeModel::getActiveTradesByUserId($user->id);
//        $markets = UserMarketModel::getMarketListByUserId($user->id);

        $header = [
            'class'=>'three',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Four'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>false, 'link'=>Url::toRoute('/cabinet/step-three/emojy')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard#research'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('trade', ['user'=>$user, 'tradeList'=>$tradeList, 'trade'=>$trade]);
    }

    public function actionNewTrade()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $active = true;
        $trade = new UserTradeModel();
        $trade->tradeNumber = UserTradeModel::setTradeNumber();
        $trade->startTrade = date('d.m.Y');

        $setupList = UserConditionModel::getSetupListByUserId($user->id);
        $markets = UserMarketModel::getMarketsByUserId($user->id);

        $header = [
            'class'=>'three',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Four'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>false, 'link'=>Url::toRoute('/cabinet/dashboard')],
                'right'=>['visible'=>false, 'link'=>Url::toRoute('/cabinet/step_three/start-trade'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('new_trade', ['markets'=>$markets, 'trade'=>$trade, 'setupList'=>$setupList]);
    }

    public function actionAddTrade()
    {
        $result = [];
        try {


            $user = Yii::$app->user->identity;

            $trade = new UserTradeModel();
            $trade->tradeNumber = UserTradeModel::setTradeNumber();
            $trade->startTrade = date('d.m.Y');

            $setupList = UserConditionModel::getSetupListByUserId($user->id);
            $markets = UserMarketModel::getMarketsByUserId($user->id);

            $result['data'] = $this->renderPartial('_trade_form', ['markets' => $markets, 'trade' => $trade, 'setupList' => $setupList]);
            $result['status'] = 'success';
        }catch (\Exception $e) {
            $result['status'] = 'error';
            $result['error'] = $e->getMessage();
        }
        return$this->asJson($result);
    }

    public function actionSaveTrade()
    {

        $form = Yii::$app->request->post('form', []);
//        echo "<pre>";print_r($form);die;
        $emody = $form['emojy'];
        $emojy = explode(',', $emody);
        $tp = $form['lot'];
        $lot = explode(',', $tp);

        $ticker = UserMarketModel::getMarketByNameAndUser($form['ticker_name'], Yii::$app->user->id);
        $tickerSum = $ticker->tickwert;
        $lots = [];
        $sum = 0;
        foreach ($lot as $value) {
            $price = number_format($value * $tickerSum, 2, '.', '');
            $sum += $price;
            $lots[] = ['tp'=>$value, 'sum'=>$price];
        }
//        $tpSum = $form['']
//        $files = [];

        for($i = 0; $i < 2; $i++) {
            if(array_key_exists($i, $_FILES['file']['name'])) {
                $ext = pathinfo($_FILES['file']['name'][$i]);
                $file = '/images/trades/' . rand(1, 1000) . time() . '.' . $ext['extension'];
                $path = Yii::getAlias('@backend') . '/web' . $file;
                if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $path)) {
                    $files[] = $file;
                }
            }
        }

        if(empty($files))
            return $this->asJson(['status' => 'error', 'message'=>'Please load file']);

        if(empty($form['trade_number']) || empty($form['ticker_name']) || empty($form['setup'])
            || (empty($form['long_amount']) && empty($form['short_amount'])))
            return $this->asJson(['status' => 'error', 'message'=>'Please fill the fields']);

        $trade =  new UserTradeModel();
        $trade->userId = Yii::$app->user->identity->id;
        $trade->tradeNumber = $form['trade_number'];
        $trade->startTrade = strtotime($form['date']);
        $trade->finishTrade = strtotime($form['date']);
        $trade->tickerName = $form['ticker_name'];
        $trade->longAmount = $form['long_amount'] ? 1 : 0;
        $trade->shortAmount = $form['short_amount'] ? 1 : 0;
        $trade->lot = $form['lot_size'];
        $trade->ticksSl = $form['tick'];
        $trade->setupAbbr = $form['setup'];
        $trade->tp = json_encode($lots);
        $trade->status = UserTrade::STATUS_NEW;
        $trade->emojy = json_encode($emojy);
        $trade->files = json_encode($files);
//        if(!empty($trade->tp))
//            $trade->tp = explode(',', $trade->tp);

//        if($trade->shortAmount > $trade->longAmount) {
//            $trade->loss = UserTradeModel::LOSS_TRUE;
//        } else {
//            $trade->loss = UserTradeModel::LOSS_FALSE;
//        }

        $trade->total = $sum;
//echo "<pre>";print_r($trade);die;
        if(UserTradeModel::saveModel($trade)) {
//            $emotions = UserConditionModel::getEmotions();

//            $data = $this->renderPartial('_emojy_form', ['emotions'=>$emotions, 'id'=>$trade->tradeNumber]);
            return $this->asJson(['status'=>'success']);
        }
//die;
        return $this->asJson(['status' => 'error', 'message'=>"Please fill the fields"]);
    }

    public function actionTradeLoss()
    {

        $form = Yii::$app->request->post('form', []);
        $ticker = UserMarketModel::getMarketByNameAndUser($form['ticker_name'], Yii::$app->user->id);
        $tickerSum = $ticker->tickwert;
        $sum = number_format($form['lot_size'] * $form['tick'] * $tickerSum, 2 ,'.', '');

        $files = [];

        for($i = 0; $i < 2; $i++) {
            if(array_key_exists($i, $_FILES['file']['name'])) {
                $ext = pathinfo($_FILES['file']['name'][$i]);
                $file = '/images/trades/' . rand(1, 1000) . time() . '.' . $ext['extension'];
                $path = Yii::getAlias('@backend') . '/web' . $file;
                if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $path)) {
                    $files[] = $file;
                }
            }
        }

        if(empty($files))
            return $this->asJson(['status' => 'error', 'message'=>'Please load file']);

        if(empty($form['trade_number']) || empty($form['ticker_name']) || empty($form['setup'])
            || (empty($form['long_amount']) && empty($form['short_amount'])))
            return $this->asJson(['status' => 'error', 'message'=>'Please fill the fields']);

        $trade =  new UserTradeModel();
        $trade->userId = Yii::$app->user->identity->id;
        $trade->tradeNumber = $form['trade_number'];
        $trade->startTrade = strtotime($form['date']);
        $trade->finishTrade = strtotime($form['date']);
        $trade->tickerName = $form['ticker_name'];
        $trade->longAmount = $form['long_amount'] ? 1 : 0;
        $trade->shortAmount = $form['short_amount'] ? 1 : 0;
        $trade->lot = $form['lot_size'];
        $trade->ticksSl = $form['tick'];
        $trade->setupAbbr = $form['setup'];
        $trade->loss = 1;
        $trade->status = UserTrade::STATUS_NEW;

        $trade->files = json_encode($files);
        $trade->total = $sum;
        if(UserTradeModel::saveModel($trade)) {
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status' => 'error', 'message'=>"Please fill the fields"]);
    }

    public function actionTrade()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;

        $active = true;
        $trade = new UserTradeModel();
        $tradeList = UserTradeModel::getTradesByUserId($user->id);

        $header = [
            'class'=>'three d-flex flex-row',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Four'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
                'right'=>['visible'=>false, 'link'=>Url::toRoute('/cabinet/dashboard#research'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('trade', ['user'=>$user, 'tradeList'=>$tradeList, 'trade'=>$trade]);
    }

    public function actionTradeEmojy()
    {
//        $id = Yii::$app->request->post('id', null);
//        $emojy = Yii::$app->request->post('emojy', null);
//        if($emojy === null)
//            return $this->asJson(['status' => 'error', 'message'=>'Please fill the fields']);


//        $trade = UserTradeModel::getTradeByNumberAndUserId($id, Yii::$app->user->id);
//        if($trade == null)
//            return $this->asJson(['status' => 'error', 'message'=>'Trade was not found']);
        $emotions = UserConditionModel::getEmotions();
        $data = $this->renderPartial('_emojy_form', ['emotions'=>$emotions]);

//            return $this->asJson(['status'=>'success']);
        return $this->asJson(['status' => 'success', 'data'=>$data]);
    }

    public function actionStartWork($id)
    {

        $model = UserTradeModel::getTradeByNumberAndUserId($id, Yii::$app->user->id);

        if($model) {
            $model->status = UserTrade::STATUS_ACTIVE;
            $model->startTrade = strtotime("now");
            UserTradeModel::saveModel($model);
        }

        return$this->redirect(['/cabinet/step-three/trade']);
    }

    public function actionFinishWork($id)
    {
        $model = UserTradeModel::getTradeByNumberAndUserId($id, Yii::$app->user->id);
        $emotions = UserConditionModel::getEmotions();

        $header = [
            'class'=>'three',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Four'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
                'right'=>['visible'=>false, 'link'=>Url::toRoute('/cabinet/dashboard#research')],
            ],
        ];
        $this->view->params['step'] = $header;

        if($model === null)
            throw new NotFoundHttpException(Yii::t('cabinet', 'Trade was not found'));

        return $this->render('finish_trade', ['trade'=>$model, 'emotions'=>$emotions]);

    }

    public function actionSaveFinishWork()
    {
        $id = Yii::$app->request->post('id', null);
        $emojy = Yii::$app->request->post('emojy', null);
        $count = Yii::$app->request->post('count', null);
        if($emojy === null || $count === null || empty($_FILES) || !array_key_exists('file', $_FILES))
            return $this->asJson(['status' => 'error', 'message'=>'Please fill the fields']);
        $files = [];

            for($i = 0; $i < 2; $i++) {
                if(array_key_exists($i, $_FILES['file']['name'])) {
                    $ext = pathinfo($_FILES['file']['name'][$i]);
                    $file = '/images/trades/' . rand(1, 1000) . time() . '.' . $ext['extension'];
                    $path = Yii::getAlias('@backend') . '/web' . $file;
//                    if (file_put_contents($path, $_FILES['file']['tmp_name'][$i])) {
                    if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $path)) {
                        $files[] = $file;
                    }
                }
            }

        if(empty($files))
            return $this->asJson(['status' => 'error', 'message'=>'Please load file']);

        $trade = UserTradeModel::getTradeByNumberAndUserId($id, Yii::$app->user->id);
        if($trade == null)
            return $this->asJson(['status' => 'error', 'message'=>'Trade was not found']);

        $trade->emojy = json_encode($emojy);
        $trade->files = json_encode($files);
        $trade->finishTrade = strtotime("now");
        $trade->status = UserTrade::STATUS_FINISHED;
//        echo "<pre>";print_r($trade);die;
        if(UserTradeModel::saveModel($trade))
            return $this->asJson(['status'=>'success', 'url'=>Url::toRoute(['/cabinet/step-three/trade'])]);
        return $this->asJson(['status' => 'error', 'message'=>'Error save data/ Please try again']);
    }

}
