<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\User;
use backend\modules\cabinet\models\UserModel;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class UserController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $this->accessRules('user.listview');
        $searchModel = new UserModel();
        $dataProvider = $searchModel->searchUserList(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $this->accessRules('user.view');
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->accessRules('user.create');
        $model = new UserModel(['scenario' => 'insert']);
        if (Yii::$app->request->post('UserModel')) {
            $model->load(Yii::$app->request->post());
            if ($model->validate() && $model::saveUser($model)) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'record not saved'));
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Update model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $this->accessRules('user.update');
        $model = $this->findModel($id);

        $oldHash = $model->password_hash;
        $model->password_hash = 'password';

        if (Yii::$app->request->post('UserModel')) {
            $model->load(Yii::$app->request->post());

            if ($model->password_hash != 'password') {
                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
            } else {
                $model->password_hash = $oldHash;
            }

            if ($model->validate() && $model::saveUser($model)) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
                return $this->redirect(Url::toRoute('index'));
            }
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing User model.
     *
     * @param $id
     * @return yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $this->accessRules('user.delete');
            if (UserModel::deleteUser($id)) {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'User deleted'));
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('admin', 'User not deleted'));
            }

        return $this->redirect(Url::toRoute('index'));
    }


    /**
     * Manages user status$model->load(Yii::$app->request->post());
     * @param $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionActive($id)
    {
        $this->accessRules('user.update');
        $model = $this->findModel($id);

        if ($model->status == User::STATUS_ACTIVE) {
            $model->status = User::STATUS_INACTIVE;
            Yii::$app->session->setFlash('success', Yii::t('user', 'User is blocked'));
        } else {
            $model->status = User::STATUS_ACTIVE;
            Yii::$app->session->setFlash('success', Yii::t('user', 'User is unblocked'));
        }

        if (!$model::saveUser($model)) {
            Yii::$app->session->setFlash('success', null);
            Yii::$app->session->setFlash('danger', Yii::t('admin', 'Action failed'));
        }
        return $this->redirect(Url::toRoute('index'));
    }

    public function findModel($id)
    {
        $model = UserModel::getUserById($id);
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
        return $model;
    }
}