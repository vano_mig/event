<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\classes\UserConditionModel;
use backend\modules\cabinet\models\Warning;
use Yii;

/**
 * Default controller for the `cabinet` module
 */
class DashboardController extends BackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $condition = UserConditionModel::getConditionByUserId($user->id);
        $dashboard = Warning::getDashboard($condition);
        $status = $condition->stepStatus;
        return $this->render('index', ['user'=>$user, 'step_status'=>$status, 'dashboard'=>$dashboard]);
    }

}
