<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\classes\CountryModel;
use backend\modules\cabinet\models\LanguageModel;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class CountryController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function actionIndex()
    {
        $this->accessRules('country.listview');
        $model = new CountryModel();
        $dataProvider = $model->searchCountryList(Yii::$app->request->queryParams);
        return $this->render('index', ['model'=>$model, 'dataProvider'=>$dataProvider]);
    }

    /**
     * Displays a single Country model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        $this->accessRules('country.view');
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Country model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException if access deny
     */
    public function actionCreate()
    {
        $this->accessRules('country.create');
        $model = new CountryModel();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model::saveCountry($model)) {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
            return $this->redirect(Url::toRoute('index'));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Country model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     */
    public function actionUpdate($id)
    {
        $this->accessRules('country.update');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && CountryModel::saveCountry($model)) {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'record saved'));
            return $this->redirect(Url::toRoute('index'));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Country model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException if access deny
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $this->accessRules('country.delete');
        CountryModel::deleteCountry($id);
        Yii::$app->session->setFlash('warning', Yii::t('admin', 'record deleted'));
        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CountryModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CountryModel::getCountryById($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}