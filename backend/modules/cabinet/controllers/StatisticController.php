<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\modules\cabinet\models\StatisticModel;
use backend\modules\cabinet\models\UserModel;
use Yii;
use yii\web\NotFoundHttpException;

class StatisticController extends BackendController
{
    public function actionIndex()
    {
        $this->accessRules('statistic.listview');

        $searchModel = new StatisticModel();
        $dataProvider = $searchModel->searchReportList(Yii::$app->request->queryParams);
        $userList = UserModel::getUserActiveList();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'userList' => $userList,
        ]);

    }

    public function actionView($id)
    {
        $this->accessRules('statistic.view');
        $model = StatisticModel::getReportById($id);
        if(!$model)
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));

        return $this->render('view', ['model'=>$model]);
    }

    public function actionAddReport()
    {
        $this->accessRules('statistic.create');

        return $this->render('index');
    }
}