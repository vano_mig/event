<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\classes\UserConditionModel;
use backend\models\classes\UserMarketModel;
use backend\models\UserCondition;
use Yii;
use yii\helpers\Url;
use ZipArchive;

/**
 * StepTwo controller for the `cabinet` module
 */
class StepTwoController extends BackendController
{

    public function actionIndex()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_two', ['user'=>$user]);
    }

    public function actionNext()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/get-ticker')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('step_two_next', ['user'=>$user]);
    }

    public function actionGetTicker()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/next')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/example')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('ticker', ['user'=>$user]);
    }


    public function actionExample()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/get-ticker')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/tick')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('example', ['user'=>$user]);
    }

    public function actionTick()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/example')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/load-example')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('tick', ['user'=>$user]);
    }

    public function actionLoadExample()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/tick')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/explain')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('load', ['user'=>$user]);
    }

    public function actionDownload()
    {
        $files = [
            Yii::getAlias('@backend').'/web/images/system/example.png', Yii::getAlias('@backend').'/web/images/system/example2.png', Yii::getAlias('@backend').'/web/images/system/example3.png'
        ];

        $zipname = 'example.zip';
        $zip = new ZipArchive;
        $zip->open($zipname, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        foreach ($files as $file) {
            $name = basename($file);
            $zip->addFile($file, $name);
        }
        $zip->close();

        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$zipname);
        header('Content-Length: ' . filesize($zipname));
        readfile($zipname);
        exit();

//        foreach ($files as $item) {
//            $file = Yii::getAlias('@backend').'/web/images/system/'.$item;
//            readfile($file);
//
//        }
//        $file = basename($_GET['file']);
//        $file = '/path/to/your/dir/'.$file;
//
//        if(!file_exists($file)){ // file does not exist
//            die('file not found');
//        } else {
//            header("Cache-Control: public");
//            header("Content-Description: File Transfer");
//            header("Content-Disposition: attachment; filename=$file");
//            header("Content-Type: application/zip");
//            header("Content-Transfer-Encoding: binary");
//
//            // read the file from disk
//            readfile($file);
//        }
    }

    public function actionExplain()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/load-example')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/market')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('explain', ['user'=>$user]);
    }

    public function actionMarket()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $active = false;
        $markets = [];
        $condition = UserMarketModel::getActiveMarketsByUserId($user->id);
        if(!empty($condition)) {
            $markets = $condition;
            $active = true;
        } else {
            $markets[] = new UserMarketModel();
        }

        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/explain')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/step-two/super'), 'active'=>$active],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('markets', ['user'=>$user, 'markets'=>$markets]);
    }

    public function actionGetMarket()
    {
        $result = [];
        $result['status'] = 'error';
        $key = Yii::$app->request->post('key');
        $market = new UserMarketModel();
        if($key) {
            $result['status'] = 'success';
            $result['data'] = $this->renderPartial('market_data', ['market'=>$market, 'key'=>$key, 'delete'=>true, 'active'=>true]);
        }

        return $this->asJson($result);

    }

    public function actionSaveMarkets()
    {
        $nameList = Yii::$app->request->post('name', []);
        $shortNameList = Yii::$app->request->post('short_name', []);
        $tickgrobe = Yii::$app->request->post('tickgrobe', []);
        $tickwert = Yii::$app->request->post('tickwert', []);

        if(empty($nameList) || empty($shortNameList) || empty($tickgrobe) || empty($tickwert))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);
        $result = [];
        $markets = [];
        foreach ($nameList as $key => $market) {
            $marketData = UserMarketModel::getMarketByIdAndUserId($key, Yii::$app->user->id);
            $model = new UserMarketModel();
            $model->id = $marketData->id;
            $model->userId = Yii::$app->user->id;
            $model->name = $market;
            $model->shortName = $shortNameList[$key];
            $model->tickgrobe = number_format($tickgrobe[$key], '2', '.', '');
            $model->tickwert = number_format($tickwert[$key], '2', '.', '');
            $markets[] = $model;

        }

        if(empty($markets))
            return $this->asJson(['status'=>'error', 'message'=>'Please fill the fields']);

        if(UserMarketModel::saveModels($markets)) {
            $condition = UserConditionModel::getConditionByUserId(Yii::$app->user->id);
            if($condition->stepStatus < 2) {
                $condition->stepStatus = 2;
                UserConditionModel::saveModel($condition);
            }
            return $this->asJson(['status'=>'success']);
        }

        return $this->asJson(['status'=>'error', 'message'=>'Unable to save data. Please try again']);

    }

    public function actionUpdateMarkets()
    {
        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $markets = [];
        $condition = UserMarketModel::getMarketsByUserId($user->id);

        if(!empty($condition)) {
            $markets = $condition;
        } else {
            $markets[] = new UserMarketModel();
        }

        $header = [
            'class'=>'two',
            'body'=>'cabinet-body-one',
            'system_text'=> Yii::t('cabinet', 'Step Three'),
            'info_text'=> [
                'first'=> Yii::t('cabinet', 'Einen Markt anlegen'),
                'second'=>Yii::t('cabinet', 'Einen Markt bearbeiten'),
            ],
            'buttons'=> [
                'left'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('markets_update', ['user'=>$user, 'markets'=>$markets]);
    }


    public function actionSuper()
    {
        $this->layout = 'congrats';
        $user = Yii::$app->user->identity;
        $header = [
            'class'=>'one',
            'body'=>'cabinet-body-one',
            'buttons'=> [
                'right'=>['visible'=>true, 'link'=>Url::toRoute('/cabinet/dashboard')],
            ],
        ];
        $this->view->params['step'] = $header;
        UserCondition::updateAll(['step_status'=>2], ['user_id'=>$user->id]);

        return $this->render('super', ['user'=>$user]);
    }

}
