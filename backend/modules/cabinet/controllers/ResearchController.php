<?php

namespace backend\modules\cabinet\controllers;

use backend\controllers\BackendController;
use backend\models\classes\UserConditionModel;
use backend\models\UserCondition;
use Yii;
use yii\helpers\Url;

/**
 * Research controller for the `cabinet` module
 */
class ResearchController extends BackendController
{

    public function actionIndex()
    {
//        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
//            'class'=>'research',
//            'body'=>'cabinet-body-one',
//            'system_text'=> Yii::t('cabinet', 'Step Four'),
//            'info_text'=> [
//                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
//                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
//            ],
            'buttons' => [
                'left' => ['visible' => true, 'link' => Url::toRoute('/cabinet/dashboard')],
                'right' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/next')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('research', ['user' => $user]);
    }

    public function actionNext()
    {
//        $this->layout = 'blank';
        $user = Yii::$app->user->identity;
        $header = [
//            'class'=>'three',
//            'body'=>'cabinet-body-one',
//            'system_text'=> Yii::t('cabinet', 'Step Four'),
//            'info_text'=> [
//                'first'=> Yii::t('cabinet', 'Einen Trade aufnehmen'),
//                'second'=>Yii::t('cabinet', 'Einen Trade bearbeiten'),
//            ],
            'buttons' => [
                'left' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research')],
                'right' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/personalize')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('next', ['user' => $user]);
    }

    public function actionPersonalize()
    {
        $user = Yii::$app->user->identity;
        $header = [
            'buttons' => [
                'left' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/next')],
                'right' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/estimate')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('personalize', ['user' => $user]);
    }


    public function actionEstimate()
    {
        $user = Yii::$app->user->identity;

        $header = [
            'buttons' => [
                'left' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/personalize')],
                'right' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/schemas')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('estimate', ['user' => $user]);
    }

    public function actionSchemas()
    {
        $user = Yii::$app->user->identity;

        $header = [
            'buttons' => [
                'left' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/estimate')],
                'right' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/next-schema')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('schemas', ['user' => $user]);
    }

    public function actionNextSchema()
    {
        $user = Yii::$app->user->identity;

        $header = [
            'buttons' => [
                'left' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/schemas')],
                'right' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/result')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('schema_next', ['user' => $user]);
    }

    public function actionResult()
    {
        $user = Yii::$app->user->identity;
        $emotions = UserConditionModel::getEmotions();

        $header = [
            'buttons' => [
                'left' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/next-schema')],
                'right' => ['visible' => true, 'link' => Url::toRoute('/cabinet/research/end')],
            ],
        ];
        $this->view->params['step'] = $header;

        return $this->render('result', ['user' => $user, 'emotions' => $emotions]);
    }

    public function actionEnd()
    {
        $this->layout = 'congrats';
        $user = Yii::$app->user->identity;
        $header = [
//            'class' => 'three',
            'body' => 'cabinet-body-one',
            'buttons' => [
                'right' => ['visible' => true, 'link' => Url::toRoute('/cabinet/step-three/trade')],
            ],
        ];
        $this->view->params['step'] = $header;
        UserCondition::updateAll(['step_status' => 4], ['user_id' => $user->id]);

        return $this->render('end', ['user' => $user]);
    }

}
