<?php
namespace backend\modules\cabinet\widgets\adminLTE;

use yii\base\Widget;

class HeaderWidget extends Widget
{
    public $user;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('@backend/modules/cabinet/views/widgets/views/header/view', [
            'user' => $this->user
        ]);
    }
}