<?php

namespace backend\modules\cabinet\widgets\adminLTE;

use backend\models\User;
use yii\base\Widget;
use Yii;

class LeftWidget extends Widget
{
    public $user;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if (Yii::$app->user->identity->role == User::ROLE_USER) {
            return $this->render('@backend/modules/cabinet/views/widgets/views/left/viewUser', [
                'user' => $this->user
            ]);
        } else {
            return $this->render('@backend/modules/cabinet/views/widgets/views/left/view', [
                'user' => $this->user
            ]);
        }
    }
}