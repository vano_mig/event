<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4 text-bold">
    <div class="title-congrats"><?= Yii::t('cabinet', 'Super !') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div class="text-line-bg"><?=Yii::t('cabinet', 'Du hast deine Lieblingsmärkte nun erfolgreich im')?></div>
        </div>
        <div class="logo">
            <?= Html::img('/images/system/logo1.png', ['class' => 'image']); ?>
        </div>
        <div class="main-name mb-4">
            <?= Html::img('/images/system/tradingj.png', ['class' => 'image']); ?>
        </div>
        <div class="flex-row justify-content-center text-center mt-4">
            <div class="mt-4 pt-4 pb-4"><?=Yii::t('cabinet', 'installiert.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mt-4 mb-4">
            <div class="pt-4 mt-4"><?=Yii::t('cabinet', 'Wir freuen uns auf die weitere Zusammenarbeit mit dir!')?></div>
        </div>
    </div>
</div>
