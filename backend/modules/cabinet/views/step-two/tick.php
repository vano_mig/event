<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Tick - Tickgröße - Tickwert') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4 example">
            <div class="mr-3 border-img"><?=Html::img('/images/system/grafik_left.png')?></div>
            <div class="ml-3 border-img"><?=Html::img('/images/system/grafik_right.png')?></div>
        </div>
    </div>
</div>
