<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Märkte anlegen:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Als Anfänger solltest du dich zunächst auf maximal 2-3 Märkte (diese hast du ja bereits schon in deinen Tradingplan festgelegt) konzentrieren und diese ausgiebig kennen lernen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Wer auf zu vielen Hochzeiten tanzt, stolpert oder rutscht gerne mal aus.')?></div>
            <div><?=Yii::t('cabinet', 'Das Tanzparkett unterscheidet sich von Hochzeit zu Hochzeit, was soviel bedeutet, dass sich unterschiedliche Märkte selbstverständlich auch unterschiedlich verhalten.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4 queue-text text-bold">
            <div><?=Yii::t('cabinet', 'Deshalb ist es sehr wichtig sich auf wenige zu konzentrieren um diese auch sauber zu verstehen.')?></div>
        </div>
    </div>
</div>
