<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Märkte anlegen:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Als Nächstes wirst du nach dem Tickersymbol gefragt.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Was bedeutet das?')?></div>
            <div><?=Yii::t('cabinet', 'Ein Tickersymbol ist die Abkürzung als Bezeichnung eines börsennotiertenUnternehmens in den USA')?></div>
            <div><?=Yii::t('cabinet', 'Dieses Symbol besteht aus 1–4 Buchstaben je nach Art der Börse und Bedeutung des Unternehmens.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Im Aktienbereich ist für die Firma Ford beispielsweise der Buchstabe F reserviert.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'AAPL steht für das Unternehmen Apple.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Im Rohstoffmarkt steht der Ticker CL für Crude Oil. GC steht für Gold, HG für Kupfer...')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Im Indexbereich steht beispielsweise der ES für den Index S&P 500 (dieser Indexsteht für die 500 stärksten Unternehmen der USA)')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Bei den Devisen (Währungspaare) gibt es dann beispielsweise den EUR/USD, USD/ CAD, GBP/USD und viele mehr.')?></div>
        </div>
    </div>
</div>
