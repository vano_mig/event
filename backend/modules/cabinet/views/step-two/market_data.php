<?php

use yii\helpers\Url;

?>
<div class="market-data d-flex" data-lang="<?=Yii::$app->language?>">
    <div class="flex-row">
    <div class="d-flex">
        <label for="name_<?= $key ?>">Name</label>
        <input type="text" name="name[<?= $key ?>]" value="<?= $market->name ?>">
    </div>
    <div class="d-flex pt-2">
        <label for="short_name_<?= $key ?>">Short Name</label>
        <input type="text" name="short_name[<?= $key ?>]" value="<?= $market->shortName ?>">
    </div>
    </div>
    <div class="flex-rox">
        <div class="d-flex">
            <label for="short_name_<?= $key ?>"><?=Yii::t('cabinte', 'Tickgröße')?></label>
            <input type="number" name="tickgrobe[<?= $key ?>]" value="<?= $market->tickgrobe ?>" step="0.01">
        </div>
        <div class="d-flex pt-2">
            <label for="tickwert_<?= $key ?>"><?=Yii::t('cabinte', 'Tickwert')?></label>
            <input type="number" name="tickwert[<?= $key ?>]" value="<?= $market->tickwert ?>" step="any">
        </div>
    </div>
    <?php if ((bool)$delete): ?>
        <button class="bg-danger click-market-rem"><i class="fas fa-minus"></i></button>
    <?php endif ?>
    <?php if ((bool)$active): ?>
        <button class="bg-green click-market-add" data-id="<?= $key + 1 ?>"
                data-url="<?= Url::toRoute('/cabinet/step-two/get-market') ?>"><i class="fas fa-plus"></i></button>
    <?php endif ?>
</div>