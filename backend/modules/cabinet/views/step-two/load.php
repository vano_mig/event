<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Hier bekommst du eine Liste als PDF-Download mit versch. Märkten Mit Tickerangabe - Tickgröße - Tickwert') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Html::img('/images/system/example.png', ['class'=>'image'])?></div>
            <div><?=Html::img('/images/system/example2.png', ['class'=>'image'])?></div>
            <div><?=Html::img('/images/system/example3.png', ['class'=>'image'])?></div>
        </div>
    </div>
    <div class="d-flex flex-row justify-content-center text-center mb-4">
        <a href="<?=Url::toRoute('/cabinet/step-two/download')?>" target="_blank"
           class="btn btn btn-lg btn-grey-light"><?=Yii::t('cabinet', 'Download')?></a>
    </div>
</div>
