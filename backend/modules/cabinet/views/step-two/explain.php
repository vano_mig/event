<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Warum deine Märkte anlegen:') ?></div>
    <div class="step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Warum musst du die kompletten Werte für jeden Markt selbst eintragen?')?></div>
            <div><?=Yii::t('cabinet', 'Warum sind diese Werte nicht bereits schon in der Software integriert?')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pt-4 mb-4">
            <div><?=Yii::t('cabinet', 'Du sollst dir Gedanken über die Märkte und deren Werte machen.')?></div>
            <div><?=Yii::t('cabinet', 'Wir wollen, dass du verstehst um was es hierbei geht..')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Es nützt nichts, wenn du in jedem Markt die gleiche Erwartungshaltung hast.')?></div>
            <div><?=Yii::t('cabinet', 'Was bedeutet das?')?></div>
        </div>
        <div class="flex-row justify-content-center text-center pb-4 mb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Märkte sind unterschiedlich Volatil.')?>
                <?=Yii::t('cabinet', 'Das wiederum bedeutet sie machen unterschiedlich große Bewegungen.')?>
                <?=Yii::t('cabinet', 'Hinzu kommen noch unterschiedliche Tickwerte.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pt-4 mb-4">
            <div><?=Yii::t('cabinet', 'Wenn zum Beispiel das Öl eine Preisveränderung von nur einem Dollar hat, dann bedeutet das, dass sich der Preis um 100 Ticks bewegt!')?></div>
            <div><?=Yii::t('cabinet', '100 x Tickwert von 10 $ = 1000,00 $ mit nur 1 Lot!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Im Beispiel des ES bedeutet die Veränderung von einem Indexpunkt zum nächsten dass sich der Markt um vier Ticks bewegt hat.')?></div>
            <div><?=Yii::t('cabinet', '4 x Tickwert von 12,50 $ = 50,- $ mit 1 Lot')?></div>
        </div>
    </div>
</div>
