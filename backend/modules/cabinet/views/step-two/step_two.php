<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Märkte anlegen:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Nachdem du nun deinen persönlichen Tradingplan gewissenhaft und gut durchdacht erstellt hast, kommen wir nun zum nächsten Schritt in deinem persönlichen TJB.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Um dein TJB auf dich einzustellen ist es nun notwendig dass du deine favorisierten Märkte entsprechend anlegst.')?>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Was bedeutet das?')?></div>
            <div><?=Yii::t('cabinet', 'Nun, zu aller erst musst du hierzu erst einmal die Märkte auskundschaften die für dich und deine Strategie am besten passen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Für Anfänger empfehlen wir hier zu aller erst einmal die weniger volatilen Märkte, also Märkte die einigermaßen ruhig verlaufen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Wir von mytradingtools.de arbeiten sehr viel mit Volumen.')?></div>
            <div><?=Yii::t('cabinet', 'Daher können wir dir in der')?></div>
            <div><?=Yii::t('cabinet', 'Vormittags Session zum Beispiel den FGBL (Euro Bund Future) o der den FESX (Euro STOXX 50 Future) aber natürlich auch den ES (S&P 500 Future) empfehlen.')?></div>
            <div><?=Yii::t('cabinet', 'CL (ÖL) geht auch....')?></div>
        </div>
    </div>
</div>
