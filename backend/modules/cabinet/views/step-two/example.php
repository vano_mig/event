<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Führen wir im folgenden den Vorgang anhand eines Beispiels mit dem Ticker vom ES (S&P 500) durch:') ?></h2>
    <div class="step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4 example">
            <div class="cabinet-block two ex-top">
                <div class="info-block"><span class="pr-4 text-green text-bold">ES</span><?= Yii::t('cabinet', 'Ticker eingeben') ?></div>
                <div class="system-block"><?= Yii::t('cabinet', 'Step Three') ?></div>
            </div>
            <div class="cabinet-block two ex-center">
                <div class="info-block"><span class="pr-4 text-green text-bold">0.25</span><?= Yii::t('cabinet', 'Tickgröße eingeben') ?></div>
                <div class="system-block"><?= Yii::t('cabinet', 'Step Three') ?></div>
            </div>
            <div class="cabinet-block two ex-bottom">
                <div class="info-block"><span class="pr-4 text-green text-bold">12.5</span><?= Yii::t('cabinet', 'Tickwert eingeben') ?></div>
                <div class="system-block"><?= Yii::t('cabinet', 'Step Three') ?></div>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4mb-4">
            <div><?=Yii::t('cabinet', 'Als Nächstes wirst du nach der Tickgröße und nach dem Tickwert gefragt.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pt-4 mb-4">
            <div><?=Yii::t('cabinet', 'Was bedeutet Tick und was ist eine Tickgröße?')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Ein Tick ist die kleinste Schrittweite auf der Stufenleiter der Börsenterminkurse, also die Kursänderungseinheit einer geringstmöglichen Preisbewegung im jeweiligen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center pb-4 mb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Der "tick" als solcher trennt den Abstand zwischen zwei aufeinander folgenden Stufensprossen auf der Skala der möglichen Börsenterminpreise.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pt-4 mb-4">
            <div><?=Yii::t('cabinet', 'Auf der folgenden Seite haben wir dir zwei Beispiele grafisch dargestellt.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Einmal anhand des ES (S&P 500) und einmal anhand des CL (Crude Oil):')?></div>
        </div>
    </div>
</div>
