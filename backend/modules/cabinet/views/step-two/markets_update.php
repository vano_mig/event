<?php

use backend\models\UserMarket;
use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Märkte anlegen:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <form action="" method="post" id="setup_one"
              data-url="<?= Url::toRoute('/cabinet/step-two/save-markets') ?>">
            <input type="hidden" id="param-token" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->csrfToken; ?>">

            <div class="flex-row justify-content-center text-center mb-4 market-list pb-4">
                <?php $count = count($markets) ?>
                <?php foreach ($markets as $key => $market): ?>
                    <?php if($market->status === UserMarket::STATUS_DELETED )
                        continue;
                    ?>
                    <?php $delete = $count == 1 ? false : true?>
                    <?php $add =  ($key + 1) == $count ? true : false?>
                    <?php $numb =  $market->id ? $market->id : 0?>
                    <?=$this->render('market_data', ['market'=>$market, 'key'=>$numb, 'delete'=>$delete, 'active'=>$add])?>


                <?php endforeach; ?>
            </div>
            <div class="col-12 flex-row mt-4">
<!--                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>-->
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
