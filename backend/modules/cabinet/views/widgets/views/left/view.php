<?php

use backend\models\User;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use backend\models\classes\UserModel;

$leftSide = [
    'System' => [
        'name' => Yii::t('admin', 'System'),
        'icon' => 'fas fa-cog nav-icon',
        'active' => true,
        'accordion' => [
            'Users' => [
                'permission' => Yii::$app->user->can('user.listview'),
                'name' => Yii::t('admin', 'Users'),
                'icon' => 'fas fa-user nav-icon',
                'url' => '/cabinet/user',
                'active' => true,
            ],
            'Language' => [
                'permission' => Yii::$app->user->can('language.listview'),
                'name' => Yii::t('admin', 'Language'),
                'icon' => 'fas fa-map-marker-alt nav-icon',
                'url' => '/cabinet/language',
                'active' => true,
            ],
            'Country' => [
                'permission' => Yii::$app->user->can('country.listview'),
                'name' => Yii::t('admin', 'Country'),
                'icon' => 'fas fa-globe-europe nav-icon',
                'url' => '/cabinet/country',
                'active' => true,
            ],
            'Translation' => [
                'permission' => Yii::$app->user->can('translation.listview'),
                'name' => Yii::t('admin', 'Translation'),
                'icon' => 'fas fa-language nav-icon',
                'url' => '/cabinet/translation',
                'active' => true,
            ],
            'Rules' => [
                'permission' => Yii::$app->user->can(User::ROLE_ADMIN),
                'name' => Yii::t('admin', 'Rules'),
                'icon' => 'fas fa-clipboard-check nav-icon',
                'url' => '/cabinet/permission',
                'active' => true,
            ],
            'Profile' => [
                'permission' => Yii::$app->user->isGuest,
                'name' => Yii::t('admin', 'Profile'),
                'icon' => 'fas fa-user nav-icon',
                'url' => '/cabinet/profile',
                'active' => true,
            ],
        ]
    ],
    'Profile' => [
        'permission' => Yii::$app->user->isGuest ? false : true,
        'name' => Yii::t('admin', 'Profile'),
        'icon' => 'fas fa-user nav-icon',
        'url' => '/cabinet/profile',
        'active' => true,
    ],
    'Reports' => [
        'permission' => 'statistic.listview',
        'name' => Yii::t('admin', 'Repots'),
        'icon' => 'fas fa-user nav-icon',
        'url' => '/cabinet/statistic',
        'active' => true,
    ],
]

?>
<aside class="main-sidebar sidebar-dark-primary">

    <a href="<?php echo Url::toRoute('/cabinet/default'); ?>"
       class="brand-link d-flex justify-content-center border-bottom-0">

        <span class="logo-mini logo-xs"><b><?php echo Yii::t('sidebarLogo', 'ENT') ?></b></span>

        <span class="logo-lg brand-text"><?php echo Yii::t('sidebarLogo', 'EVENT') ?></span>

    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-1">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat text-sm"
                data-widget="treeview"
                role="menu" data-accordion="false">
                <?php
                foreach ($leftSide as $key => $value) {
                    try {
                        if ($value['active'] && !empty($value['accordion'])) {
                            $isHidden = true;
                            $preg = "#(";
                            foreach ($value['accordion'] as $aKey => $aValue) {
                                if ($aValue['permission']) {
                                    if ($isHidden === false)
                                        $preg .= '|';
                                    $preg .= "{$aValue['url']}(?:(?:\?|\/)|\b$)";
                                    $isHidden = false;
                                }
                            }
//
                            $preg .= ")#";
                            if ($isHidden === false) {
                                echo '<li class="nav-item has-treeview ' . (preg_match($preg, $_SERVER['REQUEST_URI']) ? "menu-open" : "") . '">';
                                echo '<a href="#" class="' . (preg_match($preg, $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link') . '">
                                        <i class="' . $value['icon'] . '"></i>
                                        <p style="  display: inline-flex;
                                                    align-items: center;
                                                    justify-content: space-between;
                                                    width: 80%;">
                                            <span>' . $value['name'] . '</span>
                                            <i class="right fas fa-angle-left" style="top: inherit; right: 1rem"></i>
                                        </p>
                                    </a>';
                                echo '<ul class="nav nav-treeview">';
                                foreach ($value['accordion'] as $aKey => $aValue) {
                                    if ($aValue['active'] && $aValue['permission']) {
                                        echo '<li class="nav-item">
                                        ' . Html::a(
                                                '<i class="' . $aValue['icon'] . '"></i><p>' . $aValue['name'] . '</p>',
                                                Url::toRoute([$aValue['url']]), ['class' => preg_match("#{$aValue['url']}(?:(?:\?|\/)|\b$)#", $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link']
                                            ) . '
                                    </li>';
                                    }
                                }
                                echo '</ul>';
                                echo '</li>';
                            }
                        } else {
                            if ($value['active'] && $value['permission']) {
                                echo '<li class="nav-item">';
                                echo Html::a(
                                    '<i class="' . $value['icon'] . '"></i><p>' . $value['name'] . '</p>',
                                    Url::toRoute([$value['url']]), ['class' => preg_match("#{$value['url']}(?:(?:!\?|/)$|\b)#", $_SERVER['REQUEST_URI']) ? 'nav-link active' : 'nav-link']
                                );
                                echo '</li>';
                            }
                        }
                    } catch (Exception $e) {
                        throw new yii\web\HttpException('500', $e->getMessage() . '; Line: "' . $e->getLine() . '"; File: "' . $e->getFile() . '"');
                    }
                }
                ?>
            </ul>
            <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>