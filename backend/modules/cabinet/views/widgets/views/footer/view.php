<?php

?>

<footer class="main-footer">
    <div class="container">
        <p class="m-0 footer-center">
            &copy;<?= ' ' . '2019' . ' - ' . date('Y') . ', «' . Yii::t('site', '1IRSTGOLD') . '»' ?>
        </p>
    </div>
</footer>
