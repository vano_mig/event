<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User|null */
/* @var $writer \BaconQrCode\Writer */

use backend\formatter\UserFormatter;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = Yii::t('admin', 'View') . ': ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col mx-auto col-8">
                    <div class="table-responsive">
                        <?php echo DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'username',
                                'email',
                                [
                                    'attribute' => 'role',
                                    'value' => function ($data) {
                                        return UserFormatter::asRole($data->role);
                                    },

                                    'format' => 'html',
                                ],
                                [
                                    'attribute' => 'status',
                                    'value' => function ($data) {
                                        return UserFormatter::asStatus($data->status);
                                    },

                                    'format' => 'html',
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->asDateTime($data->created_at, 'long');
                                    },
                                    'format' => 'html',
                                ],
                                [
                                    'attribute' => 'updated_at',
                                    'value' => function ($data) {
                                        return Yii::$app->formatter->asDateTime($data->updated_at, 'long');
                                    },
                                    'format' => 'html',
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('user.update')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Update'), Url::toRoute(['update', 'id' => $model->id]), ['class' => 'btn bg-gradient-warning btn-block']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('user.delete')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'Delete'), Url::toRoute(['delete', 'id' => $model->id]), ['class' => 'btn bg-gradient-danger btn-block',
                                'data' => [
                                    'confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('user.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


