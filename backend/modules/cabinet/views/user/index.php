<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\UserSearch */
/* @var $model backend\modules\user\models\User */

/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\formatter\UserFormatter;
use backend\models\User;
use backend\modules\cabinet\models\UserModel;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = Yii::t('admin', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('user.create')): ?>
                <?php echo Html::a(Yii::t('admin', 'Create new user'),
                    Url::toRoute(['create']), ['class' => 'btn  bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                        ],
                        [
                            'attribute' => 'username',
                            'value' => function ($data) {
                                return $data->username;
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'email',
                            'value' => function ($data) {
                                return $data->email;
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'role',
                            'value' => function ($data) {
                                return UserFormatter::asRole($data->role);
                            },
                            'format' => 'html',
                            'filter' => UserModel::getRoles(),
                            'filterInputOptions' => ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control'],
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($data) {
                                return UserFormatter::asStatus($data->status);
                            },
                            'format' => 'html',
                            'filter' => UserModel::getStatuses(),
                            'filterInputOptions' => ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control'],
                            'headerOptions' => ['class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center align-middle'],
                        ],
                        [
                            'class' => ActionColumn::class,
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view} {update} {active} {delete}',
                            'buttons' => [
                                'active' => function ($url, $model, $key) {
                                    if ($model->status == User::STATUS_ACTIVE) {
                                        return Html::a('<i class="fas fa-lock"></i>', $url, [
                                            'class' => 'mx-2 text-dark',
                                            'title' => Yii::t('admin', 'Lock'),
                                            'aria-label' => Yii::t('admin', 'Lock'),
                                            'data-pjax' => Yii::t('admin', 'Lock'),
                                            'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to lock this item?'),],
                                        ]);
                                    } else {
                                        return Html::a('<i class="far fa-check-square"></i>', $url, [
                                            'class' => 'mx-2 text-dark',
                                            'title' => Yii::t('admin', 'Unlock'),
                                            'aria-label' => Yii::t('admin', 'Unlock'),
                                            'data-pjax' => Yii::t('admin', 'Unlock'),
                                            'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to unlock this item?'),],
                                        ]);
                                    }
                                },
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-pencil-alt"></i>', $url, [
                                        'class' => 'mx-2 text-warning',
                                        'title' => Yii::t('admin', 'Update'),
                                        'aria-label' => Yii::t('admin', 'Update'),
                                        'data-pjax' => Yii::t('admin', 'Update'),
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fas fa-trash-alt"></i>', $url, [
                                        'class' => 'mx-2 text-dark',
                                        'title' => Yii::t('admin', 'Delete'),
                                        'aria-label' => Yii::t('admin', 'Delete'),
                                        'data-pjax' => Yii::t('admin', 'Delete'),
                                        'data' => ['confirm' => Yii::t('admin', 'Are you sure you want to delete this item?'), 'method' => 'POST'],
                                    ]);
                                },
                            ],
                            'visibleButtons' => [
                                'active' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('user.update', ['user' => $model]);
                                },
                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('user.view', ['user' => $model]);
                                },
                                'update' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('user.update', ['user' => $model]);
                                },
                                'delete' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('user.delete', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
