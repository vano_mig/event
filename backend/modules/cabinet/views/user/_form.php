<?php

/* @var $birthdaySelect array */
/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User|null|yii\db\ActiveRecord */
/* @var $modelProfileUpdate array|backend\modules\user\models\UserProfile|null|yii\db\ActiveRecord */

use backend\models\User;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col min-wr-16 max-wr-30 mx-auto">
            <?php echo $form->field($model, 'email')->textInput()->label(Yii::t('users', 'Email (Login)')) ?>

            <?php echo $form->field($model, 'username')->textInput() ?>

            <?php echo $form->field($model, 'role')->dropDownList(Yii::$app->user->can(User::ROLE_ADMIN) ? $model::getRoles() : $model::getRolesForManager(), ['prompt' => Yii::t('users', 'Select role')]) ?>
            <?php echo $form->field($model, 'status')->dropDownList($model::getStatuses(),
                ['prompt' => Yii::t('users', 'Select status')]); ?>

            <?php echo $form->field($model, 'password_hash', ['template' => "{label}\n{hint}\n{input}<div class='invalid-feedback'></div>", 'options' => ['class' => 'form-group mb-0']])->passwordInput() ?>

        </div>
        <div class="col-12 mt-4 pt-4 border-top">
            <div class="row col-12 col-lg-8 mx-auto">
                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
                </div>
                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']); ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>