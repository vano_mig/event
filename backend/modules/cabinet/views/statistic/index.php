<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cabinet\models\StatisticModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\formatter\UserFormatter;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = Yii::t('admin', 'Reports');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Reports')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <?php if (Yii::$app->user->can('user.create')): ?>
                <?php echo Html::a(Yii::t('admin', 'Create new report'),
                    Url::toRoute(['create']), ['class' => 'btn  bg-gradient-success']) ?>
            <?php endif ?>
            <div class="table-responsive">
                <?php echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                        ],
                        [
                            'attribute' => 'user_id',
                            'value' => function ($data) {
                                return UserFormatter::getUserData($data->user_id);
                            },
                            'format' => 'html',
                            'filter'=>$userList,
                            'filterInputOptions' => ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control'],
                        ],
                        [
                            'attribute' => 'amount',
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'market',
                            'format' => 'html',
//                            'filter' => UserModel::getRoles(),
//                            'filterInputOptions' => ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control'],
                        ],
                        [
                            'attribute' => 'created_at',
                            'format' => 'html',
//                            'filter' => UserModel::getRoles(),
//                            'filterInputOptions' => ['prompt' => Yii::t('admin', '--All--'), 'class' => 'form-control'],
                        ],
                        [
                            'class' => ActionColumn::class,
                            'header' => Yii::t('admin', 'Actions'),
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="far fa-eye"></i>', $url, [
                                        'class' => 'mx-2 text-info',
                                        'title' => Yii::t('admin', 'View'),
                                        'aria-label' => Yii::t('admin', 'View'),
                                        'data-pjax' => Yii::t('admin', 'View'),
                                    ]);
                                },
                            ],
                            'visibleButtons' => [

                                'view' => function ($model, $key, $index) {
                                    return Yii::$app->user->can('statistic.view', ['user' => $model]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
