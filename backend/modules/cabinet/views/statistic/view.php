<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User|null */
/* @var $writer \BaconQrCode\Writer */

use backend\formatter\UserFormatter;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = Yii::t('admin', 'View ') . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'homeLink' => ['label' => Yii::t('admin', 'Reports')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col mx-auto col-8">
                    <div class="table-responsive">
                        <?php echo DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                [
                                    'attribute' => 'userId',
                                    'value' => function ($data) {
                                        return UserFormatter::getUserData($data->userId);
                                    },
                                    'format' => 'html',
                                ],
                                'amount',
                                'market',
                                'createdAt',
                                'comment',
                            ],
                        ]) ?>
                    </div>
                    <img src="<?=$data->file?>" alt="screen">
                </div>
            <div class="col-12 mt-4 pt-4 border-top">
                <div class="row col-12 col-lg-8 mx-auto">
                    <?php if (Yii::$app->user->can('statistic.listview')): ?>
                        <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                            <?php echo Html::a(Yii::t('admin', 'List'), Url::toRoute(['index']), ['class' => 'btn bg-gradient-primary btn-block']) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


