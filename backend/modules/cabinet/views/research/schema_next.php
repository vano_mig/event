<?php

use yii\bootstrap4\Html;

?>
<div class="view-body flex-row justify-content-center text-center mb-4 schema-search">
    <div class="mt-4">
        <div class="justify-content-center text-center mt-4 mb-4 pb-4">
            <div class="d-flex flex-row justify-content-center pb-4">
                <div class="margin-title">
                    Trade Nr.: 0000
                </div>
                <div class="margin-title">
                    Seite 1
                </div>
                <div class="margin-title">
                    Datum: 00.00.0000
                </div>
            </div>
            <div class="d-flex justify-content-center text-center">
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Trefferquote Heute in %') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Trefferquote Aktuelle Woche in %') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Trefferquote Letzte Woche in %') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Trefferquote Aktueller Monat in %') ?></div>
                </div>
            </div>
            <div class="d-flex justify-content-center text-center mt-4">
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Trefferquote Letzter Monat in %') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Trefferquote Aktuelles Jahr in %') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Trefferquote Gesamt in %') ?></div>
                </div>
            </div>

            <div class="mt-3 schema-desc">
                <div class="mt-3 mb-3">
                    <span class="info-message mt-3"><?= Yii::t('cabinet', 'Trefferquote Long-Trades.') ?></span>
                </div>
                <div class="mt-3 mb-3">
                    <span class="info-message mt-3 mb-3"><?= Yii::t('cabinet', 'Trefferquote Short -Trades.') ?></span>
                </div>
                <div class="mt-3 mb-3">
                    <span class="info-message mt-3 mb-3"><?= Yii::t('cabinet', 'Durchnittsnote aller Trades') ?></span>
                </div>
                <div class="mt-4 mt-3"><span class="info-message mt-3 mb-3"><?= Yii::t('cabinet', 'Durchnittsnote aller Trades diese Woche') ?></span></div>
                <div class="mt-4 mt-3"><span class="info-message mt-3 mb-3"><?= Yii::t('cabinet', 'Durchnittsnote aller Trades diese Woche') ?></span></div>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pt-4 mb-4">
            <div class="d-flex flex-row justify-content-center text-center">
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Ergebnis <br>Heute <br>in Ticks + Durchschnitt pro Trade') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Gesamtergebnis Aktuelle Woche in Ticks + Durchschnitt pro Trade') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Gesamtergebnis Letzte Woche in Ticks + Durchschnitt pro Trade') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Gesamtergebnis Aktueller Monat in Ticks + Durchschnitt pro Trade') ?></div>
                </div>
            </div>
            <div class="d-flex row justify-content-center text-center mt-4">
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Gesamtergebnis Letzter Monat in Ticks + Durchschnitt pro Trade') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Gesamtergebnis Aktuelles Jahr in Ticks + Durchschnitt pro Trade') ?></div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Gesamtergebnis Gesamt<br> in Ticks + Durchschnitt pro Trade') ?></div>
                </div>
            </div>
        </div>
    </div>
</div>