<?php

use yii\bootstrap4\Html;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="mt-4">
        <div class="flex-row justify-content-center text-center mb-4 pb-4">
            <div class="d-flex flex-row justify-content-center">
                <div class="margin-title">
                    Trade Nr.: 0000
                </div>
                <div class="margin-title">
                    Seite 1
                </div>
                <div class="margin-title">
                    Datum: 00.00.0000
                </div>
            </div>
            <div class="d-flex flex-row justify-content-center">
                <?= Html::img('/images/system/grafik_zero.png', ['class' => 'grafik']) ?>
                <?= Html::img('/images/system/grafik_one.png', ['class' => 'grafik']) ?>
                <?= Html::img('/images/system/grafik_two.png', ['class' => 'grafik']) ?>
            </div>
            <div class="text-button">
                <?= Yii::t('cabinet', 'Screenshot (2-3 Bildschirme') ?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4 pb-4">
            <div class="d-flex flex-row justify-content-center text-center">
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Trade') ?></div>
                    <div><?= Yii::t('cabinet', 'Einstieg Uhrzeit') ?></div>
                    <div>00:00</div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Ausstieg TP1') ?></div>
                    <div><?= Yii::t('cabinet', 'Uhrzeit') ?></div>
                    <div>00:00</div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Ausstieg TP2') ?></div>
                    <div><?= Yii::t('cabinet', 'Uhrzeit') ?></div>
                    <div>00:00</div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Ausstieg TP3') ?></div>
                    <div><?= Yii::t('cabinet', 'Uhrzeit') ?></div>
                    <div>00:00</div>
                </div>
                <div class="info-message">
                    <div><?= Yii::t('cabinet', 'Ausstieg TP4') ?></div>
                    <div><?= Yii::t('cabinet', 'Uhrzeit') ?></div>
                    <div>00:00</div>
                </div>
            </div>
            <div class="mt-3">
                <span class="info-message"><?= Yii::t('cabinet', 'Gesamte Haltedauer des Trades: 0000 min.') ?></span>
            </div>
            <div class="flex-row justify-content-center text-center mt-4 pt-4 mb-4">
                <div class="d-flex flex-row justify-content-center text-center">
                    <div class="info-message">
                        <div><?= Yii::t('cabinet', 'TP1') ?></div>
                        <div><?= Yii::t('cabinet', 'LOT‘s') ?></div>
                        <div><?= Yii::t('cabinet', 'Ticks') ?></div>
                    </div>
                    <div class="info-message">
                        <div><?= Yii::t('cabinet', 'TP2') ?></div>
                        <div><?= Yii::t('cabinet', 'LOT‘s') ?></div>
                        <div><?= Yii::t('cabinet', 'Ticks') ?></div>
                    </div>
                    <div class="info-message">
                        <div><?= Yii::t('cabinet', 'TP3') ?></div>
                        <div><?= Yii::t('cabinet', 'LOT‘s') ?></div>
                        <div><?= Yii::t('cabinet', 'Ticks') ?></div>
                    </div>
                    <div class="info-message">
                        <div><?= Yii::t('cabinet', 'TP4') ?></div>
                        <div><?= Yii::t('cabinet', 'LOT‘s') ?></div>
                        <div><?= Yii::t('cabinet', 'Ticks') ?></div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-center text-center mt-3">
                    <div>
                        <span class="info-message"> <?= Yii::t('cabinet', 'Gesamtergebnis dieses Trades in Pips') ?></span>
                        <span class="info-message"> <?= Yii::t('cabinet', 'Gesamtergebnis dieses Trades in $') ?></span>
                    </div>
                </div>
            </div>
            <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4 mb-4">
                <div class="d-flex flex-row justify-content-center text-center">
                    <div class="info-message">
                        <div><?= Yii::t('cabinet', 'Emotion') ?></div>
                        <div><?= Yii::t('cabinet', 'vor dem') ?></div>
                        <div><?= Yii::t('cabinet', 'Trade') ?></div>
                    </div>
                    <div class="info-message">
                        <div><?= Yii::t('cabinet', 'Emotion') ?></div>
                        <div><?= Yii::t('cabinet', 'während dem') ?></div>
                        <div><?= Yii::t('cabinet', 'Trade') ?></div>
                    </div>
                    <div class="info-message">
                        <div><?= Yii::t('cabinet', 'Emotion') ?></div>
                        <div><?= Yii::t('cabinet', 'nach dem') ?></div>
                        <div><?= Yii::t('cabinet', 'Trade') ?></div>
                    </div>
                </div>
                <div class="flex-row justify-content-center text-center schemas">
                    <div>
                        <div class="cabinet-block research research-bg">
                            <!--                <div class="system-block">-->
                            <? //= Yii::t('cabinet', 'Step research') ?><!--</div>-->
<!--                            <div class="info-research">--><?//= Yii::t('cabinet', 'Bewerte heir objectiv deinen Trade "Sei dein eigener Coach"') ?><!--</div>-->
                            <?= Yii::t('cabinet', 'Bewerte heir objectiv deinen Trade "Sei dein eigener Coach"') ?>
                        </div>
                    </div>
                    <div class="d-flex flex-row research-list">
                        <div class="cabinet-block research">
                            <!--                <div class="system-block">-->
                            <? //= Yii::t('cabinet', 'Step research') ?><!--</div>-->
                            <div id="schema-info" class="info-block">
                                <div><?= Yii::t('cabinet', 'Benote hier deinen Trade') ?></div>
                                <div><?= Yii::t('cabinet', 'nach dem Schulnotensystem') ?></div>
                                <div><?= Yii::t('cabinet', 'z.B.: 2,5 für eine ordentliche Trade-Ausführung') ?></div>
                                <div><?= Yii::t('cabinet', 'mit etwas ängstlichem Verhalten') ?></div>
                            </div>
                        </div>
                        <div class="cabinet-block research">
                            <!--                <div class="system-block">-->
                            <? //= Yii::t('cabinet', 'Step research') ?><!--</div>-->
                            <div class="info-block"><?= Yii::t('cabinet', 'Erkenntnis für den nächsten Trade') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>