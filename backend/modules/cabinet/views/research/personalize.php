<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Dein persönliches Research') ?></h2>
    <div class="step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4 mb-4">
            <div>
                <?=Yii::t('cabinet', 'Wenn es ein guter Trade war (nicht das Ergebnis zählt) dann bist du zu 100 % nach deinem Tradingplan eingestiegen und hast zu 100 % den Trade bis zum Schluss verwaltet.')?>
                <?=Yii::t('cabinet', 'Also bekommt der Trade auch eine gute Bewertung.')?>
                <?=Yii::t('cabinet', 'Ob es nun ein Gewinner oder ein Verlierer war ist völlig Wurscht!!!.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div>
                <?=Yii::t('cabinet', 'Analysiere deinen Trade und schreibe haargenau in deine Bewertung, ob du ihn sauber, strukturiert und nach Tradingplan ausgeführt hast.')?>
                <?=Yii::t('cabinet', 'Schreibe einen Fehler sofort und ohne schöne Worte in dein persönliches Bewertungsfeld.')?>
                <?=Yii::t('cabinet', 'Schreibe auch auf was du im nächsten Trade Besser machen wirst.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div>
                <?=Yii::t('cabinet', 'Du musst lernen dein eigener Coach zu werden.')?>
            </div>
            <div>
                <?=Yii::t('cabinet', 'Für diese Aufgabe musst du komplett aus deiner Trader-Hülle schlüpfen und das Gewand deines Personal-Trainers überziehen.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4 text-bold text-uppercase">
            <div><?=Yii::t('cabinet', 'DU TUST DAS ALLES NÄMLICH FÜR DICH! NUR FÜR DICH!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4 pb-4 text-bold">
            <div><?=Yii::t('cabinet', 'Dein ICH-Coach ist immer für dich da und kann ein Auge auf dich werfen. Ein fremder Coach kann das nicht!')?></div>
        </div>
    </div>
</div>
</div>
