<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <div class="mt-4">
        <div class="justify-content-center text-center mt-4 mb-4 pb-4">
            <div class="d-flex flex-row justify-content-center pb-4">
                <div class="margin-title">
                    Trade Nr.: 0000
                </div>
                <div class="margin-title">
                    Seite 1
                </div>
                <div class="margin-title">
                    Datum: 00.00.0000
                </div>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4 mb-4">
            <div>
                <span class="info-message"><?= Yii::t('cabinet', 'Während des Trades (wenn möglich) mach dir eine Kurznotiz über deine Emotionen.') ?></span>
            </div>
        </div>

        <div class="flex-row justify-content-center text-center">
            <!--                <div class="cabinet-block-bg setup-form">-->
            <table class="table table-bordered-emojy table">
                <tbody>
                <?php foreach ($emotions as $key => $data): ?>
                    <?php if ($key % 3 == 0): ?>
                        <tr>
                    <?php endif ?>
                    <td>
                        <!-- data-id="<? //=$key?>" class="checkbox-em <?php //if(in_array($key, $condition->emotions)) echo 'border-green' ?>"> -->
                        <ul>
                            <?php foreach ($data as $item): ?>
                                <li><?= $item ?></li>
                            <?php endforeach ?>
                        </ul>
                        <p class="text-bold text-left"><?= Yii::t('cabinet', 'Ergebnis:') ?></p>
                    </td>
                    <?php if (($key + 1) % 3 == 0 || ($key + 1) == count($emotions)): ?>
                        </tr>
                    <?php endif ?>
                <?php endforeach; ?>
                </tbody>
            </table>
            <!--                </div>-->
        </div>
        <div class="d-flex justify-content-center text-center mt-4 mb-4 pb-4">

                <div class="">
                    <span class="info-message"><?= Yii::t('cabinet', 'Trefferquote mit Setup 1') ?></span>
                </div>
                <div >
                   <span class="info-message"><?= Yii::t('cabinet', 'Trefferquote mit Setup 2') ?></span>
                </div>
                <div >
                    <span class="info-message"><?= Yii::t('cabinet', 'Trefferquote mit Setup 3') ?></span>
                </div>

        </div>
        <div class="d-flex justify-content-center text-center mt-4 mb-4 pb-4">
            <div class="bg-white p-4 result justify-content-center text-center">
                <div><?=Yii::t('cabinet', 'Bewertung des Trades von Fremdcoach.')?><br>
                 <?=Yii::t('cabinet', 'Sofern du einen hast......')?></div>
            </div>
        </div>
    </div>
</div>
