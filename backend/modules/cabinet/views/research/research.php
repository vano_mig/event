<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="hello-user"><?= Yii::t('cabinet', 'Hallo {name}', ['name' => $user->username]) ?></div>
    <div class="cabinet-info">
        <div><?= Yii::t('cabinet', 'Willkommen in deinem persönlichen TJB') ?></div>
        <div><?= Yii::t('cabinet', 'Du arbeitest im Moment mit deinem Konto (Kontonummer oder Name)') ?></div>
    </div>
    <div class="mt-4">
        <div class="d-flex flex-row justify-content-center text-center">
            <div class="cabinet-block research" id="research">
                <!--                <div class="system-block">--><?//= Yii::t('cabinet', 'Step research') ?><!--</div>-->
                <div class="info-block"><?= Yii::t('cabinet', 'Meine Research Liste') ?></div>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center pb-4 mb-4">
            <div class="cabinet-block research" id="research">
                <!--                <div class="system-block">--><?//= Yii::t('cabinet', 'Step research') ?><!--</div>-->
                <div class="info-block"><?= Yii::t('cabinet', 'Meine Trade Statistic') ?></div>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mt-4 pt-4">
            <div class="cabinet-block-bg fenster">
                <!--                <div class="system-block">--><?//= Yii::t('cabinet', 'Action-Fenster') ?><!--</div>-->
                <div class="info-block research-block">
                    <p class="block-title"><?= Yii::t('cabinet', 'Your Personal Info Board') ?></p>
                    <div class="block-info-body">
                        <p>
                            <?= Yii::t('cabinet', 'Dieses Board weißt dich auf einen Fehler hin') ?><br><?=Yii::t('cabinet', 'wenn du z.B. gegen diene eigenen.')?>
                        </p>
                        <p>
                            <?= Yii::t('cabinet', 'Regeln aus dienem Tradingplan werstößt, und warhnt dich vor deinen eigenen Ego.') ?>
                        </p>
                        <p>
                            <?= Yii::t('cabinet', 'Außerdem weißt es dich auf fehlende Eintragungen in deiner') ?> <br><?=Yii::t('cabinet', 'Trade-Documentation hin')?>
                        </p>
                        <p>
                            <?= Yii::t('cabinet', 'Immer wieder laufen heir deine Motivationsbilder aus deinem Tradingplan durch.') ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>