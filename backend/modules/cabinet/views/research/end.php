<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-congrats title-big text-bold"><?= Yii::t('cabinet', 'Ende (vorerst)') ?></div>
</div>
