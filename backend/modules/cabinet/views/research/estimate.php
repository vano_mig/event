<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Dein persönliches Research') ?></h2>
    <div class="step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4 mb-4">
            <div>
                <?=Yii::t('cabinet', 'Um deine Trades zu bewerten rufst du zunächst in deiner persönlichen Tradehistorie deinen gewünschten Trade auf.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Diesen ndest du unter folgendem Button')?></div>
            <div class="cabinet-block research" id="research">
                <!--                <div class="system-block">--><?//= Yii::t('cabinet', 'Step research') ?><!--</div>-->
                <div class="info-block"><?= Yii::t('cabinet', 'Meine Research Liste') ?></div>
            </div>
            <div class="mt-4 pt-4"><?=Yii::t('cabinet', 'Anschließend erscheint eine Tabelle')?></div>
            <div>
                <table class="table table-research table-responsive">
                    <thead>
                    <tr>
                        <th><?=Yii::t('cabinet', 'Trade-Nr.')?></th>
                        <th><?=Yii::t('cabinet', 'Datum')?></th>
                        <th><?=Yii::t('cabinet', 'Uhrzeit (Zeit des Einstiegs)')?></th>
                        <th><?=Yii::t('cabinet', 'Ticker')?></th>
                        <th><?=Yii::t('cabinet', 'Status Selfcoach')?></th>
                        <th><?=Yii::t('cabinet', 'Status Fremdcoach')?></th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1232</td>
                            <td>05.03.2020</td>
                            <td>10:15</td>
                            <td>ES</td>
                            <td><?=Html::img('/images/system/green_circle.png')?></td>
                            <td><?=Html::img('/images/system/red_circle.png')?></td>
                        </tr>
                        <tr>
                            <td>1233</td>
                            <td>05.03.2020</td>
                            <td>11:20</td>
                            <td>ES</td>
                            <td><?=Html::img('/images/system/red_circle.png')?></td>
                            <td><?=Html::img('/images/system/red_circle.png')?></td>
                        </tr>
                        <tr>
                            <td>1234</td>
                            <td>05.03.2020</td>
                            <td>9:34</td>
                            <td>SL</td>
                            <td><?=Html::img('/images/system/red_circle.png')?></td>
                            <td><?=Html::img('/images/system/red_circle.png')?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div>
                <?=Yii::t('cabinet', 'In deiner Researchliste ndest du dann deine Trades die du schon beurteilt hast und vor allem die, die du noch unbedingt bewerten musst.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Grün bedeutet: Der Trade wurde bewertet Rot bedeutet: Der Trade muss dringend noch beurteilt werden.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4 pb-4">
            <div><?=Yii::t('cabinet', 'Durch drücken auf den roten Button in der Statusliste gelangst du direkt zur jeweiligen Trade-Dokumentation und zum Bewertungsfeld.')?></div>
        </div>
    </div>
</div>
</div>
