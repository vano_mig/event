<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Dein persönliches Research') ?></h2>
    <div class="step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4 mb-4">
            <div>
                <?=Yii::t('cabinet', 'Nun kommen wir zum eigentlichen Kern unseres gesamten Vorhabens.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Gleichzeitig ist dieser Schritt für viele der schwierigste von allen!')?></div>
            <div><?=Yii::t('cabinet', 'Aber mach dir keine Sorgen, mit etwas Übung kriegst du das hin!')?></div>
            <div><?=Yii::t('cabinet', 'Es ist noch kein Meister vom Himmel gefallen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div>
                <?=Yii::t('cabinet', 'Du musst jetzt deine eigenen Trades objektiv und unvoreingenommen bewerten. Keine Schönrederei, kein selber anlügen......')?>
                 <?=Yii::t('cabinet', 'Objektiv und schonungslos!')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Sei ehrlich zu dir selbst.')?></div>
            <div><?=Yii::t('cabinet', 'Jedes „ach das passt schon“ oder „ist ja nicht so schlimm, ist ja nur Demo“ ist verboten!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4 pb-4 text-uppercase">
            <div><?=Yii::t('cabinet', 'DEMO IST WIE ECHTGELD - SONST IST ES KEIN DEMO!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center pt-4  mt-4 mb-4 text-bold">
            <div><?=Yii::t('cabinet', 'Sei ehrlich zu dir selbst!!!!!!!!')?></div>
            <div><?=Yii::t('cabinet', 'Es geht nicht darum es uns von mytradingtools recht zu machen oder deinem Coach.')?></div>
            <div><?=Yii::t('cabinet', 'Es geht darum es für dich selbst recht zu machen!')?></div>
            </div>
        </div>
    </div>
</div>
