<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-congrats title-big text-bold"><?= Yii::t('cabinet', 'Herzlichen Glückwunsch !') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4 nt-4">
            <div class="text-line-bg pl-4 pr-4"><?=Yii::t('cabinet', 'Du hast nun alle Prozesse der Tradeaufnahme erfolgreich gemeistert! Respekt!')?></div>
            <div class="text-line-bg pl-4 pr-4"><?=Yii::t('cabinet', 'Du hast einen großartigen Willen der Dich Step by Step ans Ziel führt!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4 pt-4">
            <div class="text-line-bg"><?=Yii::t('cabinet', 'Auf geht“s zum nächsten Schritt gemeinsam mit deinem persönlichen')?></div>
        </div>
        <div class="logo">
            <?= Html::img('/images/system/logo1.png', ['class' => 'image']); ?>
        </div>
        <div class="main-name mb-4">
            <?= Html::img('/images/system/tradingj.png', ['class' => 'image']); ?>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 text-bold">
            <h1 class="mt-4 pt-4 pb-4 text-bold"><?=Yii::t('cabinet', 'Das Research')?></h1>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div class=""><?=Yii::t('cabinet', 'Das bewerten deiner eigenen Trades ist der eigentliche Lernprozess deines Tradings.')?></div>
            <div class=""><?=Yii::t('cabinet', 'Ab jetzt und für immer!')?></div>
            <div class=""><?=Yii::t('cabinet', 'Werde dein eigener Coach!')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div class="pt-4 mt-4"><?=Yii::t('cabinet', 'Viel Spaß dabei!')?></div>
        </div>
    </div>
</div>
