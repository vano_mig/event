<?php

use backend\models\UserMarket;
use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Trade №') ?> <?=$trade->tradeNumber?></h2>
    <div class="step-one-text">
        <form action="" method="post" id="step-three-finish" enctype="multipart/form-data"
              data-url="<?= Url::toRoute('/cabinet/step-three/save-finish-work') ?>">
            <input type="hidden" id="param-token" name="<?= Yii::$app->request->csrfParam; ?>"
                   value="<?= Yii::$app->request->csrfToken; ?>">
            <input type="hidden" id="id" name="id"
                   value="<?= $trade->tradeNumber; ?>">
            <div class="d-flex flex-row text-center mt-4 research-list">
                <div class="screen-block mr-2" data-id="1">
                    <div class="profile-content">
                        <div><?= Yii::t('cabinet', 'Load screenshot.') ?></div>
                        <div class="mt-3 mb-1 filename"></div>
                        <input type="file" name="file[]" placeholder="file" id="photo1" class="screenshot">
                    </div>

                </div>
                <div class="screen-block mr-2 ml-2" data-id="2">
                    <div class="profile-content">
                        <div><?= Yii::t('cabinet', 'Load screenshot') ?></div>
                        <div class="mt-3 mb-1 filename"></div>
                        <input type="file" name="file[]" placeholder="file" id="photo2" class="screenshot">
                    </div>
                </div>
                <div class="screen-block ml-2" data-id="3">
                    <div class="profile-content">
                        <div><?= Yii::t('cabinet', 'Load screenshot.') ?></div>
                        <div class="mt-3 mb-1 filename"></div>
                        <input type="file" name="file[]" placeholder="file" id="photo3" class="screenshot">
                    </div>
                </div>
            </div>
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form trade-form">
                    <h4 class="mt-3 text-bold"><?= Yii::t('cabinet', 'Deine Gefühle vor dem Trade') ?></h4>
                    <div>
                        <div><?= Yii::t('cabinet', 'Wie hast du dich gefühlt bevor du in den Trade eingestiegen bist:') ?></div>
                        <div><?= Yii::t('cabinet', 'max. zwei Möglichkeiten sind durch anklicken wählbar)') ?></div>
                    </div>
                    <table class="table table-bordered-emojy table">
                        <tbody>
                        <?php foreach ($emotions as $key => $data): ?>
                            <?php if ($key % 3 == 0): ?>
                                <tr>
                            <?php endif ?>
                            <!--                            <td  data-id="--><?//=$key?><!--" class="checkbox-em --><?php //if(in_array($key, $condition->emotions)) echo 'border-green' ?><!--">-->
                            <td>
                                <ul>
                                    <?php foreach ($data as $index =>$item): ?>
                                        <li data-id="<?=$index?>" class="checkbox-em"><?= $item ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </td>
                            <?php if (($key + 1) % 3 == 0 || ($key + 1) == count($emotions)): ?>
                                </tr>
                            <?php endif ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
