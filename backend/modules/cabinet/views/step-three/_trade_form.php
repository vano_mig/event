<?php

use backend\models\UserMarket;
use yii\helpers\Url;

?>
<!--<div id="form-trade">-->
<form action="" method="post" id="trade-setup" enctype="multipart/form-data" multiple="multiple" data-url="<?= Url::toRoute('/cabinet/step-three/save-trade') ?>">
    <input type="hidden" id="param-token" name="<?= Yii::$app->request->csrfParam; ?>"
           value="<?= Yii::$app->request->csrfToken; ?>">
    <div class="flex-row text-center mt-4">
        <div class="cabinet-block-bg setup-form pt-3 pb-3 trade-form">
            <div class="trade">
                <label for="trade_number"><?= Yii::t('trade', 'Fortlaufende Trade №') ?></label>
                <input type="text" id="trade_number" name="trade_number" placeholder="000000" value="<?=$trade->tradeNumber?>" readonly>
                <button class="btn btn-dark new-trade" data-url="<?=Url::toRoute('/cabinet/step-three/add-trade')?>"><i class="fas fa-plus"></i></button>
            </div>
            <div class="trade">
                <label for="datum"><?= Yii::t('trade', 'Datum') ?></label>
                <input type="text" id="datum" name="trade_start" value="<?=$trade->startTrade?>" placeholder="00.00.0000" readonly>
            </div>
            <div class="trade">
                <label for="market"><?= Yii::t('trade', 'Ticker') ?>
                    <span class="text-sm"> <?= Yii::t('trade', '(welcher Markt)') ?></span></label>
                <select id="market" name="ticker_name">
                    <option value="">---</option>
                    <?php if (!empty($markets)): ?>
                        <?php foreach ($markets as $item): ?>
                            <?php if ($item->status == UserMarket::STATUS_DELETED): ?>
                                <?php continue; ?>
                            <?php endif ?>
                            <option value="<?= $item->name ?>"
                                    data-name="<?= $item->shortName ?>" data-price="<?=$item->tickwert?>"
                                data-ticker="<?=$item->tickgrobe?>"><?= $item->name ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="trade">
                <label for="screenshot" class="text-orange"><?= Yii::t('trade', 'Screenshots') ?></label>
                <div class="trade-block">
                    <span class="files-span">files</span>
                    <input type="file" id="screenshot_<?=$trade->tradeNumber?>" class="filename files-input" name="file[]" value="" multiple>
                </div>
            </div>
            <div class="trade">
                <label for="setup"><?= Yii::t('trade', 'Welches Setup') ?></label>
                <select id="setup" name="setup_abbr">
                    <option value="">---</option>
                    <?php if (!empty($setupList)): ?>
                        <?php foreach ($setupList as $setup): ?>
                            <option value="<?= $setup ?>"><?= $setup ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="trade mt-2">
                <label for="long_amount" class="text-success"><?= Yii::t('trade', 'LONG Einstiegspreis') ?></label>
                <input type="checkbox" id="long_amount" class="long_amount" name="long_amount" value="1">
            </div>
            <div class="trade">
                <label for="short_amount" class="text-danger"><?= Yii::t('trade', 'SHORT Einstiegspreis') ?></label>
                <input type="checkbox" id="short_amount" class="short_amount" name="short_amount" value="2">
            </div>
            <div class="trade mt-2">
                <label for="lot_size"><?= Yii::t('trade', 'LOT-SIZE') ?></label>
                <input type="number" id="lot_size" name="lot" placeholder="000" value="">
            </div>
            <div class="trade mb-2">
                <label for="tick"><?= Yii::t('trade', 'Stop Loss in Pips/Ticks') ?></label>
                <input type="number" id="tick" name="tick" placeholder="000" value="">
                <button type="submit" class="btn btn-danger btn-loss" data-id="stop" data-url="<?=Url::toRoute('/cabinet/step-three/trade-loss')?>"><?= Yii::t('trade', 'Loss') ?></button>
            </div>
            <?php for ($i = 1; $i < 6; $i++): ?>
                <div class="trade">
                    <label for="lot_<?=$i?>"><?= Yii::t('trade', 'TP {n} in Pips', ['n' => $i]) ?></label>
                    <input type="number" id="lot_<?=$i?>" data-id="<?=$i?>" class="lots" name="tp[<?= $i ?>]" placeholder="000" value="">
                    <input type="number" id="lot_sum_<?=$i?>" class="lots-sum" name="tp_sum[<?= $i ?>]" value="" readonly>
                    <?php if ($i == 5): ?>
                        <button type="submit" class="btn btn-success btn-save-trade" data-url="<?=Url::toRoute('/cabinet/step-three/trade-emojy')?>"><?= Yii::t('trade', 'Fertig') ?></button>
                    <?php endif ?>
                </div>
            <?php endfor ?>
        </div>
    </div>
</form>
<!--</div>-->