<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Html::img('/images/system/step_3_schema.png', ['class'=>'image-scheme'])?></div>
        </div>
    </div>
</div>
