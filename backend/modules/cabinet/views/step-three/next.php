<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Beispiel eines ausskalierten Trades im ES (S&P 500)') ?></h2>
    <div class="step-one-text step-bottom tick">
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4 mb-4 text-bold">
            <div><?=Yii::t('cabinet', 'Der Trade wurde mit 4 LOTˋs gestartet:')?></div>
        </div>


        <div class="flex-row d-flex text-center mt-4 pt-4 mb-4 taste">
            <div class="flex-row justify-content-center text-center"><div class="block-t block-t-pad"><span>T</span></div></div>
            <div class="ml-4 pl-4"><?=Yii::t('cabinet', 'Taste „T“ drücken')?></div>
        </div>
        <div class="flex-row d-flex text-center mt-4 pt-4 mb-4 taste">
            <div class="block-g"><span>Teil. gewinn 1</span></div>
            <div>
            <div class="ml-4 pl-4 text-left"><?=Yii::t('cabinet', 'TJB fragt dich jetzt nach deinem 1. Teilgewinn in Ticks. Z.B.:')?>
                <span class="float-right text-bold"><?=Yii::t('cabinet', '7 Ticks')?></span>
            </div>
            <div class="ml-4 pl-4 pt-4 mt-4 text-left"><?=Yii::t('cabinet', 'Danach wird sofort die LOT Anzahl abgefragt die du an dieser Stelle aus dem Markt genommen hast.')?>
                <span class="float-right text-bold"><?=Yii::t('cabinet', '2 LOTˋs')?></span>
            </div>
            </div>
<!--            <div class="block-t float-left"><span>T</span></div>-->
        </div>
        <div class="block-t position-block"><span>T</span></div>

        <div class="flex-row d-flex text-center mt-4 pt-4 mb-4 taste">
            <div class="block-g"><span>Teil. gewinn 2</span></div>
            <div>
                <div class="ml-4 pl-4 text-left"><?=Yii::t('cabinet', 'TJB fragt dich jetzt nach deinem 2. Teilgewinn in Ticks. Z.B.:')?>
                    <span class="float-right text-bold"><?=Yii::t('cabinet', '12 Ticks')?></span>
                </div>
                <div class="ml-4 pl-4 pt-4 mt-4 text-left"><?=Yii::t('cabinet', 'Danach wird sofort die LOT Anzahl abgefragt die du an dieser Stelle aus dem Markt genommen hast.')?>
                    <span class="float-right text-bold"><?=Yii::t('cabinet', '1 LOTˋs')?></span>
                </div>
            </div>
            <!--            <div class="block-t float-left"><span>T</span></div>-->
        </div>
        <div class="block-t position-block"><span>T</span></div>

        <div class="flex-row d-flex text-center mt-4 pt-4 mb-4 taste">
            <div class="block-g"><span>Teil. gewinn 3</span></div>
            <div>
                <div class="ml-4 pl-4 text-left"><?=Yii::t('cabinet', 'TJB fragt dich jetzt nach deinem 3. Teilgewinn in Ticks. Z.B.:')?>
                    <span class="float-right text-bold"><?=Yii::t('cabinet', '1 Ticks')?></span>
                </div>
                <div class="ml-4 pl-4 pt-4 mt-4 text-left"><?=Yii::t('cabinet', 'Danach wird sofort die LOT Anzahl abgefragt die du an dieser Stelle aus dem Markt genommen hast.')?>
                    <span class="float-right text-bold"><?=Yii::t('cabinet', '1 LOTˋs')?></span>
                </div>
            </div>
            <!--            <div class="block-t float-left"><span>T</span></div>-->
        </div>



        <div class="flex-row justify-content-center text-center mb-4 taste border-block">
            <div><?=Yii::t('cabinet', 'Ergebnis dieses Trades nach 3 Teilgewinnen:')?></div>
            <table class="table total-table">
                <tbody>
                <tr>
                    <td>1 Teilgewinn:</td>
                    <td>2 LOT a 12,50 $ x 7 Ticks</td>
                    <td>= 175,00 $</td>
                </tr>
                <tr>
                    <td>2 Teilgewinn:</td>
                    <td>1 LOT a 12,50 $ x 12 Ticks</td>
                    <td>= 175,00 $</td>
                </tr>
                <tr>
                    <td>3 Teilgewinn:</td>
                    <td>1 LOT a 12,50 $ x 1 Ticks</td>
                    <td>= 175,00 $</td>
                </tr>
                </tbody>
            </table>
            <div class="text-right pr-4"><?=Yii::t('cabinet', 'Gesamt = 337,50 $')?></div>
        </div>
    </div>
</div>
