<?php

use backend\models\UserTrade;
use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Your active trades') ?></h2>
    <div class="step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4mb-4">
            <div>
                <?=Html::a(Yii::t('cabinet', 'Neuer Trade'), Url::toRoute('/cabinet/step-three/new-trade'), ['class'=>'btn btn-grey'])?>
                <?php if(empty($tradeList)):?>
                    <div class="market-data d-flex flex-row text-center justify-content-center">
                        <h4><?=Yii::t('cabinet', 'Active trades was not found...') ?></h4>
                    </div>
                <?php else :?>
                <table class="table table-trade mt-4">
                    <thead>
                    <tr>
                        <th><?=Yii::t('cabinet', 'Trade №')?></th>
                        <th><?=Yii::t('cabinet', 'Short name')?></th>
                        <th><?=Yii::t('cabinet', 'Lot')?></th>
                        <th><?=Yii::t('cabinet', 'Stop LIst')?></th>
                        <th><?=Yii::t('cabinet', 'SetupAbbr')?></th>
                        <th><?=Yii::t('cabinet', 'Status')?></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($tradeList as $trade):?>
                        <tr>
                            <td><?=$trade->tradeNumber?></td>
                            <td><?=$trade->tickerShortName?></td>
                            <td><?=$trade->lot?></td>
                            <td><?=$trade->ticksSl?></td>
                            <td><?=$trade->setupAbbr?></td>
                            <td>
                                <?php if($trade->status == UserTrade::STATUS_FINISHED):?>
                                    <?=Html::tag('span', Yii::t('cabinet', 'Finished'), ['class'=>'bg-green pl-2 pr-2 pt-1 pb-1']) ?>
                                <?php elseif($trade->status == UserTrade::STATUS_ACTIVE):?>
                                    <?=Html::tag('span', Yii::t('cabinet', 'Active'), ['class'=>'bg-danger pl-2 pr-2 pt-1 pb-1'])?>
                                <?php else :?>
                                    <?=Html::tag('span', Yii::t('cabinet', 'New'), ['class'=>'bg-secondary pl-2 pr-2 pt-1 pb-1'])?>
                                <?php endif?>
                            </td>
                            <td>
                                <?php if($trade->status == UserTrade::STATUS_FINISHED):?>
<!--                                    --><?//=Html::tag('span', Yii::t('cabinet', 'Finished'), ['class'=>'bg-green pl-2 pr-2 pt-1 pb-1']) ?>
                                <?php elseif($trade->status == UserTrade::STATUS_ACTIVE):?>
                                    <a href="<?=Url::toRoute(['/cabinet/step-three/finish-work', 'id'=>$trade->tradeNumber])?>"
                                    ><span class="text-danger"><i class="fas fa-stop" title="<?=Yii::t('cabinet', 'Finish')?>"></i></span></a>
                                <?php else :?>
                                    <a href="<?=Url::toRoute(['/cabinet/step-three/start-work', 'id'=>$trade->tradeNumber])?>">
                                        <span class="text-green"><i class="fas fa-play" title="<?=Yii::t('cabinet', 'Start')?>"></i></span></a>
                                <?php endif?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <?php endif ?>
                <div>

                </div>
            </div>

        </div>
    </div>
</div>
