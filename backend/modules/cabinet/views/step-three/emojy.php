<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Gefühle......') ?></h2>
    <div class="step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4mb-4">
            <div>
                <?= Yii::t('cabinet', 'Während des Trades (wenn möglich) mach dir eine Kurznotiz über deine Emotionen.') ?>
                <?= Yii::t('cabinet', 'Diese solltest du gleich nach dem Trade oder nach der Session in dein TJB eintragen.') ?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div><?= Yii::t('cabinet', 'Wie hast du dich vor dem Trade gefühlt?') ?></div>
            <div><?= Yii::t('cabinet', 'Wie hast du dich während des Trades gefühlt?') ?></div>
            <div><?= Yii::t('cabinet', 'Wie hast du dich nach dem Trade gefühlt?') ?></div>
        </div>
        <div class="flex-row justify-content-center text-center pb-4 mb-4">
            <div>
                - <?= Yii::t('cabinet', 'Hierfür gibt dir TJB über vorgegebene Reiter folgende Eintragungsmöglichkeiten vor:') ?></div>
        </div>
        <form action="" method="post" id="emotions" data-url="<?= Url::toRoute('/cabinet/step-three/save-emojy') ?>">
            <input type="hidden" id="param-token" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->csrfToken; ?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form height-block">
                    <h4 class="mt-3 text-bold"><?= Yii::t('cabinet', 'Deine Gefühle vor dem Trade') ?></h4>
                    <div>
                        <div><?= Yii::t('cabinet', 'Wie hast du dich gefühlt bevor du in den Trade eingestiegen bist:') ?></div>
                        <div><?= Yii::t('cabinet', 'max. zwei Möglichkeiten sind durch anklicken wählbar)') ?></div>
                    </div>
                    <table class="table table-bordered-emojy table">
                        <tbody>
                        <?php foreach ($emotions as $key => $data): ?>
                            <?php if ($key % 3 == 0): ?>
                                <tr>
                            <?php endif ?>
<!--                            <td  data-id="--><?//=$key?><!--" class="checkbox-em --><?php //if(in_array($key, $condition->emotions)) echo 'border-green' ?><!--">-->
                            <td  data-id="<?=$key?>" class="checkbox-em">
                                <ul>
                                    <?php foreach ($data as $item): ?>
                                        <li><?= $item ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </td>
                            <?php if (($key + 1) % 3 == 0 || ($key + 1) == count($emotions)): ?>
                                </tr>
                            <?php endif ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
<!--            <div class="col-12 flex-row ">-->
<!--                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>-->
<!--                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>-->
<!--            </div>-->
        </form>
    </div>
</div>
