<?php

use backend\models\UserMarket;
use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('trade', 'Trade-Aufnahmefenster') ?></h2>
    <div class="step-one-text trade-body">
        <?= $this->render('_trade_form', ['markets'=>$markets, 'trade'=>$trade, 'setupList'=>$setupList])?>
    </div>
</div>
