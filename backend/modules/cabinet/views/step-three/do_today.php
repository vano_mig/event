<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mt-4 pt-4 mb-4">
    <h2 class="mt-4 pt-4 text-bold "><?= Yii::t('cabinet', 'Was du heute kannst besorgen, das verschiebe nicht auf morgen') ?></h2>
    <div class="step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mt-4 pt-4 pb-4mb-4">
            <div>
                <?=Yii::t('cabinet', 'Achte immer darauf, dass du deine Trades zeitnah (am besten direkt nach dem Trade) vervollständigst und beurteilst.')?>
                <?=Yii::t('cabinet', 'So kannst du dich am besten an die Situationen, die sich im laufe des Trades ergeben haben, erinnern.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 pb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Wenn du dich konsequent daran hältst hat das viele Vorteile:')?></div>
            <div>- <?=Yii::t('cabinet', 'Du vermeidest Overtrading und die berühmt berüchtigten Rachetrades')?></div>
        </div>
        <div class="flex-row justify-content-center text-center pb-4 mb-4">
            <div>- <?=Yii::t('cabinet', 'Du fährst wieder etwas runter und bekommst dadurch deine Emotionen wieder auf ein „Normallevel“.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center pb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Du erkennst eventuell gleich einen Fehler und kannst diesen bei deinem nächsten Trade vermeiden.......')?></div>
        </div>
        <div class="flex-row justify-content-center text-center pb-4 mb-4">
            <div><?=Yii::t('cabinet', 'Das wichtigste dabei ist aber, wie bereits schon erwähnt, dass du dabei deine Emotionen re ektierst und dich entsprechend danach verhältst.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center pb-4 mb-4">
            <div>
                <?=Yii::t('cabinet', 'War es ein Super-Trade solltest du darüber nachdenken und den Entschluss fassen Feierabend für heute zu machen.')?>
                <?=Yii::t('cabinet', 'Freu Dich und genieße den Tag.')?>
            </div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div>
                <?=Yii::t('cabinet', 'War es ein Minus-Trade?')?>
                <?=Yii::t('cabinet', 'Setz dir einen Spiegel vor und analysiere ob der Fehler an dir lag, oder ob du alles korrekt nach Tradingplan umgesetzt hast.')?>
                <?=Yii::t('cabinet', 'Entscheide dann ob du einen weiteren Trade planst oder ob du eine Pause einlegst oder sogar die Session für heute beendest.')?>
            </div>
        </div>
    </div>
</div>
