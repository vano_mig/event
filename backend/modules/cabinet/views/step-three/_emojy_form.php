<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<!--<form action="" method="post" id="emotions-finish" data-url="--><?//= Url::toRoute('/cabinet/step-three/save-trade-emojy') ?><!--">-->
    <h4 class="mt-2"><?= Yii::t('cabinet', 'Deine Gefühle vor dem Trade') ?></h4>
<!--    <input type="hidden" id="param-token" name="--><?//= Yii::$app->request->csrfParam; ?><!--"-->
<!--           value="--><?//= Yii::$app->request->csrfToken; ?><!--">-->
<!--    <input type="hidden" name="id" value="--><?//=$id?><!--">-->
<table class="table table-bordered-emojy table">
    <tbody>
    <?php foreach ($emotions as $key => $data): ?>
        <?php if ($key % 2 == 0): ?>
            <tr>
        <?php endif ?>
        <!--                            <td  data-id="--><? //=$key?><!--" class="checkbox-em --><?php //if(in_array($key, $condition->emotions)) echo 'border-green' ?><!--">-->
        <td  >
            <ul>
                <?php foreach ($data as $index=>$item): ?>
                    <li data-id="<?= $index ?>" class="checkbox-em"><?= $item ?></li>
                <?php endforeach ?>
            </ul>
        </td>
        <?php if (($key + 1) % 2 == 0 || ($key + 1) == count($emotions)): ?>
            </tr>
        <?php endif ?>
    <?php endforeach; ?>
    </tbody>
</table>
<button type="submit" class="btn btn-success finish-trade"><?=Yii::t('trade', 'Finish')?></button>
</form>
