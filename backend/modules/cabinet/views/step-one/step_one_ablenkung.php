<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Keine Ablenkung!') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Bevor es ins aktive Trading geht und man sich auf den <br> Trading-Modus einstimmt, müssen sämtliche <br> Ablenkungsfaktoren radikal entfernt werden.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Dazu zählen Störgeräusche von lauten Fernsehern, Handys oder anderen Tönen, die einen klaren und fokussierten Gedanken verhindern könnten.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Auch Computerprogramme oder Seiten, die dich Zeit und Fokus kosten und einfach nur ablenken, werden geschlossen.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Es wird alles Notwendige unternommen, um das eigene Stresslevel zu reduzieren.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center queue-text mb-4">
            <?= Yii::t('cabinet', 'Auch hierfür können wir dich mit speziellen Mental Power Streams die eigens nur für die Trading Session entwickelt wurden, unterstützen. Informiere dich einfach in unserem Shop.') ?>
        </div>
    </div>
</div>
