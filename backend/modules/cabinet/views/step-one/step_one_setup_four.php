<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-setup"><?= Yii::t('cabinet', 'Beschreibe dein 4. Setup') ?></div>
    <div class="step-one-text">
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-setup-four')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Beschreibe hier dein 4.Setup') ?></h4>
                    <h3><?= Yii::t('cabinet', 'Beschreibe ganz genau deine Einstiegskriterien: (hier gilt es auch immer das Potential deines Trades zu erkennen z.B. CRV mindestens 2:1)') ?></h3>
                    <?php for ($i = 0; $i < 4; $i++): ?>
                        <?php if (array_key_exists($i, $condition->setupFourData)): ?>
                            <input type="text" name="setup_data[<?= $i ?>]"
                                   value="<?= $condition->setupFourData[$i] ?>"
                                   placeholder="— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —">
                        <?php else : ?>
                            <input type="text" name="setup_data[<?= $i ?>]" value=""
                                   placeholder="— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —">
                        <?php endif ?>
                    <?php endfor ?>
                </div>
                <div class="cabinet-block-bg setup-form">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Beschreibe hier ganz genau deine Vorgehensweise mit dem SL (Stoploss) bei diesem Setup.') ?></h4>
                    <h3><?= Yii::t('cabinet', 'Wo setzt du ihn? Nach welchem Kriterium? Moneymanagement!') ?></h3>
                    <?php for ($i = 0; $i < 4; $i++): ?>
                        <?php if (array_key_exists($i, $condition->setupFourManage)): ?>
                            <input type="text" name="setup_manage[<?= $i ?>]"
                                   value="<?= $condition->setupFourManage[$i] ?>"
                                   placeholder="— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —">
                        <?php else : ?>
                            <input type="text" name="setup_manage[<?= $i ?>]" value=""
                                   placeholder="— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —">
                        <?php endif ?>
                    <?php endfor ?>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
