<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Verlust-Limitierung:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Wochenverlustlimit:')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Wenn die Marktkonditionen einer Woche einfach nicht passen und Tag für Tag Verluste entstehen, wird ein klares Wochenverlustlimit definiert.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Sobald dieses erreicht ist, wird die Woche für den Handel ausgeschlossen.')?>
        </div>
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-limit-lost-finish')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form-small">
                    <h4 class="mt-4 mb-4"><?= Yii::t('cabinet', 'Mein Betrag für mein Wochenverlustlimit lautet:') ?></h4>
                    <input type="number" name="limit_weekly" class="setup-money"
                           placeholder="XXXX.XX" value="<?= $condition->limitWeekly ?>" step=any> <span class="text-red text-bold"> €</span>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
