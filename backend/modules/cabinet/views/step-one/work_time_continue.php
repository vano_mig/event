<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Arbeitszeiten:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4 time-content">
            <?=Yii::t('cabinet', 'Bereits nach 3-4 Wochen täglichem Trading wird dir deine TJB auf jeden Fall schon mal auswerten können <br> zu welcher Session du <br> am besten handelst und am profitabelsten bist.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mt-4 mb-4">
            <div class="mt-4"> <?=Yii::t('cabinet', 'Handelszeiten:')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Morgen-Session zwischen 08:00 und 12:30 Uhr')?></div>
            <div><?=Yii::t('cabinet', 'Nachmittag-Session zwischen: 15:00 und 18:00 Uhr')?></div>
            <div><?=Yii::t('cabinet', 'Abend-Session zwischen 20:00 und 22:00 Uhr')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4">
            <div class="pt-4">
                <div><?=Yii::t('cabinet', 'Neben den aktiven Tradingzeiten solltest du dir auch Pausen einplanen.')?></div>
                <div><?=Yii::t('cabinet', 'In diesen Pausen ordnest du dich neu und verschaffst dir neue Energie.')?></div>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mt-4 mb-4">
            <?=Yii::t('cabinet', 'Wichtig ist, dass du dir klare Arbeitsblöcke mit anschließenden Pausen einplanst, um effektiv arbeiten zu können.')?>
        </div>
    </div>
</div>
