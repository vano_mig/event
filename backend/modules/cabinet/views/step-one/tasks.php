<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine persönlichen Ziele:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Setze dir Ziele.Das ist wichtig!')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Du hast Gründe dafür, dass du dich täglich vor den Rechner hockst und auf deinen Desktop starrst.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Diese Gründe sind immer mit Zielen verbunden.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Du weißt, dass der Erfolg am Markt nicht von einem Tag auf den anderen kommt sondern mit regelmäßigem und stetigen Zuwachs auf deinem Broker-Konto.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Deine persönlichen Ziele und die damit verbundenden Visionen sollten vor allem auch visuell festgehalten und radikal verfolgt werden.')?></div>
        </div>
    </div>
</div>
