<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Trade Limitierung:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-limit-trade-finish')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form-small">
                    <h4 class="mb-3"><?= Yii::t('cabinet', 'So limitiere ich die Anzahl meiner Trades:') ?></h4>
                    <textarea name="limit_desc" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="108"><?=$condition->limitDesc?></textarea>
                </div>
            </div>
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form-small">
                    <h4 class="mb-3"><?= Yii::t('cabinet', 'Folgende Maßnahmen ergreife ich bei Erreichen des Limits:') ?></h4>
                    <textarea name="limit_to_do" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="108"><?=$condition->limitToDo?></textarea>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
