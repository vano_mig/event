<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Dein maximales Risiko pro Trade :') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Dein persönliches Risikomanagement ist einer der Grundpfeiler deines Tradings!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Um dein Risikomanagement möglichst einfach zu gestalten, musst du ein maximales Risiko pro Trade festlegen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="mb-4"> <?=Yii::t('cabinet', 'Diesen Maximalbetrag (Betonung liegt auf MAXIMAL )setzt du pauschal für jeden Trade ein.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="mt-4 mb-4">
                <?=Yii::t('cabinet', 'Optimal berechnest du deinen Maximalbetrag, indem du dir dein Tagesverlustlimit, welches du dir bereits zuvor schon festgelegt hast, nimmst und dieses durch die maximale Anzahl an Verlusttrades pro Tag teilst (deine maximale Anzahl an Verlusttrades pro Tag sind bereits auch schon von Dir festgelegt).')?>
            </div>
        </div>
    </div>
</div>
