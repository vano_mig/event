<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-setup"><?= Yii::t('cabinet', 'Dein Vertrag mit dir selbst:') ?></div>
    <div class="step-one-text step-bottom">
        <form action="" method="post" id="setup_one_finish" data-url="<?=Url::toRoute('/cabinet/step-one/save-contract')?>">
            <div class="flex-row justify-content-center text-center mb-4">
                <div class="cabinet-block-bg setup-form-sm contract-form">
                    <h4 class="mt-4 mb-4"><?= Yii::t('cabinet', 'Hiermit verpflichte ich mich mir selbst gegenüber, mich stets mit äußerster Disziplin an meinen Tradingplan zu halten.') ?></h4>
                    <div><?=Yii::t('cabinet', 'Ort.')?><input type="text" name="contract_ort"
                                                              value="<?= $condition->contractOrt ?>"
                           placeholder="......................................."></div>
                    <div><?=Yii::t('cabinet', 'Datum')?><input type="text" name="contract_date"
                           value="<?= $condition->contractDate ?>"
                                                               placeholder="......................................."></div>
                </div>
            </div>
            <div class="flex-row justify-content-center text-center mb-4 sign-photo">
                <div class="cabinet-block-bg setup-form-small contract-form">
                    <div class="mt-4 mb-4"><?= Yii::t('cabinet', 'Unterschrift: ....................................') ?></div>
                    <div><?=Yii::t('cabinet', '(Fotogra ere deine Unterschrift und lade sie hier hoch)')?></div>
                    <input type="file" name="signature" id="sign-input"
                           data-name="<?= $condition->contractSignature ?>">
                </div>
            </div>
            <div class="flex-row justify-content-center text-center mt-4 mb-4">
                <div class="pt-4"><?=Yii::t('cabinet', 'Einen Vertrag mit dir selbst klingt zunächst vielleicht komisch, aber denke mal darüber nach:')?></div>
                <div><?=Yii::t('cabinet', 'Wer möchte sich schon ganz bewusst selbst anlügen!')?></div>
                   <div><?=Yii::t('cabinet', 'Wenn du diesen Vorgang nicht ernst nehmen kannst, dann beginne bitte erst gar nicht mit dem Traden.')?>
                       <?=Yii::t('cabinet', 'Gib uns Bescheid und du erhältst umgehend dein Geld von uns zurück! Wir möchten kein Geld von dir, wenn wir bereits jetzt schon feststellen, dass wir nicht zueinander passen.')?>
                </div>
            </div>
            <div class="flex-row justify-content-center text-center mb-4">
                <div><?=Yii::t('cabinet', 'Das ist nicht böse gemeint, das ist einfach nur fair!')?></div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
