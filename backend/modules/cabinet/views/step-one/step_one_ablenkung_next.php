<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Keine Ablenkung!') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Zur Ablenkung zählt natürlich auch deine familiäre Umgebung.')?>
<?= Yii::t('cabinet', 'Eine Familie zu haben, ist toll und sollte dich motivieren.') ?>
<?= Yii::t('cabinet', 'Doch sobald du dich in deine Trading – Session begibst, solltest du dich am besten alleine im Raum befinden.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Tür zu, Kopfhörer auf, Mental Power Streams einschalten und volle Konzentration und Aufmerksamkeit auf das Trading- Desktop dadurch isolierst du dich am besten und kannst dich optimal auf deine Session fokussieren.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Auch der Ort des Tradings muss gut bedacht werden.') ?>
            <?= Yii::t('cabinet', 'Trading am Laptop in der Sonne am Strand mag zwar möglich sein, sieht in der Praxis aber nicht so entspannt aus, wie man glaubt.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Es passieren viele unerwartete Fehler bei solchen leichtsinnigen Aktionen!') ?>
        </div>
    </div>
</div>
