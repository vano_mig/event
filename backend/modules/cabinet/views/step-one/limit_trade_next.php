<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Trade Limitierung:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Für jedes Ergebnis muss eine Antwort vorhanden sein.')?></div>
            <div><?=Yii::t('cabinet', 'Es darf nicht sein, dass du willkürlich drau os tradest ohne klare Maßnahmen und Regeln.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Andernfalls ist die Null auf deinem Konto die einzige Limitierung.')?>
        </div>
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-limit-trade-next')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form-small">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Meine maximale Anzahl an Verlusttrades die ich hintereinander in einer Session akzeptiere.') ?></h4>
                    <input type="text" name="max_decrease_point_trade" class="setup-abbr"
                           placeholder="0" value="<?= $condition->maxDecreasePointTrade ?>">
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
