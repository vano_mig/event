<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Regeln:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Durch deine Regeln setzt du dir klar definierte Grenzen im Trading.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Du bist als Trader selbstständig und für alle deine Entscheidungen selbst verantwortlich.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="mb-4"> <?=Yii::t('cabinet', 'Du brauchst davor keine Angst zu haben. Eigenverantwortung baust du im Laufe der Zeit auf.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center text-bold mt-4">
            <div class="mt-4 mb-4">
                <?=Yii::t('cabinet', 'Ebenso verstehst du im Laufe der Zeit immer mehr worauf es ankommt.')?>
                <?=Yii::t('cabinet','Training, Disziplin, Research und Geduld sind der Schlüssel zum Erfolg.')?>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="mt-4"><?=Yii::t('cabinet', 'Auf dem Weg des Aufbaus wirst du vor vielen Herausforderungen und Fallen stehen.')?></div>
        </div>
    </div>
</div>
