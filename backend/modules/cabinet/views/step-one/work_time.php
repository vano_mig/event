<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Arbeitszeiten:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Du kannst beinahe 24 Stunden täglich traden (Werktags).')?>,
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Genau deshalb ist es wichtig dass du dir überlegst zu welchen Uhrzeiten/zu welchen Sessions du traden möchtest.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Halte dich dann an diese Zeiten so gut es geht!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center">
            <div class="mt-4">
            <?=Yii::t('cabinet', 'Damit vermeidest du gefährliches Overtrading bzw. unkonzentriertes Arbeiten.')?>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Natürlich ist es nicht immer möglich sich auf die Minute daran zu halten.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Wenn du einen bestehenden Trade verwalten musst, dann musst du das natürlich ordentlich und sauber durchführen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Achte darauf wenn du Session-übergreifend handelst, dass der Markt sich morgens anders verhält als in der Nachmittag-Session oder am Abend.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="mt-4 mb-4">
            <?=Yii::t('cabinet', 'Du musst deine Erwartungshaltung entsprechend anpassen.')?>
            </div>
        </div>
    </div>
</div>
