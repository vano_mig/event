<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Umgebung') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Deine persönliche Umgebung und dein tagesaktueller Umstand kann dich als Trader extrem motivieren und unterstützen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Allerdings kann es auch das genaue Gegenteil heraufbeschwören und dich extrem negativ beeinflussen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Wichtig hierbei ist es, dass du dir die Umstände so zurechtmachst, dass es dich keinesfalls negativ beeinflussen kann.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Achtung! Auch ein unangenehmes Telefonat oder ein Streit mit einer dir nahestehenden Person kann einen negativen Umstand erzeugen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Du solltest keinesfalls mit irgendwelchen negativen Emotionen an deinem Trading-Desktop sitzen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Das kann sich sehr gefährlich auf dein Tradingverhalten auswirken, bis hin zum Ruin!')?>
        </div>
    </div>
</div>
