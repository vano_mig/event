<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Dokumentation') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Jeder einzelne Trade muss dokumentiert werden. Hierbei unterstützt dich TJB.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Allerdings steht die Verwaltung eines Trades immer an erster Stelle.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Das bedeutet, dass du TJB unter gewissen Umständen nicht während eines Trades mit allen erforderlichen Daten füttern kannst.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Für diese Fälle musst du unbedingt einen Block und einen Stift bereithalten, damit du dir Kurznotizen machen kannst um diese nach dem Trade ordentlich in dein TJB übertragen zu können') ?>
        </div>
        <div class=" flex-row justify-content-center text-center mb-4">
            <div><?= Yii::t('cabinet', 'Sehr wichtig sind deine Screenshoots die du immer wieder während des Trades machen solltest.') ?></div>
        </div>
        <div class=" flex-row justify-content-center text-center mb-4">
            <div><?= Yii::t('cabinet', 'Das hilft dir später beim Research und bei der Tradebewertung unheimlich.') ?></div>
        </div>
    </div>
</div>
