<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

$days = [
    'mon' => Yii::t('cabinet', 'Montag'),
    'tue' => Yii::t('cabinet', 'Dienstag'),
    'wed' => Yii::t('cabinet', 'Mittwoch'),
    'thu' => Yii::t('cabinet', 'Donnerstag'),
    'fri' => Yii::t('cabinet', 'Freitag')
]; ?>

<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="step-one-text">
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-work-time-pause')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Zu diesen Wochentagen und Uhrzeiten werde ich primär traden:') ?></h4>
                    <table class="table table-bordered table-time">
                        <thead>
                        <tr>
                            <td><?=Yii::t('cabinet', 'Wochentag')?></td>
                            <td><?=Yii::t('cabinet', 'Montag')?></td>
                            <td><?=Yii::t('cabinet', 'Dienstag')?></td>
                            <td><?=Yii::t('cabinet', 'Mittwoch')?></td>
                            <td><?=Yii::t('cabinet', 'Donnerstag')?></td>
                            <td><?=Yii::t('cabinet', 'Freitag')?></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?=Yii::t('cabinet', 'Uhrzeiten')?></td>
                            <?php foreach($days as $key=>$day): ?>
                            <td>
                                <?php if (array_key_exists($key, $condition->timeWork)): ?>
                                    <input type="text" name="work_time[<?= $key ?>]"
                                           value="<?= $condition->timeWork[$key] ?>"
                                           data-inputmask="'mask': '99:99 / 99:99'" placeholder="11:00 / 19:00">
                                <?php else : ?>
                                    <input type="text" name="work_time[<?= $key ?>]" value=""
                                           data-inputmask="'mask': '99:99 / 99:99'" placeholder="11:00 / 19:00">
                                <?php endif ?>
                            <?php endforeach; ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="cabinet-block-bg setup-form">
                    <h3 class="mt-3"><?= Yii::t('cabinet', 'Meine Pausen definiere ich wie folgt:') ?></h3>
                    <table class="table table-time-pause">
                        <?php foreach($days as $key=>$day): ?>
                        <tr>
                            <td><?=$day?></td>
                            <td>
                                <?php if (array_key_exists($key, $condition->timePause)): ?>
                                        <input type="text" name="work_pause[<?= $key ?>]"
                                               value="<?= $condition->timePause[$key] ?>"
                                               data-inputmask="'mask': '99:99 / 99:99'" placeholder="12:00 / 13:00">
                                <?php else : ?>
                                    <input type="text" name="work_pause[<?= $key ?>]" value=""
                                           data-inputmask="'mask': '99:99 / 99:99'" placeholder="12:00 / 13:00">
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>

<?php //$this->registerJsFile(Yii::getAlias('@backend').DIRECTORY_SEPARATOR.'web/js/inputmask.min.js'); ?>
<?php //$this->registerJsFile(Yii::getAlias('@backend').DIRECTORY_SEPARATOR.'web/js/inputmask.binding.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

