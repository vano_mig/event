<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Rahmenbedingungen') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Deine zukünftigen persönlichen Rahmenbedingungen sorgen für einen klaren Fokus im Trading.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Um dich in der großen weiten Trading-Welt nicht zu verlaufen und um immer primär das zu tun, was getan werden muss, musst du deine Rahmenbedingungen definieren!.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Andernfalls wirst du dich im Zweifelsfall im Kreis drehen und nicht vorwärtskommen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Es ist wichtig, gewisse Parameter klar zu definieren, um es einmal richtig zu machen, anstatt es dreißig Mal wiederholen zu müssen und nicht voranzukommen.')?>
        </div>
    </div>
</div>
