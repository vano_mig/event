<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-setup"><?= Yii::t('cabinet', 'Beschreibe dein 4. Setup') ?></div>
    <div class="step-one-text">
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-setup-four-next')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form-sm">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Beschreibe hier ganz genau deine Vorgehensweise mit deinen TP‘s (Take Pro ts) bei diesem Setup.') ?></h4>
                    <h3><?= Yii::t('cabinet', 'Wo nimmst du deine Gewinne mit? Nach welchem Kriterium?') ?></h3>
                    <?php for ($i = 0; $i < 2; $i++): ?>
                        <?php if (array_key_exists($i, $condition->setupFourCriterium)): ?>
                            <input type="text" name="setup_criterium[<?= $i ?>]"
                                   value="<?= $condition->setupFourCriterium[$i] ?>"
                                   placeholder="— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —">
                        <?php else : ?>
                            <input type="text" name="setup_criterium[<?= $i ?>]" value=""
                                   placeholder="— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —">
                        <?php endif ?>
                    <?php endfor ?>
                </div>
                <div class="cabinet-block-bg setup-form-sm">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Benenne hier dieses Setup: (z.B.: Breakout Long / Backtest Reverse / ......)') ?></h4>
                    <h3></h3>
                    <?php for ($i = 0; $i < 2; $i++): ?>
                        <?php if (array_key_exists($i, $condition->setupFourCriteriumName)): ?>
                            <input type="text" name="setup_criterium_name[<?= $i ?>]"
                                   value="<?= $condition->setupFourCriteriumName[$i] ?>"
                                   placeholder="-------------------------------------------------------------------------— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —-----------------------------------------------------------------------------------------------------">
                        <?php else : ?>
                            <input type="text" name="setup_criterium_name[<?= $i ?>]" value=""
                                   placeholder="— — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — —">
                        <?php endif ?>
                    <?php endfor ?>
                </div>
                <div class="cabinet-block-bg setup-form-sm">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Benenne hier dieses Setup mit einem Kürzel von max. 3 Buchstaben:') ?></h4>
                    <h3><?= Yii::t('cabinet', '(z.B. BOL für Breakout Long oder BR für Backtest Reverse)') ?></h3>
                    <input type="text" name="setup_criterium_abbr" class="setup-abbr"
                           placeholder="OOO" value="<?= $condition->setupFourCriteriumAbbr ?>" maxlength="3">
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
