<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Disziplin:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Wiederholung:')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Als Trader bist du nur so gut wie dein eigener Tradingplan!')?></div>
            <div><?=Yii::t('cabinet', 'Baue deshalb eine unschlagbare Disziplin auf, sei ordentlich und stets eißig.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Je ausführlicher und durchdachter dein Trainingsplan definiert ist, desto besser, klarer und einfacher wirst du an dein Ziel kommen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div class="pb-4"><?=Yii::t('cabinet', 'Halte dich an deine Maßnahmen und handle verantwortungsvoll und eigenverantwortlich!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4 text-bold">
            <div class="pt-4"><?=Yii::t('cabinet', 'Am Ende bist du selbst dafür verantwortlich, ob du dein Leben positiv verändern oder scheitern wirst!')?></div>
        </div>
    </div>
</div>
