<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Regeln:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Um den Totalverlust zu vermeiden, stellst du dir klare in Stein gemeißelte Regeln auf.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Wenn du nicht bereit bist deine eigenen Regeln diszipliniert und eisern einzuhalten, dann schalte die Software bitte jetzt sofort ab!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="mb-4"> <?=Yii::t('cabinet', 'Beginne niemals mit dem Trading!')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
                <?=Yii::t('cabinet', 'Dein Totalverlust ist so sicher wie das Amen in der Kirche!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
                <?=Yii::t('cabinet', 'Undisziplinierte Trader werden von der Börse geschluckt und leergesaugt (Pleite!) wieder ausgespuckt!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4 mt-4 text-bold">
            <div class="mt-4 mb-4"><?=Yii::t('cabinet', 'Disziplin ist die Brücke zwischen einer Idee und dem Erfolg!')?></div>
        </div>
    </div>
</div>
