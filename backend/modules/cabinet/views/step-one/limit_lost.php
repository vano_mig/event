<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Verlust-Limitierung:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Um das eigene Tradingkonto nicht gegen die Wand zu fahren, wird neben der Quantität der Trades auch der maximale Verlust begrenzt.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Dies sorgt für den Schutz des Kontos und verhindert weitere gravierende Verluste.')?>
        </div>
    </div>
</div>
