<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Ordnung') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Profitables Trading ist abhängig von deiner Disziplin, deiner Ordnung und von deinem Fleiß.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Die Ordnung an deinem Arbeitsplatz ist genauso wichtig wie die Ordnung deiner Gedanken.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center queue-text mb-4">
            <?= Yii::t('cabinet', 'Bei der Ordnung deiner Gedanken können wir dich mit') ?>
        </div>
        <div class="mps-image">
            <?=Html::img('/images/system/mps.png', ['class'=>'images']) ?>
        </div>
        <div class="mps-image-name">
            <?=Html::img('/images/system/mps_name.png', ['class'=>'images']) ?>
        </div>
        <div class=" flex-row justify-content-center text-center mb-4 queue-text">
            <div><?= Yii::t('cabinet', 'optimal unterstützen.') ?></div>
        </div>
        <div class=" flex-row justify-content-center text-center mb-4 queue-text">
            <div><?= Yii::t('cabinet', 'Für die Ordnung an deinem Arbeitsplatz musst du bitte selbst sorgen.') ?></div>
            <div><?= Yii::t('cabinet', 'Stelle also unbedingt sicher, dass Unordnung in deiner Umgebung keine Behinderung darstellt und achte stets auf deinen Arbeitsplatz.') ?></div>

        </div>
    </div>
</div>
