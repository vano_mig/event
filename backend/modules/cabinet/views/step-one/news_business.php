<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Wirtschaftsnachrichten:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Durch fast schon tägliche Veröffentlichungen von Wirtschaftsnachrichten bewegen sich Märkte oft sehr irrational und unberechenbar.')?>,
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Genau zu diesen Zeitpunkten der Veröffentlichung ist Vorsicht geboten.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="mb-4"> <?=Yii::t('cabinet', 'Hierfür definierst du einen klaren Zeitrahmen, zu dem du niemals tradest.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Wir empfehlen dir, fünf bis zehn Minuten vor Veröffentlichung der wichtigsten und bedeutsamsten Nachrichten keine Positionen mehr zu eröffnen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Undisziplinierte Trader werden von der Börse geschluckt und leergesaugt (Pleite!) wieder ausgespuckt!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="mb-4"><?=Yii::t('cabinet', 'Auch solltest du nach den Nachrichten mindestens noch 5 min.warten.')?></div>
        </div>
    </div>
</div>
