<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Welche Handelsrichtung:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Bevor du mit einer eigentlichen Tradingsession startest')?>,
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'musst du von Vornherein ganz klar die Handelsrichtung')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'und deinen daraus resultierenden Handelsstil festlegen.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center queue-text">
            <div class="mb-4">
            <?=Yii::t('cabinet', 'Möchtest du klassisches Intraday-Trading betreiben oder kurzfristiges Scalping?')?>
            </div>
        </div>
    </div>
</div>
