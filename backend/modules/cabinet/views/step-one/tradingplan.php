<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Dein Tradingplan') ?></div>
    <div class="mt-4 step-one-text">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Mit Hilfe von TJB kannst du schnell herausfinden mit welchen deiner Setups du in bestimmten Märkten zu bestimmten Zeiten am profitabelsten bist.') ?>
            <?= Yii::t('cabinet', 'TJB legt schonungslos und schnell deine Fehler und Schwächen auf und berichtet dir ab einer gewissen Datenmenge (Je mehr desto besser) wann du am besten und am profitabelsten bist und von wo und wann du am besten deine Finger von der Tastatur lässt!') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center title-trade">
            <?= Yii::t('cabinet', 'Nur so kannst du profitabel werden!') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Vorausgesetzt du trägst immer ordentlich deine Daten in dein TJB ein und bist bereit ständig an dir zu arbeiten und dich zu verbessern.') ?>
        </div>
        <div class="flex-row justify-content-center text-center title-trade">
            <div><?= Yii::t('cabinet', 'Es gibt keinen heiligen Gral im Trading!') ?></div>
            <div><?= Yii::t('cabinet', 'Kein Indikator oder irgendwelche Signaldienste spülen dir automatisch Geld auf dein Konto!') ?></div>
            <p> <?= Yii::t('cabinet', 'TJB hilft dir dabei dein eigener Heiliger Trading Gral zu werden!') ?></p>
        </div>
    </div>
</div>
