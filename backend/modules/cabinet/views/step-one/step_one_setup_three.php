<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-setup"><?= Yii::t('cabinet', 'Beschreibe dein 3. Setup') ?></div>
    <div class="step-one-text">
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-setup-three')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Beschreibe hier dein 3.Setup') ?></h4>
                    <h3><?= Yii::t('cabinet', 'Beschreibe ganz genau deine Einstiegskriterien: (hier gilt es auch immer das Potential deines Trades zu erkennen z.B. CRV mindestens 2:1)') ?></h3>
                    <textarea name="setup_data" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="188"><?=$condition->setupThreeData?></textarea>
                </div>
                <div class="cabinet-block-bg setup-form">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Beschreibe hier ganz genau deine Vorgehensweise mit dem SL (Stoploss) bei diesem Setup.') ?></h4>
                    <h3><?= Yii::t('cabinet', 'Wo setzt du ihn? Nach welchem Kriterium? Moneymanagement!') ?></h3>
                    <textarea name="setup_manage" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="188"><?=$condition->setupThreeManage?></textarea>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
