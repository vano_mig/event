<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>

<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Schritt für Schritt:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Oftmals setzen wir Ziele und Aufgaben nicht um, weil sie uns einfach viel zu groß erscheinen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Wenn die gewünschten Veränderungen nicht sofort in Bewegung kommen dann lässt man oft eine schlechte Angewohnheit weiter dahingleiten.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Im Trading endet so ein Verhalten meist mit einem platten Konto.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Mit der 1% Regel zerlegst du dein großes Ziel in einzelne kleine Schritte.')?></div>
            <div><?=Yii::t('cabinet', 'Jeden Tag eine Kleinigkeit die machbar ist!')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Mit TJB-Research und Screentime, die du im Laufe der Zeit sammelst kommst du deinem Ziel immer ein Stückchen näher.')?></div>
        </div>
    </div>
</div>