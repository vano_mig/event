<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Vorbereitung') ?></div>
    <div class="mt-4 step-one-text">
        <form action="" method="post" id="prepare" data-url="<?=Url::toRoute('/cabinet/step-one/save-prepare')?>">
            <div class="d-flex flex-row justify-content-center text-center mb-4">
                <?= Yii::t('cabinet', 'Bevor du mit deiner Trading-Session startest, musst du deine Workspace entsprechend vorbereiten.') ?>
                <?= Yii::t('cabinet', 'Hierzu ordnest du in deiner Tradingplattform deine Charts ordentlich und für dich komfortabel an.') ?>
                <?= Yii::t('cabinet', 'Als nächstes führst du entsprechend deinen Strategien eine saubere Marktanalyse durch und checkst unbedingt den Wirtschaftskalender.') ?>
            </div>
            <div class="d-flex flex-row justify-content-center text-center rituale mb-4">
                <?= Yii::t('cabinet', 'Beschreibe hier deine Vorgehensweise/deine Trading-Vorbereitung') ?>
            </div>
            <div class="d-flex flex-row justify-content-center text-center">
                <div class="cabinet-block-bg rituale-form">
                    <h3 class="mt-3"><?= Yii::t('cabinet', 'Meine Trading-Vorbereitung:') ?></h3>
                    <textarea name="prepare" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="270"><?=$condition->prepare?></textarea>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
