<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>

<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Schritt für Schritt:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Jeden Tag ein kleines Stückchen besser werden. Jeden Tag nur 1%.')?></div>
            <div><?=Yii::t('cabinet', 'Kennst du dieses Buch schon?')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Die 1%-Methode – Minimale Veränderung, maximale Wirkung:')?></div>
            <div><?=Yii::t('cabinet', 'Mit kleinen Gewohnheiten jedes Ziel erreichen - Mit Micro Habits zum Erfolg')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="adv-image pt-4 pb-4"><?=Html::img('/images/system/adv.png', ['class'=>'image'])?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Das Konzept stammt von Alan Weiss und die Idee hinter diesem Konzept ist simpel und unserer Meinung nach einfach nur grandios!')?></div>
        </div>
    </div>
</div>