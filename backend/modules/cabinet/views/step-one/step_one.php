<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Dein Tradingplan') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
           <?=Yii::t('cabinet', 'Um mit dem Trading ernsthaft überhaupt beginnen zu können steht an erster Stelle zunächst ein gut durchdachter Tradingplan.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
           <?=Yii::t('cabinet', 'Ohne diesen Plan geht es nicht!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Ohne Tradingplan zu handeln ist das selbe wie durch den Dschungel zu laufen ohne Kompass.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'DEIN PERSÖNLICHER TRADINGPLAN soll dir täglich dabei helfen diszipliniert nach deinen Regeln zu traden')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Dein persönlicher Tradingplan wird in deinem Persönlichen TJB-System (Trading Journal Booster) verankert und bildet ab sofort das Herz deiner Tradingkarriere. Die Regeln sind in Stein gemeißelt und dürfen unter keinen Umständen verletzt werden.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Ein Trader ist nur so gut wie sein Tradingplan!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Reflektiere und analysiere täglich dein Trading mit TJB.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Die Herausforderung hierbei liegt darin, dein TJB System Tag für Tag, Trade für Trade mit den notwendigen Daten zu füttern um mit den Erkenntnissen, die dir TJB in sekundenschnelle liefert, zu arbeiten.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Dafür muss dir aber von vorn herein klar sein, dass es im Trading keine 100 % Trefferquote gibt! Kein Trader auf dieser Welt schafft es eine 100 % Quote zu erreichen. Wer das behauptet ist hochgradig unseriös')?>
        </div>
    </div>
</div>
