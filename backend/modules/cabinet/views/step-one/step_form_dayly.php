<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-setup"><?= Yii::t('cabinet', 'Schritt für Schritt:') ?></div>
    <div class="step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div class="pb-4"><?=Yii::t('cabinet', 'Dein 1% Ziel')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div class="pt-4"><?=Yii::t('cabinet', 'Es gibt immer Gründe dafür, warum man etwas angeht – so auch im Trading.')?></div>
            <div class="pb-4"><?=Yii::t('cabinet', 'Was ist das? Warum und was steckt hinter dem eigenen Vorhaben?  Wie sieht die  eigene Vision aus?')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div class="pt-4 pb-4"><?=Yii::t('cabinet', 'Definiere dir deinen Optimalzustand, dein übergeordnetes Ziel, und breche es auf einzelne Schritte und Meilensteine runter.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-2">
            <div class="pt-4"><?=Yii::t('cabinet', 'Durch das Runterbrechen auf ein Tagesziel definierst du kleinere untergeordnete Ziele und Aufgaben, welche auf das Große hinarbeiten.')?></div>
        </div>
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-step-by-step-dayly')?>">
            <div class="flex-row justify-content-center text-center mb-4">
                <div class="cabinet-block-bg setup-form-small">
                    <h4 class="mt-4 mb-4"><?= Yii::t('cabinet', 'Mein Betrag für mein Tagesziel lautet:') ?></h4>
                            <input type="text" name="dayly_amount" class="setup-money"
                                   value="<?= $condition->daylyAmount ?>"
                                   placeholder="XXXX.XX"> <span class="text-bold text-red" step=any>€</span>
                </div>
            </div>
            <div class="flex-row justify-content-center text-center mt-4 mb-4 color-red">
                <div class="pt-4"><?=Yii::t('cabinet', 'Arbeite mit diesem Tagesziel mindestens zwölf Wochen.')?></div>
                <div class="pb-4"><?=Yii::t('cabinet', 'Danach kannst du dein Tagesziel erhöhen.')?>
                    <?=Yii::t('cabinet', 'Voraussetzung hierfür ist natürlich dass du zuvor schon profitabel warst.')?>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
