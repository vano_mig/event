<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>

<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine persönlichen Ziele:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Lade dir hier verschiedene Bilder deiner Ziele,Träume und Visionen hoch, damit TJB dir diese immer wieder in bestimmten Situationen aufzeigen kann.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Das kann beispielsweise ein Bild von einer glücklichen und zufriedenen Familie sein, das kann ein Bild von einem Auto sein, von einem Boot, von einem Urlaub, von einem Bild welches dir Freiheit vermittelt, ein Bild von einem Menschen dem du etwas Gutes tun möchtest, deinem Traumhaus......')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Jeder Mensch hat etwas von dem er träumt!')?></div>
        </div>
        <form action="" data-url="<?=Url::toRoute('/cabinet/step-one/load-photo')?>" id="photo-form" method="POST" enctype="multipart/form-data">

        <div class="d-flex flex-row justify-content-center text-center photo-content">
            <div class="photo-block" data-id="1">
                <div class="profile-content">
                    <div><?= Yii::t('cabinet', 'Lade hier dein persönliches Bild hoch.') ?></div>
                    <div class="mt-3 mb-2"><?= Yii::t('cabinet', 'Was willst du erreichen...') ?></div>
                    <div class="filename"><?=$condition->photo_1?></div>

                    <input type="file" name="file[]" placeholder="file" id="photo1">
                </div>

            </div>
            <div class="photo-block"  data-id="2">
                <div class="profile-content">
                    <div><?= Yii::t('cabinet', 'Lade hier dein persönliches Bild hoch.') ?></div>
                    <div class="mt-3 mb-2"><?= Yii::t('cabinet', 'Was willst du erreichen...') ?></div>
                    <div class="filename"<?=$condition->photo_2?>></div>

                    <input type="file" name="file[]" placeholder="file" id="photo2">

                </div>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center photo-content">
            <div class="photo-block"  data-id="3">
                <div class="profile-content">
                    <div><?= Yii::t('cabinet', 'Lade hier dein persönliches Bild hoch.') ?></div>
                    <div class="mt-3 mb-2"><?= Yii::t('cabinet', 'Was willst du erreichen...') ?></div>
                    <div class="filename"><?=$condition->photo_3?></div>
                    <input type="file" name="file[]" placeholder="file" id="photo3">

                </div></div>
            <div class="photo-block" data-id="4">
                <div class="profile-content">
                    <div><?= Yii::t('cabinet', 'Lade hier dein persönliches Bild hoch.') ?></div>
                    <div class="mt-3 mb-2"><?= Yii::t('cabinet', 'Was willst du erreichen...') ?></div>
                    <div class="filename"><?=$condition->photo_4?></div>
                    <input type="file" name="file[]" placeholder="file" id="photo4">
                </div>
            </div>
        </div>
        </form>
    </div>
</div>