<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Nimm dir deine Zeit die du brauchst') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Auf den folgenden Seiten hilft dir TJB dabei deinen persönlichen Trainingsplan Schritt für Schritt zu erstellen.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Nimm dir dafür ausreichend Zeit. Es ist durchaus möglich, dass du dafür mehrere Stunden vielleicht auch Tage brauchst.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Das ist überhaupt nicht schlimm! Je durchdachter und ausgereifter dein Plan ist, desto besser!') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Solltest du an einem beliebigen Punkt im System zunächst nicht weiterkommen weil du dir dazu einfach noch Gedanken machen möchtest, kannst du jederzeit deine bisherigen Eingaben zwischenspeichern und jederzeit an dem Punkt weiter machen an dem du aufgehört hast.') ?>
        </div>
        <div class=" flex-row justify-content-center text-center title-trade">
            <div><?= Yii::t('cabinet', 'Trading ist kein Sprint!') ?></div>
        </div>
        <div class=" flex-row justify-content-center text-center title-trade">
            <div><?= Yii::t('cabinet', 'Trading ist ein Marathon!') ?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Je besser du dich auf diesen Marathon vorbereitest desto mehr Spaß wird er dir später bereiten.') ?>
        </div>
    </div>
</div>
