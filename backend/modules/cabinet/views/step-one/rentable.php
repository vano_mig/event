<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Dein Profitabilitätsmanagement:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Im Trading gibt es viele verschiedene Arten, die Profitabilität zu erlangen.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Am Ende des Tages ist es immer nur ein Rechenspiel.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Du kannst auf unterschiedliche Managementarten die Profitabilität erreichen')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Es gibt hunderte von sogenannten ultimativen Trademanagements.')?></div>
            <div><?=Yii::t('cabinet', 'Viele Trading-Gurus versprechen dir mit ihrem Trademanagement das Blaue vom Himmel!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center text-bold mt-4 mb-4">
            <div class="pt-4 pb-4"><?=Yii::t('cabinet', 'Denke immer daran, das Geschäft an der Börse ist und bleibt immer ein Spiel mit den Wahrscheinlichkeiten!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4">
            <div><?=Yii::t('cabinet', 'Niemand kann und darf dir im Trading etwas versprechen, das ist unseriös.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center text-bold mb-4">
            <div class="pt-4"><?=Yii::t('cabinet', 'Nur du selbst kannst dich durch vernünftige Entscheidungen im Spiel halten!')?></div>
            <div><?=Yii::t('cabinet', 'Dein TJB unterstützt dich dabei!')?></div>
        </div>
    </div>
</div>
