<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Ziele in Schritte einteilen:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Dein Trading besteht aus vielen Meilensteinen, die letztendlich zum großen Ziel führen.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4 pl-4 pr-4">
            <div class="pl-4 pr-4"><?=Yii::t('cabinet', 'Um diese Meilensteine zu erreichen, musst du <br>immer besser und konstant profitabel werden.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div class="pb-4"> <?=Yii::t('cabinet', 'Keine Angst! Es geht....... Mit Disziplin und Durchhaltevermögen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4">
            <div class="pt-4"><?=Yii::t('cabinet', 'Hierzu steht am Anfang dein Kapitalerhalt.')?></div>
            <div class="pb-4"><?=Yii::t('cabinet', 'Anschließend kommt die Kapitalvermehrung!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4">
            <div class="pt-4"><?=Yii::t('cabinet', 'Du musst die Saat erst ausbringen bevor du sie ernten, weiterverarbeiten und letztenendes verzehren kannst!')?></div>
        </div>
    </div>
</div>
