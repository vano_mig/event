<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Rituale') ?></div>
    <div class="mt-4 step-one-text">
        <form action="" method="post" id="rituale" data-url="<?=Url::toRoute('/cabinet/step-one/save-rituale')?>">
            <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->csrfToken; ?>">
            <div class="d-flex flex-row justify-content-center text-center rituale mb-4">
                <?= Yii::t('cabinet', 'Trage hier deine persönlichen Rituale ein die <br> du ab jetzt täglich durchführen wirst <br> um dein Mindset direkt vor Arbeitsbeginn auf Höchstleistung zu trimmen') ?>
            </div>
            <div class="d-flex flex-row justify-content-center text-center">
                <div class="cabinet-block-bg rituale-form">
                    <h3 class="mt-3"><?= Yii::t('cabinet', 'Meine persönlichen Rituale, welche ich vor dem Trading durchführen werde:') ?></h3>
                    <textarea name="rituale" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="270"><?=$condition->rituale?></textarea>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
