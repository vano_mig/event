<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Wohlfühlmärkte') ?></div>
    <div class="mt-4 step-one-text">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Um mit deinem Trading Journal Booster eine klare Expertise aufzubauen, mit welcher du täglich ein sauberes und ordentliches Research durchführen kannst, bringt es dir absolut nichts, wenn du auf zehn Märkten gleichzeitig herumtanzt.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4 text-bold">
            <?=Yii::t('cabinet', 'So wirst du die Eigenschaften und Besonderheiten eines Marktes nie kennenlernen.')?>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div>
            <?=Yii::t('cabinet', 'Zudem sollten es Märkte sein, in denen du dich wohlfühlst.')?>
            <?=Yii::t('cabinet', 'Wenn du deine Favoriten gefunden hast, solltest du ausschließlich diese für die nächsten Monate traden und entsprechend Researchen.')?>
            </div>
            <div>
                <?=Yii::t('cabinet', 'Sobald du deine Favoriten im Schlaf beherrschst, kannst du deine Watchlist immer noch erweitern.')?>
            </div>
        </div>
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-markets')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg market-form-sm">
                    <h4 class="mt-4 mb-4"><?= Yii::t('cabinet', 'Auf diese Märkte werde ich mich festlegen:') ?></h4>
                    <textarea name="setup_markets" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="144"><?=$condition->markets?></textarea>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
