<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Trade Limitierung:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Um das sogenannte Overtrading zu vermeiden, welches oftmals zu horrenden oder gar Totalverlusten führt, setzt du dir Limits.')?>
            <?=Yii::t('cabinet', 'Folgende Limitierungen solltest du unbedingt definieren:')?>
        </div>
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-limit-trade')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form-small">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Ich trade eine max. Summe an Trades pro Tag.') ?></h4>
                    <input type="text" name="max_trade" class="setup-abbr" required
                           placeholder="0" value="<?= $condition->maxTrade ?>">
                </div>
            </div>
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form-small">
                    <h4 class="mt-3"><?= Yii::t('cabinet', 'Meine maximale Anzahl an Verlusttrades.am Tag, die ich akzeptiere') ?></h4>
                    <input type="text" name="max_decrease_trade" class="setup-abbr" required
                           placeholder="0" value="<?= $condition->maxDecreaseTrade ?>">
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
