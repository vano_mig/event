<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>

<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine persönlichen Ziele:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Niemand wird von heute auf morgen Millionär an der Börse.')?></div>
            <div><?=Yii::t('cabinet', 'Daher ist es wichtig, dass du dir realistische Tagesziele setzt und konsequent auf deine Meilensteine zuarbeitest.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Über dein persönliches Tagesziel regelt sich auch deine Tradingsfrequenz.')?></div>
            <div><?=Yii::t('cabinet', 'So kommst du nicht ins Overtrading und wirst zu gierig!')?></div>
            <div><?=Yii::t('cabinet', 'Gier hat im Trading nichts verloren, Gier hindert dich daran deine Ziele zu erreichen.')?></div>
            <div><?=Yii::t('cabinet', 'Mit Gier zerstörst du teilweise deinen kompletten Tagesgewinn und noch viel mehr!')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Zu deinen weiteren Zielen gehört das Besserwerden.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Hierbei unterstützt dich dein Persönliches TJB welches dir eine professionelle Expertise erstellt.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Du lernst mittels einer objektiven Selbstanalyse an deinen eigenen Fehlern zu arbeiten und daraus zu lernen.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Immer vorausgesetzt du trägst konsequent und sauber deine Daten in dein TJB ein und bewertest objektiv und ehrlich deine einzelnen Trades!')?></div>
        </div>

    </div>
</div>