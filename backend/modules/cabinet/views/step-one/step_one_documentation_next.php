<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Dokumentation') ?></div>
    <div class="mt-4 step-one-text">
        <form action="" method="post" id="prepare" data-url="<?=Url::toRoute('/cabinet/step-one/save-documentation')?>">
            <div class="flex-row justify-content-center text-center mb-4">
               <div> <?= Yii::t('cabinet', 'Neben den einzelnen Tradeinformationen wird <br> jeder Trade kommentiert und ausgewertet.') ?></div>
                <div><?= Yii::t('cabinet', 'Am Ende des Tages oder spätestens am Folgetag erfolgt ein Review / eine Gesamtauswertung der Trades.') ?></div>
                <div><?= Yii::t('cabinet', 'Auch die Trademöglichkeiten, welche an diesem Tag möglich gewesen wären, werden geresearcht.')?></div>
                <div><?=Yii::t('cabinet', 'Nur durch ein ordentliches Research kannst du für künftige Trades lernen.') ?></div>
                <div class="text-bold"><?=Yii::t('cabinet', 'Es gibt nichts wichtigeres!') ?></div>
            </div>
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg rituale-form">
                    <h3 class="mt-3"><?= Yii::t('cabinet', 'So dokumentiere ich meine Trades:') ?></h3>
                    <textarea name="documentation" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="270"><?=$condition->documentation?></textarea>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
