<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Trading ist ein Beruf!') ?></div>
    <div class="mt-4 step-one-text">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Um deinen Beruf diszipliniert und strukturiert ausüben zu können brauchst du tägliche Rituale bevor du dich an den Rechner setzt um zu traden.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'In keinem Beruf sollte man morgens bei Arbeitsbeginn völlig planlos einfach mal so drauf los arbeiten. In den allermeisten Fällen kommt dabei nichts produktives heraus.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Märkte verändern sich stetig. Das wiederum bedeutet: jeder Tag ist anders. Du kannst beim Trading an einem Tag viel Geld verdienen aber viel leichter und viel schneller kannst du auch viel Geld verlieren!')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center title-trade">
            <?=Yii::t('cabinet', 'Je ruhiger und gelassener du dich an deinen Rechner setzt, desto besser und strukturierter wird dein Handelstag verlaufen. Hektik am Morgen und auf den letzten Drücker unausgeglichen am Rechner sitzen wird denitiv dein Konto zerstören!')?>
        </div>
    </div>
</div>
