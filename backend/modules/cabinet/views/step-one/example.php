<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Ein kleines Beispiel für dich:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Dein Tagesverlustlimit beträgt $ 700 und es dürfen maximal zwei Verlusttrades realisiert werden.')?></div>
            <div><?=Yii::t('cabinet', 'So würde das maximale Risiko pro Trade $ 350 betragen.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Dann berechnest du deine Positionsgröße für den einzelnen Trade:')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Beachte dass du die Positionsgröße (Anzahl der LOT’s für diesen Trade) immer aus deinem maximalen Risiko pro Trade und dem Stopploss den du zuvor analysierst hast, berechnest.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Beispiel:')?></div>
            <div><?=Yii::t('cabinet', 'Es werden pro Trade $ 350 riskiert und der Stopploss für den Trade beträgt im CL (WTI Oil). 15 Ticks.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'So wird zuerst ermittelt, wie hoch das Risiko pro Tick ist:')?></div>
            <div><?=Yii::t('cabinet', '$ 350 : 15 Ticks = $ 23,33 pro Tick.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Dann teilt man diesen Wert durch den Tickwert des Futures – in unserem Fall der CL (hier ist ein Tick $ 10 Wert pro Kontrakt).')?></div>
            <div><?=Yii::t('cabinet', 'Also $ 23,33 : $ 10 = 2,33 Kontrakte.')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Somit beträgt die optimale Kontraktanzahl für diesen Trade 2 Lots.')?>
        </div>
    </div>
</div>
