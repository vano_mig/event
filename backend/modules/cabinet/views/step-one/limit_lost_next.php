<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Verlust-Limitierung:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-limit-lost-next')?>">
            <div class="flex-row justify-content-center text-center">
                <div class="cabinet-block-bg setup-form-sm">
                    <div class="form-info"><?= Yii::t('cabinet', 'Tagesverlustlimit:') ?></div>
                    <div class="form-info">
                        <?=Yii::t('cabinet', '“Ich setze mir folgenden Betrag fest, welchen ich pro Tag maximal verlieren darf, bis ich den Tradingtag abbreche“.')?>
                    </div>
                    <div class="form-info">
                        <?=Yii::t('cabinet', 'Mein Maximalbetrag für mein Tagesverlustlimit lautet:')?>
                    </div>

                    <input type="number" name="limit_daily" class="setup-money"
                           placeholder="XXXX.XX" value="<?= $condition->limitDaily ?>" step=any> <span class="text-red text-bold"> €</span>
                </div>
                <div class="cabinet-block-bg setup-form-sm">
                    <div class="form-info">
                       <div class="text-bottom"> <?=Yii::t('cabinet', 'Diesen Maximalbetrag teile ich in')?>
                           <input type="number" name="max_limit_one" value="<?php echo $condition->limitDailyMax?>" class="setup-money" id="step-limit">
                           <?=Yii::t('cabinet','Trades auf, so dass ich nach')?>
                           <input type="number" name="limit_daily_step" value="<?php echo $condition->limitDailyStep?>" class="setup-money" id="step-limit-2">
                           <?=Yii::t('cabinet','Verlustrades in Folge sofort meinen Tradingtag abbreche')?></div>
                    </div>
                    <div class="form-info">
                        <?=Yii::t('cabinet', '(Du kannst dich natürlich auch auf nur 2 Verlusttrades festlegen)')?>
                    </div>
                    <div class="form-info">
                        <?=Yii::t('cabinet', 'Es sollten aus psychologischen Gründen nur nicht mehr als drei sein!')?>
                    </div>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
