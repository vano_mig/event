<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Dein Profitabilitätsmanagement:') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Der Markt macht immer was er will und niemals das was du von ihm forderst.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Wenn es für dich mal an einem Tag nicht läuft, dann höre rechtzeitig auf!')?></div>
            <div><?=Yii::t('cabinet', 'Das ist ein Versprechen das du dir selber geben musst!')?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <div> <?=Yii::t('cabinet', 'Es wird immer wieder mal solche Tage geben,<br>du wirst sie nicht komplett verhindern können!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Du kannst aber deinen Totalverlust verhindern!<br>Und du kannst diese Tage reduzieren.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4">
            <div><?=Yii::t('cabinet', 'Und genau hier liegt der Hase im Pfeffer!')?></div>
            <div class="pb-4"><?=Yii::t('cabinet', 'Die Kunst aufzuhören... nicht mit irgendwelchen Rachetrades im letzten Moment das Ruder herum reißen zu wollen!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mt-4 mb-4">
            <div class="pt-4 pb-4"><?=Yii::t('cabinet', 'Hier unterscheidet sich der Pro vom Amateur.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div class="pt-4"><?=Yii::t('cabinet', 'Für diesen Fall muss dein rigoroses Management an oberster Stelle stehen.')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Bleibe dabei und zieh es gnadenlos durch!')?></div>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?=Yii::t('cabinet', 'Wechsle nicht zwischen verschiedenen Managementarten, bleibe auf Kurs und bleibe bei deinem persönlichen Plan.')?></div>
        </div>
    </div>
</div>
