<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Routine') ?></div>
    <div class="mt-4 step-one-text">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Eine tägliche Routine stellst du mit deinen persönlichen täglichen Ritualen sicher.') ?>
            <?= Yii::t('cabinet', 'Mit diesen Ritualen sammelst du vor Beginn deiner Trading-Tätigkeit jeden Morgen frische Energie und bereitest dich mental auf das Trading vor.') ?>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?= Yii::t('cabinet', 'Dieser Prozess ist extrem wichtig! Solltest du dein tägliches Ritual aus irgendwelchen Gründen nicht sauber durchführen können, wäre es am besten, an diesem Tag erst gar nicht mit dem Traden zu beginnen. Das ist kein Witz, sondern mein kompletter ernst!') ?></div>
            <div><?= Yii::t('cabinet', 'Eine leichtsinnige oder fahrlässige Entscheidung kostet dich immer sofort bares Geld.') ?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Deine Routine kann aus Meditation bestehen, einem Buch lesen, ein morgendlicher Spaziergang, Kaffee trinken, Zeitung lesen oder ähnliches.') ?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Sinn und Zweck dessen ist es dein Energie- und Konzentrationslevel auf Maximum und dein Stresslevel auf das absolute Minimum zu stellen.') ?>
        </div>
    </div>
</div>
