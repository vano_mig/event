<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-setup"><?= Yii::t('cabinet', 'Schritt für Schritt:') ?></div>
    <div class="step-one-text step-bottom">
        <form action="" method="post" id="setup_one" data-url="<?=Url::toRoute('/cabinet/step-one/save-step-by-step-form')?>">
            <div class="flex-row justify-content-center text-center">
                <div><?=Yii::t('cabinet', 'Dein 1% Ziel')?></div>
                <div class="cabinet-block-bg setup-form-sm">
                    <h4 class="mt-4 mb-4"><?= Yii::t('cabinet', 'Formuliere hier deine großen Visionen und Ziele in Worten:') ?></h4>
                    <textarea name="tasks" placeholder="<?php for($i = 0; $i <500; $i++){echo '— ';}?>"
                              maxlength="144"><?=$condition->tasks?></textarea>
                </div>
            </div>
            <div class="col-12 flex-row ">
                <button type="submit" class="float-left btn btn-lg btn-grey-light">Bearbeiten</button>
                <button type="submit" class="float-right btn btn-lg btn-grey-light">Speichern</button>
            </div>
        </form>
    </div>
</div>
