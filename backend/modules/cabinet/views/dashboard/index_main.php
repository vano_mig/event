<?php

use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="hello-user"><?= Yii::t('cabinet', 'Hallo {name}', ['name' => $user->username]) ?></div>
    <div class="cabinet-info">
        <div><?= Yii::t('cabinet', 'Willkommen in deinem persönlichen TJB') ?></div>
        <div><?= Yii::t('cabinet', 'Du arbeitest im Moment mit deinem Konto (Kontonummer oder Name)') ?></div>
    </div>
    <div class="mt-4">
        <div class="d-flex flex-row justify-content-center text-center menu-block-content">
            <!--            <a href="-->
            <? //= Url::toRoute('/cabinet/zero/profile') ?><!--" class="loop-href disabled">-->
            <!--                <div class="cabinet-block zero menu-cab">-->
            <!--                    <div class="info-block">-->
            <? //= Yii::t('cabinet', 'Erstelle deinen Account') ?><!--</div>-->
            <!--                    <div class="system-block">--><? //= Yii::t('cabinet', 'Step One') ?><!--</div>-->
            <!--                </div>-->
            <!--            </a>-->
            <a href="<?= Url::toRoute('/cabinet/zero/profile') ?>" class="loop-href">
                <div class="cabinet-block zero menu-cab d-flex flex-row">
                    <div class="system-block"><?= Yii::t('cabinet', 'Step One') ?></div>
                    <div class="info-block"><?= Yii::t('cabinet', 'Bearbeite deinen Account') ?></div>
                </div>
            </a>
        </div>
       <!-- <div class="d-flex flex-row justify-content-center text-center">
            <a href="<?/*= Url::toRoute('/cabinet/step-one') */?>" class="loop-href"
            <?php /*if ($step_status >=1) echo 'title="Step finished"'*/?>>
                <div class="cabinet-block menu-cab one <?/*= (int)$step_status == 0 ? 'error' : '' */?>">
                    <div class="system-block"><?/*= Yii::t('cabinet', 'Step Two') */?></div>
                    <div class="info-block"><?/*= Yii::t('cabinet', 'Erstelle deinen Tradingplan') */?></div>
                </div>
            </a>
            <a href="<?/*= Url::toRoute('/cabinet/step-one') */?>" class="loop-href" title="<?php /*if ($step_status < 1) echo 'Please fill step before'*/?>">
                <div class="cabinet-block one menu-cab <?/*= (int)$step_status == 0 ? 'error' : '' */?>">
                    <div class="system-block"><?/*= Yii::t('cabinet', 'Step Two') */?></div>
                    <div class="info-block"><?/*= Yii::t('cabinet', 'Bearbeite deinen Tradingplan') */?></div>
                </div>
            </a>
        </div>-->
        <div class="d-flex flex-row justify-content-center text-center menu-block-content">
            <a href="<?= Url::toRoute('/cabinet/step-one') ?>" class="loop-href" title="<?php if ($step_status >= 1 ) echo 'Step finished'?>">
                <div class="cabinet-block menu-cab d-flex flex-row <?= (int)$step_status == 0 ? 'error' : '' ?> one">
                    <div class="system-block"><?= Yii::t('cabinet', 'Step Two') ?></div>
                    <div class="info-block"><?= Yii::t('cabinet', 'Erstelle deinen Tradingplan') ?></div>
                </div>
            </a>
            <a href="<?= Url::toRoute('/cabinet/step-one') ?>" class="loop-href" title="<?php if ($step_status < 1) echo 'Please fill step before'?>">
                <div class="cabinet-block two menu-cab one d-flex flex-row <?= (int)$step_status == 0 ? 'error' : '' ?>">
                    <div class="system-block"><?= Yii::t('cabinet', 'Step Two') ?></div>
                    <div class="info-block"><?= Yii::t('cabinet', 'Bearbeite deinen Tradingplan') ?></div>
                </div>
            </a>
        </div>
        <div class="d-flex flex-row justify-content-center text-center menu-block-content">
            <a href="<?= Url::toRoute('/cabinet/step-two') ?>" class="loop-href" title="<?php if ($step_status != 1 && $step_status < 2) echo 'Please fill step before'?> <?php if ($step_status >=2) echo 'Step finished'?>">
                <div class="cabinet-block menu-cab d-flex flex-row <?= (int)$step_status == 1 ? 'error' : '' ?> two">
                    <div class="system-block"><?= Yii::t('cabinet', 'Step Three') ?></div>
                    <div class="info-block"><?= Yii::t('cabinet', 'Einen Markt anlegen') ?></div>
                </div>
            </a>
            <a href="<?= Url::toRoute('/cabinet/step-two/update-markets') ?>" class="loop-href" title="<?php if ($step_status < 2) echo 'Please fill step before'?>">
                <div class="cabinet-block two menu-cab two d-flex flex-row">
                    <div class="system-block"><?= Yii::t('cabinet', 'Step Three') ?></div>
                    <div class="info-block"><?= Yii::t('cabinet', 'Einen Markt bearbeiten') ?></div>
                </div>
            </a>
        </div>
        <div class="d-flex flex-row justify-content-center text-center menu-block-content">
            <a href="<?= Url::toRoute('/cabinet/step-three') ?>" class="loop-href" title="<?php if ($step_status != 2 && $step_status < 3) echo 'Please fill step before'?> <?php if ($step_status >=3) echo 'Step finished'?>">
                <div class="cabinet-block menu-cab three d-flex flex-row <?= (int)$step_status == 2 ? 'error' : '' ?>">
                    <div class="system-block"><?= Yii::t('cabinet', 'Step Four') ?></div>
                    <div class="info-block"><?= Yii::t('cabinet', 'Einen Trade aufnehmen') ?></div>
                </div>
            </a>
            <a href="<?= Url::toRoute('/cabinet/step-three/new-trade') ?>"
               class="loop-href" title="<?php if ($step_status < 3) echo 'Please fill step before'?>">
                <div class="cabinet-block menu-cab three d-flex flex-row">
                    <div class="system-block"><?= Yii::t('cabinet', 'Step Four') ?></div>
                    <div class="info-block"><?= Yii::t('cabinet', 'Neuer Trade') ?></div>
                </div>
            </a>
        </div>
        <div class="d-flex flex-row justify-content-center text-center menu-block-content">
            <div class="cabinet-block-bg fenster">
<!--                                <div class="system-block">-->
                <? //= Yii::t('cabinet', 'Action-Fenster') ?>
<!--                                    </div>-->
                <div class="justify-content-center text-center">
                    <p class="block-title pt-2"><?= Yii::t('cabinet', 'Your Personal Info Board') ?></p>
                    <div class="block-info-body">
                        <?php if(empty($dashboard)):?>
                        <p>
                            <?= Yii::t('cabinet', 'Dieses Board weißt dich auf einen Fehler hin') ?>
                            <br><?= Yii::t('cabinet', 'wenn du z.B. gegen diene eigenen.') ?>
                        </p>
                        <p>
                            <?= Yii::t('cabinet', 'Regeln aus dienem Tradingplan werstößt, und warhnt dich vor deinen eigenen Ego.') ?>
                        </p>
                        <p>
                            <?= Yii::t('cabinet', 'Außerdem weißt es dich auf fehlende Eintragungen in deiner') ?>
                            <br><?= Yii::t('cabinet', 'Trade-Documentation hin') ?>
                        </p>
                        <p>
                            <?= Yii::t('cabinet', 'Immer wieder laufen heir deine Motivationsbilder aus deinem Tradingplan durch.') ?>
                        </p>
                        <?php else :?>
                            <div class="text-danger mb-2">
                                <?php if(!empty($dashboard['time']))
                                    echo $dashboard['time']?>
                            </div>
                            <div class="text-danger mb-4">
                                <?php if(!empty($dashboard['trade']))
                                    echo $dashboard['trade']?>
                            </div>
                            <div class="mb-4">
                                <?php if(!empty($dashboard['emojy'])):?>
                                <!-- Slideshow container -->
                                <div class="slideshow-container">
                                    <?php foreach ($dashboard['emojy'] as $img):?>
                                    <!-- Full-width images with number and caption text -->
                                    <div class="mySlides fade">
                                        <img src="<?=$img?>" class="slide-image">
                                    </div>
                                    <?php endforeach?>
                                </div>

                                <!-- The dots/circles -->
<!--                                <div style="text-align:center">-->
<!--                                    <span class="dot" onclick="currentSlide(1)"></span>-->
<!--                                    <span class="dot" onclick="currentSlide(2)"></span>-->
<!--                                    <span class="dot" onclick="currentSlide(3)"></span>-->
<!--                                </div>-->
                                <?php endif?>
                            </div>
                        <?php endif?>
                    </div>
                </div>
            </div>
        </div>
        <a href="<?= Url::toRoute('/cabinet/research') ?>"
           class="loop-href <?= (int)$step_status < 3 ? 'disabled' : '' ?>"">
            <div class="d-flex flex-row justify-content-center text-center">
                <div class="cabinet-block research" id="research">
                    <!--                <div class="system-block">-->
                    <? //= Yii::t('cabinet', 'Step research') ?><!--</div>-->
                    <div class="info-block"><?= Yii::t('cabinet', 'Statistik') ?></div>
                </div>
            </div>
        </a>
    </div>
</div>
