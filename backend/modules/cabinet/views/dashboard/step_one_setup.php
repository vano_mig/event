<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Deine Setups') ?></div>
    <div class="mt-4 step-one-text step-bottom">
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Nun kommen wir zu deinen persönlichen Setups mit denen du in Zukunft arbeiten wirst.')?>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?= Yii::t('cabinet', 'Beschreibe deine Setups so ausführlich wie nur irgendwie möglich.')?>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?= Yii::t('cabinet', 'Du kannst in deinem TJB mehrere Setups festlegen.') ?></div>
            <div><?= Yii::t('cabinet', 'Allerdings wird es dir zu Beginn deiner Tradingkarriere nichts einbringen, wenn du mit zu vielen Setups hantierst.') ?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center mb-4">
            <?=Yii::t('cabinet', 'Deshalb empfehlen wir dir dringend am Anfang mit nur 2 Setups zu arbeiten.')?>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?= Yii::t('cabinet', 'Je besser du diese im Laufe der Zeit beherrschst desto erfolgreicher wirst du sein!') ?></div>
            <div> <?= Yii::t('cabinet', 'Weniger ist auch in diesem Fall mehr!') ?></div>
        </div>
        <div class="d-flex flex-row justify-content-center text-center step-bottom">
            <?= Yii::t('cabinet', '5 verschiedene Setups nützen dir überhaupt nichts wenn du sie während einer Session durcheinander bringst und vor lauter Aufregung keines davon richtig anwenden kannst.') ?>
        </div>
        <div class="flex-row justify-content-center text-center mb-4">
            <div><?= Yii::t('cabinet', 'Nur zur Information:') ?></div>
                <div> <?= Yii::t('cabinet', 'Wir arbeiten ausschließlich nur mit 3 verschiedenen Setups. Das reicht uns komplett um pro tabel zu traden.') ?></div>
        </div>
    </div>
</div>
