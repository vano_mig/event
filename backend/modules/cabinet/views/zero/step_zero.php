<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title"><?= Yii::t('cabinet', 'Erstelle/Bearbeite deinen Account') ?></div>
    <div class="cabinet-trade">
        <div><?= Yii::t('cabinet', 'Trage hier deine Daten ein') ?></div>
    </div>
    <div class="mt-4">
        <form action="" method="post">
            <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->csrfToken; ?>">
            <div class="d-flex flex-row justify-content-center text-center">
                <div class="profile-block">
                    <div class="profile-content">
                        <label for="name"><?= Yii::t('cabinet', 'Dein Name:') ?></label>
                        <input name="username" type="text" id="name" value="<?= $user->username ?>" placeholder="<?=Yii::t('cabinet', 'Textfeld')?>">
<!--                        <button type="submit">--><?//= Yii::t('cabinet', 'Speichern') ?><!--</button>-->
                    </div>
                </div>
                <div class="profile-block">
                    <div class="profile-content">
                        <label for="password"><?= Yii::t('cabinet', 'Passwort anlegen:') ?></label>
                        <input name="password" type="password" id="password" value="" placeholder="<?=Yii::t('cabinet', '********')?>">
<!--                        <button type="submit">--><?//= Yii::t('cabinet', 'Speichern') ?><!--</button>-->
                    </div>
                </div>
            </div>

            <div class="d-flex flex-row justify-content-center text-center">
                <div class="profile-block bottom-block">
                    <div class="profile-content">
                        <label for="email"><?= Yii::t('cabinet', 'Deine E-mail-Adresse:') ?></label>
                        <input name="email" type="email" id="email" value="<?= $user->email ?>" placeholder="<?=Yii::t('cabinet', 'Textfeld')?>">
<!--                        <button type="submit">--><?//= Yii::t('cabinet', 'Speichern') ?><!--</button>-->
                    </div>
                </div>
                <div class="profile-block bottom-block">
                    <div class="profile-content">
                        <label for="email-agent"><?= Yii::t('cabinet', 'E-mail-Adresse deines Coaches:') ?></label>
                        <input name="coach_email" type="email" id="email-agent" value="<?=$user->coach_email?>" placeholder="<?=Yii::t('cabinet', 'Textfeld')?>">
<!--                        <button type="submit">--><?//= Yii::t('cabinet', 'Speichern') ?><!--</button>-->
                    </div>
                </div>
            </div>
            <div class="content-tooltip tooltip-border">
                <?=Yii::t('cabinet', 'Wenn du hier die Mailadresse deines Coaches einträgst, erhält dieser automatisch eine Information, sobald du einen Trading-Tag beendet hast und alle Trades selbst bereits schon bewertet hast. Er kann dann deine Trades anschauen und seinen Kommentar entsprechend pro Trade hinterlegen. Diese Mailadresse kannst du selbstverständlich jederzeit löschen oder ändern. Sobald dein Coach deine Trades kommentiert hat, wirst du natürlich von deinem TJB sofort informiert')?>
            </div>
            <button type="submit" class="mt-4 btn btn-lg btn-grey-light">Bearbeiten</button>
        </form>
    </div>
</div>
