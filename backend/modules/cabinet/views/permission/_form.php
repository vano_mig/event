<?php

/* @var $role null|yii\rbac\Role */
/* @var $permissions array|Yii\rbac\Permission[] */
/* @var $this yii\web\View */

/* @var $userPermissions array */

use yii\bootstrap4\Html;

$i = 0;
$lastKey = array_key_last($permissions);
?>

<?php echo Html::beginForm('', 'post', ['class' => '']); ?>
    <div class="user-form">
        <div class="col-12 ">
            <?php if (!empty($permissions)): ?>
                <?php $section = '' ?>
                <div class="row">
                    <?php foreach ($permissions as $key => $permission): ?>
                        <?php $i++ ?>
                        <?php if ($i == 1) { ?>
                            <div class="row col-12">
                        <?php } ?>
                        <div class="col border p-2 min-wr-18">
                            <?php if ($section != $key) : ?>
                                <?php $section = $key ?>
                                <h3 class="text-wrap"><?php echo Yii::t('permissions', $section); ?></h3>
                            <?php endif ?>
                            <?php foreach ($permission as $one_item): ?>
                                <div class="custom-control custom-checkbox">
                                    <?php echo Html::checkbox('Permission[' . $one_item->name . ']',
                                        array_key_exists($one_item->name, $userPermissions) ? 1 : 0, ['class' => 'custom-control-input', 'id' => 'Permission[' . $one_item->name . ']']); ?>
                                    <label class="custom-control-label text-wrap"
                                           for="Permission[<?php echo $one_item->name ?>]"><?php echo Yii::t('permissions',
                                            $one_item->name) ?></label>
                                </div>
                            <?php endforeach ?>
                        </div>
                        <?php if ($i == 3 || ($key == $lastKey && $i != 3)) { ?>
                            </div>
                            <?php $i = 0 ?>
                        <?php } ?>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
            <?php echo Html:: hiddenInput(Yii:: $app->getRequest()->csrfParam, Yii:: $app->getRequest()->getCsrfToken(),
                []); ?>
            <?php echo Html:: hiddenInput('role', $role->name, []); ?>
        </div>
        <div class="col-12 mt-4 pt-4 border-top">
            <div class="row col-12 col-lg-8 mx-auto">
                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn bg-gradient-success btn-block']); ?>
                </div>
                <div class="col min-wr-10  max-wr-20 px-3 py-2 mx-auto">
                    <?php echo Html::a(Yii::t('admin', 'Back'), Yii::$app->request->referrer,
                        ['class' => 'btn bg-gradient-primary btn-block']); ?>
                </div>
            </div>
        </div>
    </div>
<?php echo Html::endForm() ?>