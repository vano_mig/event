<?php

/* @var $roles array|Yii\rbac\Role[] */
/* @var $this yii\web\View */

/* @var $model backend\modules\user\models\Permission */

use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;

$this->title = Yii::t('admin', 'Rules');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-header">
    <?= Breadcrumbs::widget([
        'links' => $this->params['breadcrumbs'] ?? [],
        'homeLink' => ['label' => Yii::t('admin', 'System')],
    ]) ?>
</div>
<div class="content">
    <div class="card card-success card-outline">
        <div class="card-header p-0">
            <h3 class="card-title p-2 my-1 ml-2"><i class="fas fa-exclamation-triangle"></i> <?php echo Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
            <div class="col mx-auto col-8 my-5">
                <div class="table-responsive border rounded elevation-2">
                    <table class="table table-condensed table-bordered m-0 table-border-0">
                        <thead>
                        <tr>
                            <th class="align-middle">
                                <?php echo Yii::t('admin', 'Role'); ?>
                            </th>
                            <th class="align-middle">
                                <?php echo Yii::t('admin', 'Count permissions'); ?>
                            </th>
                            <th class="align-middle text-center">
                                <?php echo Yii::t('admin', 'Actions'); ?>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($roles)): ?>
                            <?php foreach ($roles as $role): ?>
                                <tr>
                                    <td class="align-middle">
                                        <?php echo Yii::t('users', $role->name); ?>
                                    </td>
                                    <td class="align-middle">
                                        <?php echo $model->getCountPermissionsByRole($role->name); ?>
                                    </td>
                                    <td class="align-middle text-center p-1">
                                        <?php echo Html::a(Yii::t('admin', 'Update'), ['update', 'id' => $role->name], ['class' => 'btn bg-gradient-warning btn-sm']); ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

