<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <!--    <header>-->
    <!--        --><?php
    //        NavBar::begin([
    //            'brandLabel' => Yii::$app->name,
    //            'brandUrl' => Yii::$app->homeUrl,
    //            'options' => [
    //                'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
    //            ],
    //        ]);
    //        $menuItems = [
    //            ['label' => 'Home', 'url' => ['/site/index']],
    //        ];
    //        if (Yii::$app->user->isGuest) {
    //            $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    //        } else {
    //            $menuItems[] = '<li>'
    //                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
    //                . Html::submitButton(
    //                    'Logout (' . Yii::$app->user->identity->username . ')',
    //                    ['class' => 'btn btn-link logout']
    //                )
    //                . Html::endForm()
    //                . '</li>';
    //        }
    //        echo Nav::widget([
    //            'options' => ['class' => 'navbar-nav'],
    //            'items' => $menuItems,
    //        ]);
    //        NavBar::end();
    //        ?>
    <!--    </header>-->

    <main role="main" class="flex-shrink-0" id="trading">
        <div class="container">
            <div class="content">

                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <div class="cabinet-brand-name">
                    <?= Html::a(Html::img('/images/system/tj3.png', ['class' => 'image']), Url::toRoute('/cabinet/dashboard')); ?>
                    <div class="cabinet-main-line"></div>
                </div>
                <div class="cabinet-body">
                    <?= $content ?>
                    <footer class="footer mt-auto pt-3">
                        <?php $step = isset($this->params['step']) ? true : false ?>
                        <div class="container">
                            <?php if ($step): ?>
                            <?php $step = $this->params['step']?>
                                <div class="row justify-content-center text-center">
<!--                                    --><?php //if ($this->params['step']['buttons']['left']['visible'] == true): ?>
                                        <div class="pagination-circle-left" style="<?php if($step['buttons']['left']['visible'] == false) echo "visibility:hidden;"?>">
                                            <a href="<?= $this->params['step']['buttons']['left']['link'] ?>"><span
                                                        class="fas fa-arrow-circle-left"></span></a>
                                        </div>
<!--                                    --><?php //endif ?>
                                    <div class="footer-logo">
                                        <span class="text-center"><?= Html::img('/images/system/footer_logo.png') ?></span>
                                    </div>
                                    <?php if (array_key_exists('active', $this->params['step']['buttons']['right']) && $this->params['step']['buttons']['right']['active'] == false)
                                        $active = 'disabled';
                                    else
                                        $active = '';
                                    ?>

<!--                                    --><?php //if ($this->params['step']['buttons']['right']['visible'] == true): ?>
                                        <div class="pagination-circle-right" style="<?php if($step['buttons']['right']['visible'] == false) echo "visibility:hidden;"?>">
                                            <a href="<?= $this->params['step']['buttons']['right']['link'] ?>"
                                               class="<?= $active ?>">
                                                <span class="fas fa-arrow-circle-right"></span></a>
                                        </div>
<!--                                    --><?php //endif ?>

                                </div>
                            <?php else : ?>
                                    <p class="text-center"><?= Html::img('/images/system/footer_logo.png') ?></p>
                                    <!--            <p class="float-left">&copy; -->
                                    <? //= Html::encode(Yii::$app->name) ?><!-- --><? //= date('Y') ?><!--</p>-->
                                    <!--            <p class="float-right">--><? //= Yii::powered() ?><!--</p>-->
                            <?php endif ?>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </main>

    <!--    <footer class="footer mt-auto py-3 text-muted">-->
    <!--        <div class="container">-->
    <!--            <p class="text-center">--><?//= Html::img('/images/system/footer_logo.png') ?><!--</p>-->
    <!--            <p class="float-left">&copy; --><?//= Html::encode(Yii::$app->name) ?><!-- -->
    <?//= date('Y') ?><!--</p>-->
    <!--            <p class="float-right">--><?//= Yii::powered() ?><!--</p>-->
    <!--        </div>-->
    <!--    </footer>-->

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage();
