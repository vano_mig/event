<?php

namespace backend\modules\cabinet\models;

use backend\models\Message;
use Yii;
use yii\base\Model;

class MessageModel extends Model
{
    public $id;
    public $language;
    public $translation;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language','translation'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'language' => Yii::t('translation', 'Language'),
            'translation' => Yii::t('translation', 'Translation'),
        ];
    }

    public static function getMessages($id)
    {
        return Message::find()->where(['id'=>$id])->all();
    }
}