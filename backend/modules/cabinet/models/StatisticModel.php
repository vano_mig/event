<?php

namespace backend\modules\cabinet\models;

use backend\models\Statistic;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class StatisticModel extends Model
{
    public $id;
    public $userId;
    public $amount;
    public $market;
    public $file;
    public $comment;
    public $createdAt;

    public function rules()
    {
        return [
            [['userID', 'amount', 'market', 'file'], 'required'],
            ['amount', 'validateAmount'],
            ['market', 'trim'],
            ['market', 'required'],
            ['file', 'trim'],
            ['file', 'string'],
            ['comment', 'string', 'min' => 6],
        ];
    }

    public function validateAmount($attribute)
    {
        $this->$attribute = number_format($this->$attribute, 2, '.', '');

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'userId' => Yii::t('statistic', 'User'),
            'amount' => Yii::t('statistic', 'Amount'),
            'market' => Yii::t('statistic', 'Market'),
            'file' => Yii::t('statistic', 'File'),
            'comment' => Yii::t('statistic', 'Comment'),
            'createdAt' => Yii::t('admin', 'Created'),
        ];
    }

    public static function getReportById($id)
    {
        $model = Statistic::findOne($id);
        if($model === null)
            return false;
        $report = new self;
        $report->id = $model->id;
        $report->userId = $model->user_id;
        $report->amount = $model->amount;
        $report->market = $model->market;
        $report->file = $model->file;
        $report->comment = $model->comment;
        $report->createdAt = $model->created_at;
        return $report;
    }

    public static function saveReport($reportObj)
    {
        $model = Statistic::findOne($reportObj->id);
        if($model === null) {
            $model = new Statistic();
        }

        $model->user_id = $reportObj->userId;
        $model->amount = $reportObj->amount;
        $model->market = $reportObj->market;
        $model->file = $reportObj->file;
        $model->comment = $reportObj->comment;
        if($model->save())
            return true;
        return false;
    }

    public function searchReportList($params)
    {
        $this->load($params);

        $query = Statistic::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => 'id DESC'],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'user_id',
                'amount',
                'market',
                'file',
                'comment',
                'created_at',
            ]
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->userId,
            'amount'=>$this->amount,
            'market'=>$this->market,
            'file'=>$this->file,
            'comment' => $this->comment,
            'created_at' => $this->createdAt,
        ]);

        return $dataProvider;
    }
}