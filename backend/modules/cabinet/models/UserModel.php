<?php

namespace backend\modules\cabinet\models;

use backend\models\User;
use Yii;
use yii\data\ActiveDataProvider;

class UserModel extends \backend\models\classes\UserModel
{
    public $id;
    public $username;
    public $email;
    public $coach_email;
    public $password_hash;
    public $status;
    public $role;
    public $created_at;
    public $updated_at;
    public $logged;

    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['coach_email', 'trim'],
//            ['coach_email', 'required'],
            ['coach_email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User',
                'message' => Yii::t('users', 'This email address has already 
                been taken.'), 'on' => 'insert'],
            ['email', 'unique', 'filter' => ['!=', 'id', $this->id],
                'targetClass' => '\common\models\User',
                'message' => Yii::t('users', 'This email address has already 
                been taken.')],

            ['password_hash', 'trim'],
            ['password_hash', 'required',],
            ['password_hash', 'string', 'min' => 6],

            ['status', 'default', 'value' => User::STATUS_ACTIVE],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE,
                User::STATUS_INACTIVE,  User::STATUS_DELETED]],

            ['role', 'default', 'value' => User::ROLE_USER],
            ['role', 'in', 'range' => [User::ROLE_USER, User::ROLE_MANAGER, User::ROLE_ADMIN]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'created_at' => Yii::t('admin', 'Created'),
            'updated_at' => Yii::t('admin', 'Updated'),
            'username' => Yii::t('users', 'Username'),
            'password_hash' => Yii::t('users', 'Password'),
            'email' => Yii::t('users', 'Email'),
            'coach_email' => Yii::t('users', 'Coach Email'),
            'role' => Yii::t('users', 'Role'),
            'status' => Yii::t('users', 'Status'),
        ];
    }

    public function searchUserList($params)
    {
        $this->load($params);

        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => 'id DESC'],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'username',
                'email',
                'status',
                'role',
                'created_at',
                'updated_at',

            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'role'=>$this->role,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email]);
        return $dataProvider;
    }

    public static function getStatuses():array
    {
        return [
            User::STATUS_ACTIVE => Yii::t('user', 'Active'),
            User::STATUS_INACTIVE => Yii::t('user', 'Inactive'),
            User::STATUS_DELETED => Yii::t('user', 'Deleted'),
        ];
    }

    public static function getRoles()
    {
        return [
            User::ROLE_USER => Yii::t('user', User::ROLE_USER),
            User::ROLE_MANAGER => Yii::t('user', User::ROLE_MANAGER),
            User::ROLE_ADMIN => Yii::t('user', User::ROLE_ADMIN),
        ];
    }

    public static function getRolesForManager()
    {
        return [
            User::ROLE_USER => Yii::t('user', User::ROLE_USER),
            User::ROLE_MANAGER => Yii::t('user', User::ROLE_MANAGER),
        ];
    }

    public static function saveUser($userObj)
    {
        if($userObj->id) {
            $model = User::findOne($userObj->id);
            if($userObj->password_hash != $model->password_hash) {
                $model->setPassword($userObj->password_hash);
            }
//            $model->password_hash = $userObj->password_hash;
        } else {
            $model = new User();
            $model->setPassword($userObj->password_hash);

        }
        $model->username = $userObj->username;
        $model->email = $userObj->email;
        $model->coach_email = $userObj->coach_email;
        $model->status = $userObj->status;
        $model->role = $userObj->role;
        if($model->save())
            return true;
        return false;
    }

    public static function getUserById($id)
    {
        $user =  User::findOne($id);
        if($user === null)
            return false;
        $model = new self;
        $model->id = $user->id;
        $model->username = $user->username;
        $model->email = $user->email;
        $model->password_hash = $user->password_hash;
        $model->role = $user->role;
        $model->status = $user->status;
        $model->coach_email = $user->coach_email;
        $model->created_at = $user->created_at;
        $model->updated_at = $user->updated_at;
        $model->logged = $user->logged;

        return $model;
    }

    public static function deleteUser($id)
    {
        $user = User::findById($id);
        if($user && $user->delete())
            return true;

        return false;
    }

    public static function getUserActiveList():array
    {
        $list = [];
        $query = User::find()->where(['status'=>User::STATUS_ACTIVE])->all();
        if(!empty($query)) {
            foreach ($query as $item) {
                $list[$item->id] = $item->email;
            }
        }
        return $list;
    }
}