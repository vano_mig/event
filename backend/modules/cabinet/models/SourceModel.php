<?php

namespace backend\modules\cabinet\models;

use backend\models\Message;
use backend\models\SourceMessage;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SourceModel extends Model
{
    const  STATUS_OK = 1;
    const  STATUS_EDIT = 2;

    public $id;
    public $message;
    public $category;
    public $translation;
    public $islanguage;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'category'], 'required'],
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
            [['message', 'category'], 'unique', 'targetAttribute' => ['message', 'category']],
            ['isLanguage', 'safe'],
            ['translation', 'safe']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'category' => Yii::t('translation', 'Category'),
            'message' => Yii::t('translation', 'Message'),
        ];
    }
    /**
     * Get list user statuses
     * @param null $id string
     * @return array|mixed
     */
    public function getStatuses($id = null)
    {
        $array = [
            self::STATUS_OK => Yii::t('translation', 'Ok'),
            self::STATUS_EDIT => Yii::t('translation', 'Edit'),
        ];
        if ($id)
            return $array[$id];
        return $array;
    }
    /**
     * @return \yii\db\ActiveQuery
     */

    public function getMessages()
    {
        $lang = LanguageModel::getActiveCodeLanguages();
        return SourceMessage::getMessagesList($lang);
    }

    public function search($params)
    {
        $query = SourceMessage::find()->joinWith(['messages'])->distinct();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'sort' => [
                'defaultOrder' => [
                    'source_message.id' => SORT_DESC,
                ],
                'attributes' => [
                    'source_message.id',
                    'category',
                    'message' => [
                        'asc' => ['source_message.message' => SORT_ASC],
                        'desc' => ['source_message.message' => SORT_DESC],
                    ],
                    'translation' => [
                        'asc' => ['message.translation' => SORT_ASC],
                        'desc' => ['message.translation' => SORT_DESC],
                    ],
                ],
            ],

            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
echo "<pre>";print_r($params);
$this->islanguage = $params['SourceModel']['islanguage'];
        $this->load($params);
        echo "<pre>";print_r($this);die;
        if ($this->islanguage) {
            if ($this->islanguage != 'message') {
                if ($this->islanguage == '0') {
                    $query->where(['message.language' => $this->islanguage]);
                    $query->andWhere(['not in', 'message.translation', [null, '']]);
                    $query->andWhere(['REGEXP', 'message.translation', '[^\s*$]']);
                } else {
                    $query->where(['message.language' => $this->islanguage]);
                    $query->andWhere(['in', 'message.translation', [null, '']]);
                    $query->orWhere(['REGEXP', 'message.translation', '/^\s*$/']);
                }
            }
        }


//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'source_message.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category]);
        $query->andFilterWhere(['like', 'message', $this->message]);
        $query->andFilterWhere(['like', 'message.translation', $this->translation]);
//        $query->andFilterWhere(['like', 'source_message.message', $this->message]);


        return $dataProvider;
    }

    public static function saveMessage($modelObj)
    {
        $model = SourceMessage::findOne($modelObj->id);
        if($model === null) {
            $model = new SourceMessage();
        }
        $model->category = $modelObj->category;
        $model->message = $modelObj->message;

        if($model->save())
            return true;

        return false;
    }

    public static function getSourceMessage($id)
    {
        $model = SourceMessage::findOne($id);
        if($model) {
            $source = new self;
            $source->id = $model->id;
            $source->category = $model->category;
            $source->message = $model->message;

            return $source;
        }
        return null;
    }
}