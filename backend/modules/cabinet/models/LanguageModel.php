<?php

namespace backend\modules\cabinet\models;

use backend\models\classes\LanguageModel as Languages;
use backend\models\Language;
use Yii;
use yii\data\ActiveDataProvider;

class LanguageModel extends Languages
{
    public static function getListModels():array
    {
        return Language::find()->asArray()->all();
    }

    /**
     * Get list or language statuses
     * @return array
     */
    public static  function getStatuses():array
    {
        $array = [
            Language::STATUS_ACTIVE => Yii::t('language', 'active'),
            Language::STATUS_INACTIVE => Yii::t('language', 'inactive')
        ];

        return $array;
    }

    public static function searchLanguageList()
    {
        $query = Language::find();

        $provider = new ActiveDataProvider([
            'query' => $query,
//            'pagination' => [
//                'pageSize' => 20,
//            ],
        ]);
        return $provider;
    }

    public static function saveLanguage($langugaeObj)
    {
        $model = null;
        if($langugaeObj->id != false) {
            $model = Language::findOne($langugaeObj->id);

        }

        if($model == null)
            $model = new Language();

        $model->name = $langugaeObj->name;
        $model->iso_code = $langugaeObj->iso_code;
        $model->system = $langugaeObj->system;
        $model->status = $langugaeObj->status;
        if($model->save()) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                'name',
                'required',
                'message' => Yii::t('language',
                    'You should choose a language.')
            ],

            ['system', 'default', 'value' => Language::STATUS_INACTIVE],
            ['system', 'in', 'range' => [Language::STATUS_ACTIVE, Language::STATUS_INACTIVE]],

            ['status', 'default', 'value' => Language::STATUS_ACTIVE],
            ['status', 'in', 'range' => [Language::STATUS_ACTIVE, Language::STATUS_INACTIVE]]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('country', 'Name'),
            'iso_code' => Yii::t('country', 'Iso Code'),
            'system' => Yii::t('country', 'system'),
            'status' => Yii::t('admin', 'Status'),
        ];
    }

    /**
     * check language activeness - one language should always be active
     * return void
     */
    public static function checkMainLang($status, $id): void
    {

        if ($status == Language::STATUS_ACTIVE) {
            Language::updateAll(['system' => Language::STATUS_INACTIVE]);
        } else {
            $active = Language::find()->where(['status' => Language::STATUS_ACTIVE])->one();
            if (!$active) {
                $default = Language::find()->where(['iso_code' => Yii::$app->language])->one();
                if (!$default) {
                    $default = Language::find()->orderBy(['id' => SORT_DESC])->one();
                }
                $default->system = Language::STATUS_ACTIVE;
                $default->status = Language::STATUS_ACTIVE;
                $default->save();
            } elseif ($id == $active->id) {
                $default = Language::find()->where(['!=', 'id', $id])->orderBy(['id' => SORT_DESC])->one();
                $default->system = Language::STATUS_ACTIVE;
                $default->status = Language::STATUS_ACTIVE;
                $default->save();
            }
        }
    }

    public static function getLanguageById($id)
    {
        $model = new self;
        $language = Language::findOne($id);
        if($language) {
            $model->id = $language->id;
            $model->name = $language->name;
            $model->iso_code = $language->iso_code;
            $model->system = $language->system;
            $model->status = $language->status;
        }
        return $model;
    }

    public static function deleteLanguage($id)
    {
        if(Language::findOne($id)->delete())
            return true;
        return false;
    }

    public static function getActiveCodeLanguages()
    {
        $list = Language::find()->where(['status' => Language::STATUS_ACTIVE])->all();
        $array = [];
        if ($list) {
            foreach ($list as $item) {
                $array[] = $item->iso_code;
            }
        }
        return $array;
    }

    public static function getActiveLanguages()
    {
        $list = Language::find()->where(['status' => Language::STATUS_ACTIVE])->all();
        $array = [];
        if ($list) {
            foreach ($list as $item) {
                $array[$item->iso_code] = $item->name;
            }
        }
        return $array;
    }
}