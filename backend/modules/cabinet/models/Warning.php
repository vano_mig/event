<?php

namespace backend\modules\cabinet\models;

use backend\models\classes\UserLimitModel;
use backend\models\UserTrade;
use Yii;

class Warning
{

    public static function getDashboard($condition)
    {
        $result = [];
        if ($condition->stepStatus == 0) {
            return $result;
        }

        $today = strtolower(date('D'));
        if (in_array($today, ['sat, sun'])) {
            $result['time'] = Yii::t('cabinet', 'today is day off');
        } else {
            $setupDays = json_decode($condition->timeWork, true);
            if (!empty($setupDays)) {
                $day = $setupDays[$today];

                if (empty($day) || $day == '') {
                    $result['time'] = Yii::t('cabinet', 'today is day off');
                } else {
                    $time = explode('/', $day);
                    $date = date('Y-m-d H:i');
                    $timeNow = strtotime($date);
                    if (!empty($time) && $timeNow < strtotime(date('Y-m-d') . ' ' . $time[0])) {
                        $result['time'] = Yii::t('cabinet', 'You work outside the plan. Your work time is {time}', ['time' => $day]);
                    }

                    if (!empty($time) && $timeNow > strtotime(date('Y-m-d') . ' ' . $time[1])) {
                        $result['time'] = Yii::t('cabinet', 'You work outside the plan. Your work time is {time}', ['time' => $day]);
                    }
                }
            }
            $result['emojy'] = [];
            if ($condition->photo_1)
                $result['emojy'][] = $condition->photo_1;
            if ($condition->photo_2)
                $result['emojy'][] = $condition->photo_2;
            if ($condition->photo_3)
                $result['emojy'][] = $condition->photo_3;
            if ($condition->photo_4)
                $result['emojy'][] = $condition->photo_4;

            if (empty($result['emojy']))
                unset($result['emojy']);

            $today = date('Y-m-d');
//            $sunday = date('Y-m-d', strtotime('sunday this week'));

            $tradings = UserTrade::find()->where(['>=', 'finish_trade', $today . ' 00:00:00'])
                ->andWhere(['<=', 'finish_trade', $today . ' 23:59:59'])
                ->andWhere(['user_id' => Yii::$app->user->id])->count();

            if ($tradings > 0 && $tradings > $condition->maxTrade) {
                $result['trade'] = Yii::t('cabinet', 'You have exceeded the number of trades ({trade} from {max})', ['trade' => $tradings, 'max' => $condition->maxTrade]);

            }

            $tradings = UserTrade::find()->where(['>=', 'finish_trade', $today . ' 00:00:00'])
                ->andWhere(['<=', 'finish_trade', $today . ' 23:59:59'])
                ->andWhere(['user_id' => Yii::$app->user->id])->count();

            if ($tradings > 0 && $tradings > $condition->maxDecreaseTrade) {
                $result['trade'] = Yii::t('cabinet', 'You have exceeded the number of losing trades ({trade} from {max})', ['trade' => $tradings, 'max' => $condition->maxDecreaseTrade]);
            }

            $today = Yii::$app->user->identity->logged;
            $tradings = UserTrade::find()->where(['>=', 'finish_trade', $today])
                ->andWhere(['user_id' => Yii::$app->user->id])->count();

            if ($tradings > 0 && $tradings > $condition->maxDecreasePointTrade) {
                $result['trade'] = Yii::t('cabinet', 'You have exceeded the number of losing trades per session ({trade} from {max})', ['trade' => $tradings, 'max' => $condition->maxDecreasePointTrade]);
            }

            $limit = UserLimitModel::getLimitByUserId(Yii::$app->user->id);

            if ($limit->limit && $limit->limit >= $condition->limitDailyStep) {
                $result['trade'] = Yii::t('cabinet', 'You have exceeded the number of losing trades ({trade} from {max})', ['trade' => $limit->limit, 'max' => $condition->limitDailyStep]);
            }

            if ($limit->limitAmount && $limit->limitAmount >= $condition->limitDailyMax) {
                $result['trade'] = Yii::t('cabinet', 'you have exceeded the limit of funds ({trade} from {max})', ['trade' => $limit->limitAmount, 'max' => $condition->limitDailyMax]);
            }

            if ($limit->amountWeekly && $limit->amountWeekly >= $condition->limitWeekly) {
                $result['trade'] = Yii::t('cabinet', 'you have exceeded the limit of weekly funds ({trade} from {max})', ['trade' => $limit->amountWeekly, 'max' => $condition->limitWeekly]);
            }
        }

        return $result;
    }
}