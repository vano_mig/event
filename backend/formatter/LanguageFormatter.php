<?php

namespace backend\formatter;

use backend\models\Language;
use backend\modules\cabinet\models\LanguageModel;
use yii\bootstrap4\Html;

class LanguageFormatter
{
    /**
     * Get country flag to language
     * @param $value string
     * @return string
     */
    public static function asLangFlag($data): string
    {

        $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-' . $data['iso_code']]);
        if ($data['iso_code'] == 'uk') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-ua']);
        }
        if ($data['iso_code'] == 'en') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-gb']);
        }
        return $res;
    }

    /**
     * Get country flag to language
     * @param $value string
     * @return string
     */
    public static function asLang($data): string
    {

        $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-' . $data['iso_code']]) . Html::tag('span',
                $data['iso_code']);
        if ($data['iso_code'] == 'uk') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-ua']) . Html::tag('span', $data['iso_code']);
        }
        if ($data['iso_code'] == 'en') {
            $res = Html::tag('span', '', ['class' => 'mr-2 flag-icon flag-icon-gb']) . Html::tag('span', $data['iso_code']);
        }
        return $res;
    }

    /**
     * Get Language status
     * @param $value string
     * @return string
     */
    public static function asStatus($value): string
    {
        $status = LanguageModel::getStatuses();
        if(array_key_exists($value, $status)) {
            if ($value == Language::STATUS_ACTIVE) {
                return Html::tag('span', $status[$value], ['class' => 'badge badge-success']);
            }
        }
        return Html::tag('span', $status[$value], ['class' => 'badge badge-secondary']);
    }
}