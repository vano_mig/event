<?php

namespace backend\formatter;

use backend\models\Language;
use backend\models\User;
use backend\modules\cabinet\models\LanguageModel;
use backend\modules\cabinet\models\UserModel;
use Yii;
use yii\bootstrap4\Html;

class UserFormatter
{
    public static function asRole($role)
    {
        return Yii::t('users', $role);
    }

    /**
     * Get user status
     * @param $value string
     * @return string
     */
    public static function asStatus($value): string
    {
        $status = UserModel::getStatuses();
        if ($value == User::STATUS_ACTIVE) {
            return Html::tag('span', $status[$value], ['class' => 'badge badge-success']);
        } elseif ($value == User::STATUS_INACTIVE) {
            return Html::tag('span', $status[$value], ['class' => 'badge badge-secondary']);
        } elseif ($value == User::STATUS_DELETED) {
            return Html::tag('span', $status[$value], ['class' => 'badge badge-danger']);
        } else {
            return Html::tag('span', Yii::t('user', $value),
                ['class' => 'badge badge-warning']);
        }
    }

    public static function getUserData($id):string
    {
        $user = UserModel::getUserById($id);
        if($user == null)
            $result = Yii::t('admin', 'net set');
        else
            $result = "#$user->id - $user->email";
        return $result;
    }
}