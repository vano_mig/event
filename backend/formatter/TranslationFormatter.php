<?php

namespace backend\formatter;

use backend\modules\cabinet\models\LanguageModel;
use Yii;

class TranslationFormatter
{
    /**
     * @param $value
     * @return array
     */
    public static function getTranslationLanguage()
    {
        $array = [];
        $list = ['message' => Yii::t('admin', '--All--')];
        $list += LanguageModel::getActiveLanguages();
        foreach ($list as $key => $item) {
            $array[$key] = Yii::t('language', $item);
        }
        return $array;
    }
}