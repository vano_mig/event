$(document).ready(function() {
    $('#rituale').submit(function () {
        let form = $('#rituale').serialize();
        let url = $('#rituale').data('url');
        $.ajax({
            url: url,
            data: form,
            method: 'POST',
            success: function (result) {
                if(result.status == 'success') {
                    // alert(result.message);
                    $('.pagination-circle-right a').removeClass('disabled');
                    $('#rituale .cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                    // window.location.replace($('.pagination-circle-right a').attr('href'));
                } else {
                    alert(result.message);
                }
            },
            error: function () {
                alert('Error');
            }
        });
       return false;
    });

    $('#prepare').submit(function () {
        let form = $('#prepare').serialize();
        let url = $('#prepare').data('url');
        $.ajax({
            url: url,
            data: form,
            method: 'POST',
            success: function (result) {
                if(result.status == 'success') {
                    $('.pagination-circle-right a').removeClass('disabled');
                    $('#prepare .cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');

                    // window.location.replace($('.pagination-circle-right a').attr('href'));
                } else {
                    alert(result.message);
                }
            },
            error: function () {
                alert('Error');
            }
        });
       return false;
    });

    $('#documentation').submit(function () {
        let form = $('#documentation').serialize();
        let url = $('#documentation').data('url');
        $.ajax({
            url: url,
            data: form,
            method: 'POST',
            success: function (result) {
                if(result.status == 'success') {
                    $('.pagination-circle-right a').removeClass('disabled');
                    $('#documentation .cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');

                    // window.location.replace($('.pagination-circle-right a').attr('href'));
                } else {
                    alert(result.message);
                }
            },
            error: function () {
                alert('Error');
            }
        });
        return false;
    });

    $('#setup_one').submit(function () {
        let form = $('#setup_one').serialize();
        let url = $('#setup_one').data('url');
        $.ajax({
            url: url,
            data: form,
            method: 'POST',
            success: function (result) {
                if(result.status == 'success') {
                    $('.pagination-circle-right a').removeClass('disabled');
                    $('#setup_one .cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                    $('#setup_one .market-data').css('border', '10px solid rgba(48, 177, 32, 0.65)');

                    // window.location.replace($('.pagination-circle-right a').attr('href'));
                } else {
                    alert(result.message);
                }
            },
            error: function () {
                alert('Error');
            }
        });
        return false;
    });

    $('.photo-block').click(function () {
        let id = $(this).data('id');
        document.getElementById('photo'+id).click();
    });

    $("body").on('change', '.photo-block input', function () {
        let url = $('#photo-form').data('url');
        let id = $(this).closest('.photo-block').data('id');
        let data = $(this).closest('.photo-block').find('.filename'); //.append('<span class="mt4 text-green">file.name</span>');

        let files = this.files;
        if(files && files.length > 0) {
            let file = files[0];

            if (file.size > 1024 * 10485760) {
                alert("Error! Image size must be < 10Mb");
                return false;
            }
            data.empty();
            data.append('<span class="mt4 text-danger">Loading file...</span>');

            if(file.type.match('png') || file.type.match('jpg') || file.type.match('jpeg')) {
                var form_data = new FormData(); // Creating object of FormData class
                form_data.append("file", file); // Appending parameter named file with properties of file_field to form_data
                form_data.append("id", id); // Adding extra parameters to form_data
                $.ajax({
                    url: url,
                    data: form_data,
                    // dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    success: function (result) {
                        if(result.status == 'success') {
                            data.empty();
                            data.append('<span class="mt4 text-green">'+file.name+'</span>');
                            $('.pagination-circle-right a').removeClass('disabled');
                        } else {
                            alert('Error');
                        }
                    },
                    error: function () {
                        alert('Error! Allowed format: PNG, JPG, JPEG');
                    }
                });

                return false;
            } else {
                alert("Error! Allowed format: PNG, JPG, JPEG");
                return false;
            }
        }
        return false;
    });

    $('.sign-photo').click(function () {
        document.getElementById('sign-input').click();
    });

    $('#setup_one_finish').submit(function () {
        let ort = $('input[name="contract_ort"]').val();
        let data = $('input[name="contract_date"]').val();
        let name = $('#sign-input').data('name');
        let url = $('#setup_one_finish').data('url');
        let files = $('#sign-input').prop("files");

        let form_data = new FormData();
        if(files && files.length > 0) {
            let file = files[0];

            if (file.size > 1024 * 10485760) {
                alert("Error! Image size must be < 10Mb");
                return false;
            }

            if(file.type.match('png') || file.type.match('jpg') || file.type.match('jpeg')) {
                // var form_data = new FormData(); // Creating object of FormData class
                form_data.append("file", file); // Appending parameter named file with properties of file_field to form_data
                form_data.append("ort", ort); // Adding extra parameters to form_data
                form_data.append("date", data); // Adding extra parameters to form_data
                $.ajax({
                    url: url,
                    data: form_data,
                    // dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    success: function (result) {
                        if(result.status == 'success') {
                            console.log(result);
                            // data.empty();
                            // data.append('<span class="mt4 text-green">'+file.name+'</span>');
                            $('#setup_one_finish .cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                            $('.pagination-circle-right a').removeClass('disabled');
                        } else {
                            alert(result.message);
                        }
                    },
                    error: function () {
                        alert('Error! Allowed format: PNG, JPG, JPEG');
                    }
                });

                return false;
            } else {
                alert("Error! Allowed format: PNG, JPG, JPEG");
                return false;
            }
        } else if (name.length >0) {
            // form_data = new FormData(); // Creating object of FormData class
            form_data.append("ort", ort); // Adding extra parameters to form_data
            form_data.append("date", data); // Adding extra parameters to form_data
            $.ajax({
                url: url,
                data: form_data,
                // dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function (result) {
                    if(result.status == 'success') {
                        console.log(result);
                        // data.empty();
                        // data.append('<span class="mt4 text-green">'+file.name+'</span>');
                        $('#setup_one_finish .cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                        $('.pagination-circle-right a').removeClass('disabled');
                    } else {
                        alert(result.message);
                    }
                },
                error: function () {
                    alert('Error! Allowed format: PNG, JPG, JPEG');
                }
            });

            return false;
        } else {
            alert('Please fill the fields');
        }

        return false;
    });

    $('body').on('click', '.checkbox-em', function() {
        let len = $('.border-green').length;

        if($(this).hasClass('border-green'))
            $(this).removeClass('border-green');
        else
        if(len < 2)
            $(this).addClass('border-green');

        return false;
    });

    $('#emotions').submit(function() {
       let ids = [];
       let token = $('#param-token').val();
       let url = $('#emotions').data('url');
       // console.log(token);
       // return false;
        if($('.border-green').length > 0) {
            $('.border-green').each(function (key, obj) {
                ids.push($(this).attr('data-id'));
            });
            // console.log(ids); return false;
            $.ajax({
                url: url,
                data: {'emojy':ids, '_csrf-backend':token},
                // dataType: 'script',
                // cache: false,
                // contentType: false,
                // processData: false,
                method: 'POST',
                success: function (result) {
                    // console.log('result');return false;
                    if(result.status == 'success') {
                        console.log(result);

                        $('#emotions .cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                        $('#emotions-finish').closest('.form-trade').find('.cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                        $('.pagination-circle-right a').removeClass('disabled');
                    } else {
                        alert(result.message);
                    }
                },
                error: function () {
                    alert('Error! Allowed format: PNG, JPG, JPEG');
                }
            });
        }
        return false;
    });

    $('.loop-href').click(function () {
        let title = $(this).attr('title');
        // console.error(title)
        if(typeof title !== 'undefined' && title != false) {
            $(this).addClass('disabled');
            alert(title);
            return false;
        }
        return true;
    });

    $('body').on('click', '.click-market-add', function ()  {
       let key = $(this).data('id');
       let url = $(this).data('url');
        let token = $('#param-token').val();
        $.ajax({
                url: url,
                data: {'key':key, '_csrf-backend':token},
                method: 'POST',
                success: function (result) {
                    if(result.status == 'success') {
                        if(key == 1 || $('.market-data').length == 1) {
                            $('.market-data').append('<button class="bg-danger click-market-rem"><i class="fas fa-minus"></i> </button>');
                            $('.click-market-add').addClass('click-market-remove')
                        }
                        $('.click-market-add').remove();

                        $('.market-list').append(result.data);
                    } else {
                        alert(result.message);
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
       return false;
    });

    $('body').on('click', '.click-market-rem', function () {
       $(this).closest('.market-data').remove();
       let len = $('.market-data').length;
       if(len == 1) {
            $('.click-market-rem').remove();
       }
       // console.log($('.market-data:last').data('lang'));
       let key = len + 20;
        let lang = $('.market-data:last').data('lang');
       let url = '/'+lang+'/cabinet/step-two/get-market';
       $('.click-market-add').remove();
       $('.market-data:last').append('<button class="bg-green click-market-add" data-id="'+key+'" data-url="'+url+'"><i class="fas fa-plus"></i> </button>');
       return false;
    });

    $('#market').change(function () {
       let name = $('#market option:selected').data('name');
       $('#short_name').val(name);
    });

    $('#step-three').submit(function () {
        let form = $('#step-three').serialize();
        let url = $('#step-three').data('url');
        $.ajax({
            url: url,
            data: form,
            method: 'POST',
            success: function (result) {
                if(result.status == 'success') {
                    window.location.replace(result.url);
                } else {
                    $('#step-three .cabinet-block-bg').css('border', '10px solid rgb(247 43 71 / 65%)');
                    alert(result.message);
                }
            },
            error: function () {
                alert('Error');
            }
        });
        return false;
    });

    $('.screen-block').click(function () {
        let id = $(this).data('id');
        document.getElementById('photo'+id).click();
    });

    $("body").on('change', '.screen-block input', function () {
        let id = $(this).closest('.screen-block').data('id');
        let data = $(this).closest('.screen-block').find('.filename'); //.append('<span class="mt4 text-green">file.name</span>');

        let files = this.files;
        if(files && files.length > 0) {
            let file = files[0];
            data.append('<span class="text-green">' + file.name + '</span>');
        }

        return false;
    });

    $('#step-three-finish').submit(function () {
        let url = $('#step-three-finish').data('url');
        let id = $(this).closest('.screen-block').data('id');
        let data = $(this).closest('.screen-block').find('.filename'); //.append('<span class="mt4 text-green">file.name</span>');
        let number = $('#id').val(); //.append('<span class="mt4 text-green">file.name</span>');

        let ids = [];
        if($('.border-green').length > 0) {
            $('.border-green').each(function (key, obj) {
                ids.push($(this).attr('data-id'));
            });
        }
        if(ids.length == 0) {
            alert('Please fill all fields');
            return false;
        }

        let files = [];
        $('.screen-block input').each(function (key, item) {
            if(this.files.length > 0) {
                files.push(this.files);
            }
        });


        let form_data = new FormData();
        form_data.append('emojy', ids);
        form_data.append('id', number);

        if(files.length > 0) {
            form_data.append('count', files.length);

            let valid = '';
            for (let i = 0; i < files.length; i++) {
                let file = files[i][0];

                if (file.size > 1024 * 10485760) {
                    valid = "Error! Image size must be < 10Mb";
                    return false;
                }
                if(file.type.match('png') || file.type.match('jpg') || file.type.match('jpeg')) {
                    form_data.append("file[]", file); // Appending parameter named file with properties of file_field to form_data
                } else {
                    valid = 'Error! Allowed format: PNG, JPG, JPEG';
                }
            }

            if(valid != '') {
                alert(valid);
                return false;
            }

            $.ajax({
                url: url,
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function (result) {
                    if(result.status == 'success') {
                        window.location.replace(result.url);
                    } else {
                        alert('Error');
                    }
                },
                error: function () {
                    alert('Error! Allowed format: PNG, JPG, JPEG');
                }
            });

            return false;
        } else {
            alert('Please fill all fields');
        }
        return false;
    });

    var slideIndex = 0;
    showSlides();

    function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        if(slides.length > 0) {
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slideIndex++;
            if (slideIndex > slides.length) {
                slideIndex = 1
            }
            slides[slideIndex - 1].style.display = "block";
            setTimeout(showSlides, 1500); // Change image every 2 seconds
        }
    }

    $('body').on('click', '.trade-block', function () {
        let form = $(this).closest('#trade-setup');
        let id = form.find('#trade_number').val();

        document.getElementById('screenshot_'+id).click();
    });

    $('body').on('click', '.new-trade', function () {
        let url = $(this).data('url');
        $.ajax({
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            method: 'POST',
            success: function (result) {
                if(result.status == 'success') {
                    $('.trade-body').append(result.data);
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $(".text-success").offset().top
                    }, 1500);
                } else {
                    alert(result.error);
                }
            },
            error: function () {
                alert('Error!');
            }
        });

        return false;
    });

    $("body").on('change', '.long_amount', function () {
        // console.log('dfs');
        let form = $(this).closest('#trade-setup');

        let long_amount = form.find('#long_amount').is(':checked');
        form.find('#long_amount').val(2);
        if(long_amount) {
            $("#short_amount").prop("checked", false);
            $("#short_amount").val(0);
        }
        return false;
    });

    $("body").on('change', '.short_amount', function () {
        // console.log('dfs');
        let form = $(this).closest('#trade-setup');

        let long_amount = form.find('#short_amount').is(':checked');
        form.find('#short_amount').val(2);
        if(long_amount) {
            $("#long_amount").prop("checked", false);
            $("#long_amount").val(0);
        }
        return false;
    });

    $("body").on('click', '.btn-save-trade', function () {
        let form = $(this).closest('#trade-setup');
        let block = $(this).closest('#form-trade');
        // block.append('sdfsdfsdf');
        // console.log(block);return false;
        let url = $(this).data('url');
        $('input').removeClass('bd-danger');
        $('select').removeClass('bd-danger');
        let validation = 2;
        let ticker = form.find('#market').val();
        let setup = form.find('#setup').val();
        // let long_amount = form.find('#long_amount').is(':checked');
        // let short_amount = form.find('#short_amount').is(':checked');
        let message = 'Please fill the fields';

        if(ticker.length == 0) {
            form.find('#market').addClass('bd-danger');
            validation = 1;
        }
        if(setup.length == 0) {
            form.find('#setup').addClass('bd-danger');
            validation = 1;
        }
        // if(long_amount.length == 0) {
        //     form.find('#long_amount').addClass('bd-danger');
        //     validation = 1;
        // }
        // if(short_amount.length == 0) {
        //     form.find('#short_amount').addClass('bd-danger');
        //     validation = 1;
        // }

        let files = [];
        form.find('.files-input').each(function (key, item) {
            if(this.files.length > 0) {
                files.push(this.files);
            }
        });

        // let form_data = new FormData();
        if(files.length > 0) {
            for (let i = 0; i < files[0].length; i++) {
                let file = files[0][i];

                if (file.size > 1024 * 10485760) {
                    message = "Error! Image size must be < 10Mb";
                    form.find('.trade-block').addClass('bd-danger');
                    validation = 1;
                }
                if (file.type.match('png') || file.type.match('jpg') || file.type.match('jpeg')) {
                    // form_data.append("file[]", file); // Appending parameter named file with properties of file_field to form_data
                } else {
                    message = 'Error! Allowed format: PNG, JPG, JPEG';
                    form.find('.trade-block').addClass('bd-danger');
                    validation = 1;
                }
            }
        } else {
            form.find('.trade-block').addClass('bd-danger');
            validation = 1;
        }

        if(validation == 1) {
            alert(message);
            return false;
        } else {

            // form_data.append('form[ticker_name]', ticker);
            // form_data.append('form[setup]', setup);
            // form_data.append('form[long_amount]', long_amount);
            // form_data.append('form[short_amount]', short_amount);
            // form_data.append('form[trade_number]', form.find('#trade_number').val());
            // form_data.append('form[date]', form.find('#datum').val());
            // form_data.append('form[lot_size]', form.find('#lot_size').val());
            // form_data.append('form[tick]', form.find('#tick').val());
            //
            // let lot = form.find('.lots').map(function(idx, elem) {
            //     return $(elem).val();
            // }).get();
            //
            // let lotSum = form.find('.lots-sum').map(function(idx, elem) {
            //     return $(elem).val();
            // }).get();
            //
            // form_data.append('form[lot]', lot);
            // form_data.append('form[lot_sum]', lotSum);
// console.log(form_data);return false;
            $.ajax({
                url: url,
                // data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function (result) {
                    if(result.status == 'success') {
                        // form.css('border', '10px solid rgb(247 43 71 / 65%)');
                        // form.find('.trade-form').empty();
                        $('.btn-save-trade').remove();
                        form.find('.trade-form').append(result.data);
                    } else {
                        alert('Error');
                    }
                },
                error: function () {
                    alert('Error! Allowed format: PNG, JPG, JPEG');
                }
            });
        }


        // console.log(files.length);
        return false;
    });

    $("body").on('click', '.btn-loss', function () {
        let form = $(this).closest('#trade-setup');
        // console.log('sfsdf');return false;
        // block.append('sdfsdfsdf');
        // console.log(block);return false;
        let url = $(this).data('url');
        $('input').removeClass('bd-danger');
        $('select').removeClass('bd-danger');
        let validation = 2;
        let ticker = form.find('#market').val();
        let setup = form.find('#setup').val();
        let long_amount = form.find('#long_amount').val();
        let short_amount = form.find('#short_amount').val();
        let message = 'Please fill the fields';

        if(ticker.length == 0) {
            form.find('#market').addClass('bd-danger');
            validation = 1;
        }
        if(setup.length == 0) {
            form.find('#setup').addClass('bd-danger');
            validation = 1;
        }

        let files = [];
        form.find('.files-input').each(function (key, item) {
            if(this.files.length > 0) {
                files.push(this.files);
            }
        });

        let form_data = new FormData();
        if(files.length > 0) {
            for (let i = 0; i < files[0].length; i++) {
                let file = files[0][i];

                if (file.size > 1024 * 10485760) {
                    message = "Error! Image size must be < 10Mb";
                    form.find('.trade-block').addClass('bd-danger');
                    validation = 1;
                }
                if (file.type.match('png') || file.type.match('jpg') || file.type.match('jpeg')) {
                    form_data.append("file[]", file); // Appending parameter named file with properties of file_field to form_data
                } else {
                    message = 'Error! Allowed format: PNG, JPG, JPEG';
                    form.find('.trade-block').addClass('bd-danger');
                    validation = 1;
                }
            }
        } else {
            form.find('.trade-block').addClass('bd-danger');
            validation = 1;
        }

        if(validation == 1) {
            alert(message);
            return false;
        } else {

            form_data.append('form[ticker_name]', ticker);
            form_data.append('form[setup]', setup);
            form_data.append('form[long_amount]', long_amount);
            form_data.append('form[short_amount]', short_amount);
            form_data.append('form[trade_number]', form.find('#trade_number').val());
            form_data.append('form[date]', form.find('#datum').val());
            form_data.append('form[lot_size]', form.find('#lot_size').val());
            form_data.append('form[tick]', form.find('#tick').val());
            // form_data.append('form[tick]', form.find('#tick').val());

            let lot = form.find('.lots').map(function(idx, elem) {
                return $(elem).val();
            }).get();

            // let ids = [];
            // if($('.border-green').length > 0) {
            //     $('.border-green').each(function (key, obj) {
            //         ids.push($(this).attr('data-id'));
            //     });
            // }
// console.log(ids);return false;
//             form_data.append('form[lot]', lot);
//             form_data.append('form[emojy]', ids);

            $.ajax({
                url: url,
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function (result) {
                    if(result.status == 'success') {
                        form.find('.cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                        // form.find('.trade-form').empty();
                        // block.append(result.data);
                    } else {
                        alert('Error');
                    }
                },
                error: function () {
                    alert('Error! Allowed format: PNG, JPG, JPEG');
                }
            });
        }


        // console.log(files.length);
        return false;
    });

    $("body").on('click', '.finish-trade', function () {
        let form = $(this).closest('#trade-setup');
        // console.log('sfsdf');return false;
        // block.append('sdfsdfsdf');
        // console.log(block);return false;
        let url = form.data('url');
        $('input').removeClass('bd-danger');
        $('select').removeClass('bd-danger');
        let validation = 2;
        let ticker = form.find('#market').val();
        let setup = form.find('#setup').val();
        let long_amount = form.find('#long_amount').val();
        let short_amount = form.find('#short_amount').val();
        let message = 'Please fill the fields';

        if(ticker.length == 0) {
            form.find('#market').addClass('bd-danger');
            validation = 1;
        }
        if(setup.length == 0) {
            form.find('#setup').addClass('bd-danger');
            validation = 1;
        }
        // if(long_amount.length == 0) {
        //     form.find('#long_amount').addClass('bd-danger');
        //     validation = 1;
        // }
        // if(short_amount.length == 0) {
        //     form.find('#short_amount').addClass('bd-danger');
        //     validation = 1;
        // }

        let files = [];
        form.find('.files-input').each(function (key, item) {
            if(this.files.length > 0) {
                files.push(this.files);
            }
        });

        let form_data = new FormData();
        if(files.length > 0) {
            for (let i = 0; i < files[0].length; i++) {
                let file = files[0][i];

                if (file.size > 1024 * 10485760) {
                    message = "Error! Image size must be < 10Mb";
                    form.find('.trade-block').addClass('bd-danger');
                    validation = 1;
                }
                if (file.type.match('png') || file.type.match('jpg') || file.type.match('jpeg')) {
                    form_data.append("file[]", file); // Appending parameter named file with properties of file_field to form_data
                } else {
                    message = 'Error! Allowed format: PNG, JPG, JPEG';
                    form.find('.trade-block').addClass('bd-danger');
                    validation = 1;
                }
            }
        } else {
            form.find('.trade-block').addClass('bd-danger');
            validation = 1;
        }

        if(validation == 1) {
            alert(message);
            return false;
        } else {

            form_data.append('form[ticker_name]', ticker);
            form_data.append('form[setup]', setup);
            form_data.append('form[long_amount]', long_amount);
            form_data.append('form[short_amount]', short_amount);
            form_data.append('form[trade_number]', form.find('#trade_number').val());
            form_data.append('form[date]', form.find('#datum').val());
            form_data.append('form[lot_size]', form.find('#lot_size').val());
            form_data.append('form[tick]', form.find('#tick').val());
            form_data.append('form[tick]', form.find('#tick').val());

            let lot = form.find('.lots').map(function(idx, elem) {
                return $(elem).val();
            }).get();

            let ids = [];
            if($('.border-green').length > 0) {
                $('.border-green').each(function (key, obj) {
                    ids.push($(this).attr('data-id'));
                });
            }
// console.log(ids);return false;
            form_data.append('form[lot]', lot);
            form_data.append('form[emojy]', ids);

            $.ajax({
                url: url,
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function (result) {
                    if(result.status == 'success') {
                        form.find('.cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                        // form.find('.trade-form').empty();
                        // block.append(result.data);
                    } else {
                        alert('Error');
                    }
                },
                error: function () {
                    alert('Error! Allowed format: PNG, JPG, JPEG');
                }
            });
        }


        // console.log(files.length);
        return false;
    });

    $('body').on('submit', '#emotions-finish', function() {
        let ids = [];
        let token = $('#param-token').val();
        let url = $('#emotions-finish').data('url');
        console.log('url');
        return false;
        // console.log(token);
        // return false;
        if($('.border-green').length > 0) {
            $('.border-green').each(function (key, obj) {
                ids.push($(this).attr('data-id'));
            });
            // console.log(ids); return false;
            $.ajax({
                url: url,
                data: {'emojy':ids, '_csrf-backend':token},
                // dataType: 'script',
                // cache: false,
                // contentType: false,
                // processData: false,
                method: 'POST',
                success: function (result) {
                    // console.log('result');return false;
                    if(result.status == 'success') {
                        // console.log(result);

                        // $('#emotions .cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                        $('#emotions-finish').closest('.form-trade').find('.cabinet-block-bg').css('border', '10px solid rgba(48, 177, 32, 0.65)');
                        // $('.pagination-circle-right a').removeClass('disabled');
                    } else {
                        alert(result.message);
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
        }
        return false;
    });

    $('body').on('change', '.lots', function () {
        let price = $('#market option:selected').data('price');
        let count = $(this).val();
        let result = parseFloat(price * count).toFixed(2);
        $(this).next('input').val(result);
        return false;
    });
});