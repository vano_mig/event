<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);


$config =  [
    'id' => 'event-backend',
    'name'=>'TJ3',
    'language' => 'en',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'cabinet' => [
            'class' => 'backend\modules\cabinet\Modue',
            'layout' => '@backend/modules/cabinet/views/layouts/main.php',
        ],
    ],
    'aliases' => [
        '@admin_lte' => '@backend/theme/AdminLTE',
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'cookieValidationKey' => 'asdfgbkdjgbjgfdkbsdgbfvfzkbdvsdg',
            'class' => 'backend\components\LanguageRequest'
        ],
//        'assetManager' => [
//            'linkAssets' => true,
//            'bundles' => [
//                'yii\bootstrap4\BootstrapPluginAsset' => false,
//                'yii\bootstrap4\BootstrapAsset' => false,
//            ]
//        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
//            'traceLevel' => YII_DEBUG ? 3 : 0,
//            'targets' => [
//                [
//                    'class' => 'yii\log\FileTarget',
//                    'levels' => ['error', 'warning'],
//                ],
//            ],
            'traceLevel' => 3,
            'targets' => [
                'telegram' => [
                    'class' => 'backend\components\TargetHelper',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:401', // yii\web\UnauthorizedHttpException: Your request was made with invalid credentials
                        'yii\web\HttpException:400', //yii\web\BadRequestHttpException: Не удалось проверить переданные данные.
                    ],
                    'token' => '5096388884:AAFuyh4IPQ3dNrLYpErTEVc2bC-3BgyIE1M',
                    'chatId' => 372747249,
                    'logVars' => [],
                    'template' => "{levelAndRequest}\n{text}\n{category}\n{user}\n{stackTrace}\n\nENV: ".YII_ENV,
                ]
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
//                'yii' => [
//                    'class' => yii\i18n\DbMessageSource::class,
//                    'forceTranslation' => true,
//                    'on missingTranslation' => ['backend\components\TranslationEventHandler', 'handleMissingTranslation'],
//                ],
                '*' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                    'on missingTranslation' => ['backend\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'backend\components\LanguageUrlManager',
            'rules' => [

            ],
        ],
    ],
    'params' => $params,
];

if (!YII_ENV_PROD) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

