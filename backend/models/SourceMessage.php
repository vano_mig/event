<?php

namespace backend\models;

use backend\modules\cabinet\models\LanguageModel;
use yii\db\ActiveRecord;

class SourceMessage extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source_message';
    }

    public static function getMessagesList($lang)
    {
        $model = new self;
        return $model->hasMany(Message::class, ['id' => 'id'])->where(['language'=> $lang]);
    }

    public function getMessages()
    {
        $lang = LanguageModel::getActiveCodeLanguages();
        return $this->hasMany(Message::class, ['id' => 'id'])->where(['language'=> $lang]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'category'], 'required'],
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
            [['message', 'category'], 'unique', 'targetAttribute' => ['message', 'category']],
            ['isLanguage', 'safe']

        ];
    }
}