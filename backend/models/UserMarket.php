<?php

namespace backend\models;

use yii\db\ActiveRecord;

class UserMarket extends ActiveRecord
{
    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETED = 0;

    public static function updateModels($params, $condition)
    {
        self::updateAll($params, $condition);
    }

}
