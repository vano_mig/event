<?php

namespace backend\models;

use yii\db\ActiveRecord;

class Statistic extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statistics';
    }
}