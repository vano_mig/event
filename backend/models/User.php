<?php

namespace backend\models;

use yii\rbac\DbManager;

class User extends \common\models\User
{
    /**
     * Add user role to rbac
     *
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     * @throws mixed
     */
    public function afterSave($insert, $changedAttributes): bool
    {
        parent::afterSave($insert, $changedAttributes);
        $rbac = new DbManager;
        if ($insert) {
            if ($rbac->assign($rbac->getRole($this->role), $this->id)) {
                return true;
            }
        } else {
            if (isset($changedAttributes['role']) &&
                ($rbac->revokeAll($this->id) &&
                    $rbac->assign($rbac->getRole($this->role), $this->id))) {
                return true;
            }
        }
        return false;
    }

    /**
     * delete user role
     * @params mixed
     * @return bool
     */
    public function beforeDelete(): bool
    {
        parent::beforeDelete();
        $this->trigger(self::EVENT_BEFORE_DELETE);

        $rbac = new DbManager;
        if ($rbac->revokeAll($this->id)) {
            return true;
        }
        return false;
    }
}