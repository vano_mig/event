<?php

namespace backend\models\classes;

use backend\models\UserLimit;
use yii\base\Model;

class UserLimitModel extends Model
{
    public $userId;
    public $amount;
    public $amountWeekly;
    public $limit;
    public $limitAmount;

    public static function getLimitByUserId($id)
    {
        $limit = new self;
        $model = UserLimit::findOne(['user_id'=>$id]);
        if($model === null) {
            $model = new UserLimit();
        }
        $limit->userId = $id;
        $limit->amount = $model->amount;
        $limit->amountWeekly = $model->amount_weekly;
        $limit->limit = $model->limit;
        $limit->limitAmount = $model->limit_amount;

        return $limit;
    }

    public function saveLimitByUserId($obj)
    {
        $model = UserLimit::findOne(['user_id'=>$obj->userId]);
        if($model === null)
            $model = new UserLimit();
        $model->user_id = $obj->userId;
        $model->amount = $obj->amount;
        $model->amount_weekly = $obj->amountWeekly;
        $model->limit = $obj->limit;
        $model->limit_amount = $obj->limitAmount;

        if($model->save())
            return true;

        return false;
    }
}