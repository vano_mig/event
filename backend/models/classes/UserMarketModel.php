<?php

namespace backend\models\classes;

use backend\models\UserCondition;
use backend\models\UserMarket;
use Yii;
use yii\base\Model;

class UserMarketModel extends Model
{
    public $id;
    public $userId;
    public $name;
    public $shortName;
    public $tickgrobe;
    public $tickwert;
    public $status;


    public static function getMarketsByUserId($id):array
    {
        $result = [];
        $model = UserMarket::findAll(['user_id'=>$id]);
        $condition = new self;

        if(!empty($model)) {
            foreach ($model as $item) {
                $condition = new self;
                $condition->id = $item->id;
                $condition->userId = $item->user_id;
                $condition->name = $item->name;
                $condition->shortName = $item->short_name;
                $condition->tickgrobe = $item->tickgrobe;
                $condition->tickwert = $item->tickwert;
                $condition->status = $item->status;
                $result[] = $condition;
            }
        }

        return $result;
    }

    public static function getActiveMarketsByUserId($id):array
    {
        $result = [];
        $model = UserMarket::findAll(['user_id'=>$id, 'status'=>UserMarket::STATUS_ACTIVE]);
        $condition = new self;

        if(!empty($model)) {
            foreach ($model as $item) {
                $condition = new self;
                $condition->id = $item->id;
                $condition->userId = $item->user_id;
                $condition->name = $item->name;
                $condition->shortName = $item->short_name;
                $condition->tickgrobe = $item->tickgrobe;
                $condition->tickwert = $item->tickwert;
                $condition->status = $item->status;
                $result[] = $condition;
            }
        }

        return $result;
    }

    public static function getMarketByIdAndUserId($id, $userId)
    {
        $model = UserMarket::findOne(['id'=>$id, 'user_id'=>$userId]);
        if($model === null)
            $model = new UserMarket();

        $market = new self;
        $market->id = $model->id;
        $market->userId = $model->user_id;
        $market->name = $model->name;
        $market->shortName = $model->short_name;
        $market->tickgrobe = $model->tickgrobe;
        $market->tickwert = $model->tickwert;
        $market->status = $model->status;

        return $market;
    }
    public static function getMarketByNameAndUser($name, $userId)
    {
        $model = UserMarket::findOne(['short_name'=>$name, 'user_id'=>$userId]);
        if($model === null)
            $model = new UserMarket();

        $market = new self;
        $market->id = $model->id;
        $market->userId = $model->user_id;
        $market->name = $model->name;
        $market->shortName = $model->short_name;
        $market->tickgrobe = $model->tickgrobe;
        $market->tickwert = $model->tickwert;
        $market->status = $model->status;

        return $market;
    }


    public static function saveModels($list)
    {
        $ids = [];
        foreach ($list as $obj) {
            $model = UserMarket::findOne(['id'=>$obj->id, 'user_id'=>$obj->userId]);

            if($model === null) {
                $model = new UserMarket();
                $model->user_id = $obj->userId;
            }

            $model->name = $obj->name;
            $model->short_name = $obj->shortName;
            $model->tickgrobe = $obj->tickgrobe;
            $model->tickwert = $obj->tickwert;
            $model->status = UserMarket::STATUS_ACTIVE;
            if(!$model->save()) {
//                echo "<pre>";print_r($model);die;
                return false;
            }
            $ids[] = $model->id;
        }
//echo "<pre>";print_r($ids);die;
        UserMarket::updateModels(['status'=>UserMarket::STATUS_DELETED], ['not in', 'id', $ids]);

        return true;
    }

    public static function validateModel($model)
    {
        if(!$model->user_id || empty($model->rituale) || empty($model->prepare) ||  empty($model->documentation) ||
            empty($model->setup_one_data) || empty($model->setup_one_manage) || empty($model->setup_one_criterium) || empty($model->setup_one_criterium_name) ||
            empty($model->setup_one_criterium_abbr) || empty($model->markets) || empty($model->time_work) || empty($model->time_pause) ||
            empty($model->max_trade) || empty($model->max_decrease_trade) || empty($model->max_decrease_point_trade) || empty($model->limit_desc) ||
            empty($model->limit_to_do) || empty($model->limit_daily) || empty($model->limit_daily_step) || empty($model->limit_weekly) ||
            empty($model->contract_ort) || empty($model->contract_date) || empty($model->contract_signature))

            return false;

        return true;
    }

}