<?php

namespace backend\models\classes;

use backend\models\UserCondition;
use Yii;
use yii\base\Model;

class UserConditionModel extends Model
{
    public $id;
    public $userId;
    public $rituale;
    public $prepare;
    public $documentation;
    public $setupOneData;
    public $setupOneManage;
    public $setupOneCriterium;
    public $setupOneCriteriumName;
    public $setupOneCriteriumAbbr;

    public $setupTwoData;
    public $setupTwoManage;
    public $setupTwoCriterium;
    public $setupTwoCriteriumName;
    public $setupTwoCriteriumAbbr;

    public $setupThreeData;
    public $setupThreeManage;
    public $setupThreeCriterium;
    public $setupThreeCriteriumName;
    public $setupThreeCriteriumAbbr;

    public $markets;
    public $timeWork;
    public $timePause;
    public $maxTrade;
    public $maxDecreaseTrade;
    public $maxDecreasePointTrade;
    public $limitDesc;
    public $limitToDo;
    public $limitDaily;
    public $limitDailyStep;
    public $limitDailyMax;
    public $limitWeekly;
    public $photo_1;
    public $photo_2;
    public $photo_3;
    public $photo_4;
    public $tasks;
    public $emotions;
    public $daylyAmount;
    public $contractOrt;
    public $contractDate;
    public $contractSignature;
    public $stepStatus;

    public static function getConditionByUserId($id)
    {
        $model = UserCondition::findOne(['user_id'=>$id]);
        $condition = new self;

        if($model === null) {
            $model = new UserCondition();
            $condition->userId = Yii::$app->user->id;
        }

        $condition = new self;
        $condition->id = $model->id;
        $condition->userId = $model->user_id;
        $condition->rituale = $model->rituale;
        $condition->prepare = $model->prepare;
        $condition->documentation = $model->documentation;

        $condition->setupOneData = $model->setup_one_data;
        $condition->setupOneManage = $model->setup_one_manage;
        $condition->setupOneCriterium = $model->setup_one_criterium;
        $condition->setupOneCriteriumName = $model->setup_one_criterium_name;
        $condition->setupOneCriteriumAbbr = $model->setup_one_criterium_abbr;

        $condition->setupTwoData = $model->setup_two_data;
        $condition->setupTwoManage = $model->setup_two_manage;
        $condition->setupTwoCriterium = $model->setup_two_criterium;
        $condition->setupTwoCriteriumName = $model->setup_two_criterium_name;
        $condition->setupTwoCriteriumAbbr = $model->setup_two_criterium_abbr;

        $condition->setupThreeData = $model->setup_three_data;
        $condition->setupThreeManage = $model->setup_three_manage;
        $condition->setupThreeCriterium = $model->setup_three_criterium;
        $condition->setupThreeCriteriumName = $model->setup_three_criterium_name;
        $condition->setupThreeCriteriumAbbr = $model->setup_three_criterium_abbr;

        $condition->markets = $model->markets;
        $condition->timeWork = $model->time_work;
        $condition->timePause = $model->time_pause;
        $condition->maxTrade = $model->max_trade;
        $condition->maxDecreaseTrade = $model->max_decrease_trade;
        $condition->maxDecreasePointTrade = $model->max_decrease_point_trade;
        $condition->limitDesc = $model->limit_desc;
        $condition->limitToDo = $model->limit_to_do;
        $condition->limitDaily = $model->limit_daily;
        $condition->limitDailyStep = $model->limit_daily_step;
        $condition->limitDailyMax = $model->limit_daily_max;
        $condition->limitWeekly = $model->limit_weekly;
        $condition->photo_1 = $model->photo_1;
        $condition->photo_2 = $model->photo_2;
        $condition->photo_3 = $model->photo_3;
        $condition->photo_4 = $model->photo_4;
        $condition->tasks = $model->tasks;
        $condition->daylyAmount = $model->dayly_amount;
        $condition->contractOrt = $model->contract_ort;
        $condition->contractDate = $model->contract_date;
        $condition->contractSignature = $model->contract_signature;
        $condition->stepStatus = $model->step_status;
        $condition->emotions = $model->emotions;

        return $condition;
    }

    public static function saveModel($obj, $valid = false)
    {
        $model = UserCondition::findOne(['user_id'=>$obj->userId]);

        if($model === null) {
            $model = new UserCondition();
            $model->user_id= Yii::$app->user->id;
        }

//        $model->user_id = $obj->userId;
        $model->rituale = $obj->rituale;
        $model->prepare = $obj->prepare;
        $model->documentation = $obj->documentation;

        $model->setup_one_data = $obj->setupOneData;
        $model->setup_one_manage = $obj->setupOneManage;
        $model->setup_one_criterium = $obj->setupOneCriterium;
        $model->setup_one_criterium_name = $obj->setupOneCriteriumName;
        $model->setup_one_criterium_abbr = $obj->setupOneCriteriumAbbr;

        $model->setup_two_data = $obj->setupTwoData;
        $model->setup_two_manage = $obj->setupTwoManage;
        $model->setup_two_criterium = $obj->setupTwoCriterium;
        $model->setup_two_criterium_name = $obj->setupTwoCriteriumName;
        $model->setup_two_criterium_abbr = $obj->setupTwoCriteriumAbbr;

        $model->setup_three_data = $obj->setupThreeData;
        $model->setup_three_manage = $obj->setupThreeManage;
        $model->setup_three_criterium = $obj->setupThreeCriterium;
        $model->setup_three_criterium_name = $obj->setupThreeCriteriumName;
        $model->setup_three_criterium_abbr = $obj->setupThreeCriteriumAbbr;

        $model->markets = $obj->markets;
        $model->time_work = $obj->timeWork;
        $model->time_pause = $obj->timePause;

        $model->max_trade = $obj->maxTrade;
        $model->max_decrease_trade = $obj->maxDecreaseTrade;
        $model->max_decrease_point_trade = $obj->maxDecreasePointTrade;
        $model->limit_desc = $obj->limitDesc;
        $model->limit_to_do = $obj->limitToDo;
        $model->limit_daily = $obj->limitDaily;
        $model->limit_daily_step = $obj->limitDailyStep;
        $model->limit_daily_max = $obj->limitDailyMax;
        $model->limit_weekly = $obj->limitWeekly;
        $model->photo_1 = $obj->photo_1;
        $model->photo_2 = $obj->photo_2;
        $model->photo_3 = $obj->photo_3;
        $model->photo_4 = $obj->photo_4;
        $model->tasks = $obj->tasks;
        $model->dayly_amount = $obj->daylyAmount;
        $model->contract_ort = $obj->contractOrt;
        $model->contract_date = $obj->contractDate;
        $model->contract_signature = $obj->contractSignature;
        $model->emotions = $obj->emotions;
        $model->step_status = $obj->stepStatus;
        if($model->save()) {
            if($valid && self::validateModel($model) == true && $model->step_status < 2) {
                $model->step_status = 1;
                $model->save();
            }
            return true;
        }

        return false;
    }

    public static function validateModel($model)
    {
        if(!$model->user_id || empty($model->rituale) || empty($model->prepare) ||  empty($model->documentation) ||
            empty($model->setup_one_data) || empty($model->setup_one_manage) || empty($model->setup_one_criterium) || empty($model->setup_one_criterium_name) ||
            empty($model->setup_one_criterium_abbr) || empty($model->markets) || empty($model->time_work) || empty($model->time_pause) ||
            empty($model->max_trade) || empty($model->max_decrease_trade) || empty($model->max_decrease_point_trade) || empty($model->limit_desc) ||
            empty($model->limit_to_do) || empty($model->limit_daily) || empty($model->limit_daily_step) || empty($model->limit_weekly) ||
            empty($model->contract_ort) || empty($model->contract_date) || empty($model->contract_signature))

            return false;

        return true;
    }

    public static function getEmotions():array
    {
        return [
            ['0'=>'Zufrieden', '1'=>'Ruhig / Entspannt', '2'=>'Glücklich'],
            ['3'=>'Euphorisch', '4'=>'Hoffnungsvoll', '5'=>'Motiviert'],
            ['6'=>'Nervös', '7'=>'Ängstlich', '8'=>'Vorsichtig'],
            ['9'=>'Ungeduldig', '10'=>'Hastig', '11'=>'Angespannt'],
            ['12'=>'Gierig', '13'=>'Aggressiv'],
            ['14'=>'Deprimiert', '15'=>'Unglücklich', '16'=>'Verzweifelt'],
        ];
    }

    public static function getSetupListByUserId($id)
    {
        $result = [];
        $model = UserCondition::findOne(['user_id'=>$id]);
        if($model === null)
            return $result;

        $result = [$model->setup_one_criterium_abbr, $model->setup_two_criterium_abbr, $model->setup_three_criterium_abbr];
        return $result;

    }

    public static function getSetupByAbbr($abbr, $id):string
    {
        $result = '';
        $model = self::getConditionByUserId($id);

        if ($model->setupOneCriteriumAbbr == $abbr)
            $result = 'one';
        elseif($model->setupTwoCriteriumAbbr == $abbr)
            $result = 'two';
        elseif($model->setupThreeCriteriumAbbr == $abbr)
            $result = 'three';
        return $result;
    }
}