<?php

namespace backend\models\classes;

use backend\models\UserTrade;
use Yii;
use yii\base\Model;

class UserTradeModel extends Model
{
    public const LOSS_TRUE = 2;
    public const LOSS_FALSE = 1;

    public $id;
    public $userId;
    public $tradeNumber;
    public $coachEmail;
    public $tickerName;
    public $tickerShortName;
    public $lot;
    public $ticksSl;
    public $setup;
    public $setupAbbr;
    public $emojy;
    public $files;
    public $startTrade;
    public $finishTrade;
    public $status;
    public $createdAt;
    public $tp;
    public $longAmount;
    public $shortAmount;
    public $loss;
    public $total;


    public static function getTradesByUserId($id):array
    {
        $result = [];
        $model = UserTrade::findAll(['user_id'=>$id]);

        if(!empty($model)) {
            foreach ($model as $item) {
                $trade = new self;
                $trade->id = $item->id;
                $trade->userId = $item->user_id;
                $trade->tradeNumber = $item->trade_number;
                $trade->coachEmail = $item->coach_email;
                $trade->tickerName = $item->ticker_name;
                $trade->tickerShortName = $item->ticker_short_name;
                $trade->lot = $item->lot;
                $trade->ticksSl = $item->ticks_sl;
                $trade->setup = $item->setup;
                $trade->setupAbbr = $item->setup_abbr;
                $trade->longAmount = $item->long_amount;
                $trade->shortAmount = $item->short_amount;
                $trade->tp = $item->tp;
                $trade->emojy = $item->emojy;
                $trade->files = $item->files;
                $trade->startTrade = $item->start_trade;
                $trade->finishTrade = $item->finish_trade;
                $trade->status = $item->status;
                $trade->createdAt = $item->created_at;
                $trade->loss = $item->loss;
                $trade->total = $item->total;
                $result[] = $trade;
            }
        }

        return $result;
    }

    public static function getActiveTradesByUserId($id):array
    {
        $result = [];
        $model = UserTrade::find()->where(['user_id'=>$id])->andWhere(['<', 'status', UserTrade::STATUS_FINISHED])->all();

        if(!empty($model)) {
            foreach ($model as $item) {
                $trade = new self;
                $trade->id = $item->id;
                $trade->userId = $item->user_id;
                $trade->tradeNumber = $item->trade_number;
                $trade->coachEmail = $item->coach_email;
                $trade->tickerName = $item->ticker_name;
                $trade->tickerShortName = $item->ticker_short_name;
                $trade->lot = $item->lot;
                $trade->ticksSl = $item->ticks_sl;
                $trade->setup = $item->setup;
                $trade->setupAbbr = $item->setup_abbr;
                $trade->longAmount = $item->long_amount;
                $trade->shortAmount = $item->short_amount;
                $trade->tp = $item->tp;
                $trade->emojy = $item->emojy;
                $trade->files = $item->files;
                $trade->startTrade = $item->start_trade;
                $trade->finishTrade = $item->finish_trade;
                $trade->status = $item->status;
                $trade->createdAt = $item->created_at;
                $trade->loss = $item->loss;
                $trade->total = $item->total;
                $result[] = $trade;
            }
        }

        return $result;
    }

    public static function getTradeByIdAndUserId($id, $userId)
    {
        $model = UserTrade::findOne(['id'=>$id, 'user_id'=>$userId]);
        if($model === null)
            $model = new UserTrade();

        $trade = new self;
        $trade->id = $model->id;
        $trade->userId = $model->user_id;
        $trade->tradeNumber = $model->trade_number;
        $trade->coachEmail = $model->coach_email;
        $trade->tickerName = $model->ticker_name;
        $trade->tickerShortName = $model->ticker_short_name;
        $trade->lot = $model->lot;
        $trade->ticksSl = $model->ticks_sl;
        $trade->setup = $model->setup;
        $trade->setupAbbr = $model->setup_abbr;
        $trade->long_amount = $model->longAmount;
        $trade->short_amount = $model->shortAmount;
        $trade->tp = $model->tp;
        $trade->emojy = $model->emojy;
        $trade->files = $model->files;
        $trade->startTrade = $model->start_trade;
        $trade->finishTrade = $model->finish_trade;
        $trade->status = $model->status;
        $trade->createdAt = $model->created_at;
        $trade->loss = $model->loss;
        $trade->total = $model->total;

        return $trade;
    }

    public static function getTradeByNumberAndUserId($tradeNumber, $userId)
    {
        $model = UserTrade::findOne(['trade_number'=>$tradeNumber, 'user_id'=>$userId]);
        if($model === null)
            return false;

        $trade = new self;
        $trade->id = $model->id;
        $trade->userId = $model->user_id;
        $trade->tradeNumber = $model->trade_number;
        $trade->coachEmail = $model->coach_email;
        $trade->tickerName = $model->ticker_name;
        $trade->tickerShortName = $model->ticker_short_name;
        $trade->lot = $model->lot;
        $trade->ticksSl = $model->ticks_sl;
        $trade->setup = $model->setup;
        $trade->setupAbbr = $model->setup_abbr;
        $trade->longAmount = $model->long_amount;
        $trade->shortAmount = $model->short_amount;
        $trade->tp = $model->tp;
        $trade->emojy = $model->emojy;
        $trade->files = $model->files;
        $trade->startTrade = $model->start_trade;
        $trade->finishTrade = $model->finish_trade;
        $trade->status = $model->status;
        $trade->createdAt = $model->created_at;
        $trade->loss = $model->loss;
        $trade->total = $model->total;

        return $trade;

    }

    public static function saveModel($obj)
    {
            $model = UserTrade::findOne(['id'=>$obj->id, 'user_id'=>$obj->userId]);
            $setup = UserConditionModel::getSetupByAbbr($obj->setupAbbr, $obj->userId);

            if($model === null) {
                $model = new UserTrade();
                $model->user_id = Yii::$app->user->id;
                $model->status = UserTrade::STATUS_NEW;
                $model->created_at = date('Y-m-d H:i:s');
            } else {
                $model->status = $obj->status;
            }

            $model->trade_number = $obj->tradeNumber;
            $model->ticker_name = $obj->tickerName;
            $model->ticker_short_name = $obj->tickerShortName;
            $model->coach_email = Yii::$app->user->identity->coach_email;
            $model->lot = $obj->lot;
            $model->ticks_sl = $obj->ticksSl;
            $model->setup_abbr = $obj->setupAbbr;
            $model->setup = $setup;
            $model->long_amount = $obj->longAmount;
            $model->short_amount = $obj->shortAmount;
            $model->tp = $obj->tp;
            $model->start_trade = $obj->startTrade;
            $model->finish_trade = $obj->finishTrade;
            $model->emojy = $obj->emojy;
            $model->files = $obj->files;
            $model->loss = $obj->loss;
            $model->total = $obj->total;

        if(!$model->save()) {
//            echo "<pre>";print r($model);die;
                return false;
            }

        return true;
    }

    public static function setTradeNumber()
    {
        $number = uniqid('TR-', false);
        $check = UserTrade::find()->where(['trade_number'=>$number])->one();
        if($check)
            self::setTradeNumber();
        return strtoupper($number);
    }
}