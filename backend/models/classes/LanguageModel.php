<?php

namespace backend\models\classes;

use backend\models\Language;
use Yii;
use yii\base\Model;

class LanguageModel extends Model
{
    public $id;
    public $name;
    public $iso_code;
    public $system;
    public $status;


    public static function getActiveLanguages()
    {
        return Language::find()->where(['status'=>Language::STATUS_ACTIVE])->asArray()->all();
    }
}