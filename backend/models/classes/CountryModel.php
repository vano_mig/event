<?php

namespace backend\models\classes;

use backend\models\Country;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

class CountryModel extends Model
{
    public $id;
    public $name;
    public $iso_code;
    public $phone_code;

    public function attributes()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'name' => Yii::t('country', 'Name'),
            'iso_code' => Yii::t('country', 'Iso Code'),
            'phone_code' => Yii::t('country', 'Phone code'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'iso_code', 'phone_code'], 'required'],
            [['name'], 'string', 'max' => 191],
            [['iso_code'], 'string', 'max' => 3],
            [['phone_code'], 'string', 'max' => 20],
        ];
    }

    public function searchCountryList($params)
    {
        $query = Country::find();


        $provider = new ActiveDataProvider([
            'query' => $query,
//            'pagination' => [
//                'pageSize' => 20,
//            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'iso_code', $this->iso_code])
            ->andFilterWhere(['like', 'phone_code', $this->phone_code]);
        return $provider;
    }

    public static function getCountryById($id)
    {
        $country = Country::findOne($id);
        $model = null;
        if($country !== null) {
            $model = new self;
            $model->id = $country->id;
            $model->name = $country->name;
            $model->iso_code = $country->iso_code;
            $model->phone_code = $country->phone_code;
        }
        return $model;
    }

    public static function getCountryList():array
    {
        $list = [];
        $country = Country::find()->all();
        if(!empty($country))
        {
            foreach ($country as $item) {
                $model = new self;
                $model->id = $item->id;
                $model->name = $item->name;
                $model->iso_code = $item->iso_code;
                $model->phone_code = $item->phone_code;
                $list[] = $model;
            }
        }

        return $list;
    }

    public static function getCountryListByName():array
    {
        $list = [];
        $country = Country::find()->all();
        if(!empty($country))
        {
            foreach ($country as $item) {
                $list[$item->name] = $item->name;
            }
        }

        return $list;
    }

    public static function getCountryByName($name)
    {
        $country = Country::findOne(['name'=>$name]);
        $model = null;
        if($country !== null) {
            $model = new self;
            $model->id = $country->id;
            $model->name = $country->name;
            $model->iso_code = $country->iso_code;
            $model->phone_code = $country->phone_code;
        }
        return $model;
    }

    public static function deleteCountry($id)
    {
        if(Country::findOne($id)->delete())
            return true;
        return false;
    }

    public static function saveCountry($countryObj)
    {
        $model = null;
        if($countryObj->id != false) {
            $model = Country::findOne($countryObj->id);

        }

        if($model == null)
            $model = new Country();

        $model->name = $countryObj->name;
        $model->iso_code = $countryObj->iso_code;
        $model->phone_code = $countryObj->phone_code;
        if($model->save()) {
            return true;
        }
        return false;
    }

}