<?php

namespace backend\models\classes;

use backend\models\User;
use Yii;
use yii\base\Model;

class UserModel extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $status;
    public $role;
    public $coachEmail;
    public $created_at;
    public $logged;
    public $updated_at;

    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['coachEmail', 'trim'],
            ['coachEmail', 'required'],
            ['coachEmail', 'email'],
            ['email', 'unique', 'targetClass' => \common\models\User::class,
                'message' => Yii::t('users', 'This email address has already 
                been taken.'), 'on' => 'insert'],
            ['email', 'unique', 'filter' => ['!=', 'id', $this->id],
                'targetClass' => '\common\models\User',
                'message' => Yii::t('users', 'This email address has already 
                been taken.')],

            ['password', 'trim'],
            ['password', 'required',],
            ['password', 'string', 'min' => 6],

            ['status', 'default', 'value' => User::STATUS_ACTIVE],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE,
                User::STATUS_INACTIVE,  User::STATUS_DELETED]],

            ['role', 'default', 'value' => User::ROLE_USER],
            ['role', 'in', 'range' => [User::ROLE_USER, User::ROLE_MANAGER, User::ROLE_ADMIN]],
            ['logged', 'safe']
        ];
    }

    public function save()
    {
        $model = new User();
        $model->username = $this->username;
        $model->email = $this->email;
        $model->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        $model->auth_key =$model->password_hash;
        $model->status = $this->status;
        $model->role = $this->role;
        $model->coach_email = $this->coachEmail;
        if($model->save())
            return true;
        return false;
    }
}