<?php

namespace backend\models;

use yii\db\ActiveRecord;

class Message extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language','translation'], 'safe'],
        ];
    }
}