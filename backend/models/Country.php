<?php

namespace backend\models;

use yii\db\ActiveRecord;

/**
 * Class Language
 * @package frontend\models
 */
class Country extends ActiveRecord
{
    /**
     * Get list countries
     * @return $array
     */
    public static function getList():array
    {
        $list = self::find()->asArray()->all();
        return $list;
    }
}