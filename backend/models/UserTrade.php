<?php

namespace backend\models;

use yii\db\ActiveRecord;

class UserTrade extends ActiveRecord
{
    public const STATUS_NEW = 0;
    public const STATUS_ACTIVE = 1;
    public const STATUS_FINISHED = 2;

}
