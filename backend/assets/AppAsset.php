<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/main.js',
        'js/inputmask.min.js',
        'js/inputmask.binding.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'backend\assets\FontAsset',
        'backend\assets\PluginAdminLteAsset',
        'backend\assets\PluginBootstrapAsset',
        'backend\assets\PluginFontAwesomeAsset',
//        'backend\assets\PluginTinyMceAsset',
        'backend\assets\PluginChartJSAsset',
//        'backend\assets\PluginTypeaheadAsset',
        'backend\assets\PluginFlagIconAsset',
//        'backend\assets\PluginMatchHeightAsset',
//        'backend\assets\PluginSpectrumAsset'
    ];
}
