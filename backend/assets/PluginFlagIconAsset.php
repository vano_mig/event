<?php
namespace backend\assets;

use yii\web\AssetBundle;

class PluginFlagIconAsset extends AssetBundle
{
    public $sourcePath = '@admin_lte';
    public $baseUrl = '@web';

    public $css = [
        'plugins/flag-icon-css/css/flag-icon.css',
    ];

    public $js = [];

    public $depends = [];
}