<?php
namespace backend\assets;

use yii\web\AssetBundle;

class PluginAdminLteAsset extends AssetBundle
{
    public $sourcePath = '@bower/adminlte/dist';
    public $css = [
        'css/adminlte.css',
    ];
    public $js = [
        'js/adminlte.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}