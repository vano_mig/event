<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Request password reset';
?>
<div class="view-body d-flex flex-row justify-content-center auth-button">
    <div class="text-center reset-form"><h1><?= Html::encode($this->title) ?></h1>
        <p>Please fill out your email. A link to reset password will be sent there.</p>

        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email', [
                    'inputTemplate'=>'<div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-envelope"></i></span></div>{input}</div>'])->textInput(['autofocus' => true, 'placeholder'=>Yii::t('user', 'email')])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-grey btn-block']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
