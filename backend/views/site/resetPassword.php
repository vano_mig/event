<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \frontend\models\ResetPasswordForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Reset password';
?>
<div class="view-body d-flex flex-row justify-content-center auth-button">
    <div class="text-center reset-form">
        <h1><?= Html::encode($this->title) ?></h1>
        <p>Please choose your new password:</p>
        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password', [
                    'inputTemplate' => '<div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-lock"></i></span></div>{input}</div>'])->passwordInput(['placeholder' => Yii::t('user', 'password')])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-grey btn-block']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
