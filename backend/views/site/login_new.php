<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>
<div class="view-body flex-row justify-content-center text-center mb-4">
    <div class="title-sign"><?= Yii::t('cabinet', 'Login') ?></div>
    <div class="mt-4">
        <div class="mb-2 ml-4 mr-4">
            <?php if(!empty($model->errors)):?>
                <?php foreach ($model->errors as $error) :?>
                    <div class="bg-red"><?=$error[0]?></div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <form action="" method="post">
            <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->csrfToken; ?>">
            <div class="d-flex flex-row justify-content-center text-center">
                <div class="profile-block">
                    <div class="profile-content">
                        <label for="email"><?= Yii::t('cabinet', 'Deine E-mail-Adresse:') ?></label>
                        <input name="LoginForm[email]" type="email" id="email" value="<?= $model->email ?>" placeholder="<?=Yii::t('cabinet', 'Textfeld')?>" required>
<!--                        <button type="submit">--><?//= Yii::t('cabinet', 'Login') ?><!--</button>-->
                    </div>
                </div>
                <div class="profile-block">
                    <div class="profile-content">
                        <label for="password"><?= Yii::t('cabinet', 'Dein Passwort:') ?></label>
                        <input name="LoginForm[password]" type="password" id="password" value="" placeholder="<?=Yii::t('cabinet', '********')?>" required>
<!--                        <button type="submit">--><?//= Yii::t('cabinet', 'Login') ?><!--</button>-->
                    </div>
                </div>
            </div>
            <div class="d-flex flex-row justify-content-center text-center">
                <div class="profile-block bottom-block">
                    <div class="profile-content">
                        <label for="remember"><?= Yii::t('cabinet', 'Angemeldet bleiben') ?></label>
                        <input name="LoginForm[rememberMe]" type="checkbox" id="remember" <?= $model->rememberMe ? 'checked' : '' ?> value="<?=$model->rememberMe?>">
<!--                        <button type="submit">--><?//= Yii::t('cabinet', 'Login') ?><!--</button>-->
                    </div>
                </div>
<!--                <div class="profile-block bottom-block">-->
<!--                    <div class="profile-content">-->
<!--                        <label for="reset" class="remember">-->
<!--                        --><?//= Html::a(Yii::t('cabinet', 'Reset passwort'), ['/site/request-password-reset']) ?>
<!--                        </label>-->
<!---->
<!--                        <button type="submit">--><?//= Yii::t('cabinet', 'Login') ?><!--</button>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
            <div class="d-flex flex-row justify-content-center text-center mt-4">
                <button type="submit" class="btn auth-btn"><?= Yii::t('cabinet', 'Login') ?></button>
            </div>
        </form>
    </div>
</div>
