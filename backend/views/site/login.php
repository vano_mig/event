<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('cabinet', 'Sign in');
?>

<div class="view-body d-flex flex-row justify-content-center auth-button">
    <div class="text-center reset-form"><h1 class="title-sign"><?= Html::encode($this->title) ?></h1>

    <p class="mt-4 mb-4">Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'email', [
                'inputTemplate'=>'<div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-envelope"></i></span></div>{input}</div>'])->textInput(['autofocus' => true, 'placeholder'=>Yii::t('user', 'email')])->label(false) ?>

            <?= $form->field($model, 'password', [
                'inputTemplate'=>'<div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-lock"></i></span></div>{input}</div>'])->passwordInput(['placeholder'=>Yii::t('user', 'password')])->label(false) ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-grey btn-block', 'name' => 'login-button']) ?>
            </div>
            <div>
                If you forgot your password you can <?= Html::a('reset it', ['/site/request-password-reset']) ?>.
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    </div>
</div>
