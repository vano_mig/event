<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

?>

<div class="d-flex flex-row justify-content-center auth-button">
    <div class="col-6"><?=Html::a('Login', Url::toRoute('/site/login'), ['class'=>'btn btn-block btn-grey'])?></div>
    <div class="col-6"><?=Html::a('Registrieren', Url::toRoute('/site/signup'), ['class'=>'btn btn-block btn-grey'])?></div>
</div>
<div class="logo">
    <?= Html::img('/images/system/logo1.png', ['class' => 'image']); ?>
</div>
<div class="main-name">
    <?= Html::img('/images/system/tradingj.png', ['class' => 'image']); ?>
</div>

<div class="web-name">
    <?= Html::img('/images/system/webname.png', ['class' => 'image']); ?>
</div>
